package com.medical.app.utils;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.Tracker.DataSet;
import com.medical.app.dao.entity.Tracker.DataSet.DataSetType;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LineChartInvoker {

  public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM",
      Locale.ENGLISH);

  public static void fillLineChart(LineChart mChart, Tracker dataModel) {
    mChart.getDescription().setEnabled(false);
    mChart.setDrawGridBackground(false);
    mChart.animateX(3000);
    mChart.getLegend().setEnabled(false);
    // add data
    setData(mChart, dataModel);
    mChart.setNoDataText("You need to provide data for the chart.");

    mChart.getDescription().setEnabled(false);
    //  dont forget to refresh the drawing
    mChart.invalidate();
  }

  private static void setData(LineChart mChart, Tracker dto) {
    final Map<DataSetType, List<DataSet>> listOfValues = dto.getDataSets()
        .stream()
        .sorted(Comparator.comparing(DataSet::getDate))
        .collect(Collectors.groupingBy(DataSet::getType));
    final XAxis xAxis = mChart.getXAxis();
    xAxis.setPosition(XAxisPosition.BOTTOM);
    xAxis.setValueFormatter(new ValueFormatter() {
      @Override
      public String getFormattedValue(float value) {
        long millis = TimeUnit.MILLISECONDS.toMillis((long) value);
        return SIMPLE_DATE_FORMAT.format(new Date(millis));
      }
    });

    AtomicInteger index = new AtomicInteger(0);
    final List<ILineDataSet> dataSets = listOfValues
        .keySet()
        .stream()
        .map(key -> {
          final String name = key.getName();
          final List<DataSet> lineData = listOfValues.get(key);

          final List<Entry> yVals = lineData.stream()
              .map(data -> new Entry(data.getDate().toDate().getTime(), data.getValue().floatValue()))
              .collect(Collectors.toList());
          final LineDataSet lineDataSet = new LineDataSet(yVals, name);
          final int color = ColorTemplate.VORDIPLOM_COLORS[index.getAndIncrement()];
          lineDataSet.setColor(color);
          lineDataSet.setCircleColor(color);
          lineDataSet.setLineWidth(2.5f);
          lineDataSet.setCircleRadius(3f);
          return lineDataSet;
        }).collect(Collectors.toList());

    LineData chartData = new LineData(dataSets);
    mChart.setData(chartData);
  }

}
