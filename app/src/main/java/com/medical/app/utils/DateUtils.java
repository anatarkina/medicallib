package com.medical.app.utils;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.text.Editable;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;

import com.redmadrobot.inputmask.MaskedTextChangedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {

    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy");
    public static final DateTimeFormatter JODA_DATE_FORMATTER = DateTimeFormat
            .forPattern("dd.MM.yyyy");
    public static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("dd.MM.yyyy-HH-mm-ss");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat
            .forPattern("dd.MM.yyyy HH:mm");
    public static final DateTimeFormatter ONLY_TIME_FORMATTER = DateTimeFormat
            .forPattern("HH:mm");

    public static DatePickerDialog.OnDateSetListener getDatePicker(Consumer<String> dateConsumer) {
        return new DatePickerDialog.OnDateSetListener() {
            final Calendar myCalendar = Calendar.getInstance();

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateConsumer.accept(DATE_FORMATTER.format(myCalendar.getTime()));
            }
        };
    }


    public static OnClickListener getDateClickListener(EditText dateText, Context context) {
        return (view) -> {
            Calendar myCalendar = Calendar.getInstance();
            final OnDateSetListener datePicker = getDatePicker((date) -> dateText.setText(date));
            new DatePickerDialog(context, datePicker, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        };
    }

}
