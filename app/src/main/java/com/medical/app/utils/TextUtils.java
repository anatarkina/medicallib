package com.medical.app.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.fragment.app.FragmentActivity;

public class TextUtils {
    public static View.OnFocusChangeListener getHideKeyboardFocusChanged(FragmentActivity context) {
        return (v, hasFocus) -> {
            if (!hasFocus) {
                hideKeyBoard(context,v);
            }
        };
    }

    public static void hideKeyBoard(FragmentActivity context, View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow (v.getWindowToken(), 0);
    }
}
