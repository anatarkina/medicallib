package com.medical.app.utils;

import android.content.Context;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;
import com.leinardi.android.speeddial.SpeedDialActionItem;

public class FabUtils {

  public static SpeedDialActionItem getFabItem(@IdRes int id, @DrawableRes int fabImageResource,
      String labelRes, @ColorInt int labelColor, @ColorInt int backgroundColor, Context context) {
    return new SpeedDialActionItem.Builder(id, fabImageResource)
        .setFabImageTintColor(ContextCompat.getColor(context, android.R.color.white))
        .setLabel(labelRes)
        .setLabelColor(labelColor)
        .setLabelBackgroundColor(backgroundColor)
        .create();
  }


}
