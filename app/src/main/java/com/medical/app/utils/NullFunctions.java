package com.medical.app.utils;

import com.google.common.base.Strings;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public final class NullFunctions {

    private NullFunctions() {
    }

    public static void notNull(Object... objects) {
        for (Object o : objects) {
            if (o == null) {
                throw new IllegalStateException("Value is null");
            }
        }
    }

    public static boolean checkNotNull(Object... objects) {
        for (Object o : objects) {
            if (o == null) {
                return false;
            }
        }
        return true;
    }


    public static <T> void ifNotNull(T obj, Consumer<T> consumer) {
        if (obj != null) {
            consumer.accept(obj);
        }
    }

    public static <T> void ifNotNull(T obj, Consumer<T> consumer, T defaultValue) {
        ifNotNull(obj, consumer, (Supplier<T>) () -> defaultValue);
    }

    public static <T> void ifNotNull(T obj, Consumer<T> consumer, Supplier<T> defaultSupplier) {
        if (obj != null) {
            consumer.accept(obj);
        } else {
            consumer.accept(defaultSupplier.get());
        }
    }

    public static <T> T ifNotNull(Object obj, Supplier<T> supplier) {
        if (obj != null) {
            return supplier.get();
        }
        return null;
    }

    public static <T, R> R ifNotNullGet(T obj, Function<T, R> converter) {
        if (obj != null) {
            return converter.apply(obj);
        }
        return null;
    }

    public static String ifNotEmptyGetString(String obj, Function<String, String> converter) {
        if (!isBlankString(obj)) {
            return converter.apply(obj);
        }
        return "";
    }

    public static <T> T ifNotEmptyGet(String obj, Function<String, T> converter) {
        if (!isBlankString(obj)) {
            return converter.apply(obj);
        }
        return null;
    }

    public static boolean isBlankString(String string) {
        return string == null || string.trim().isEmpty();
    }

    public static <T, R> R ifNotNullGet(T obj, Function<T, R> converter, R defaultValue) {
        if (obj != null) {
            return converter.apply(obj);
        }
        return defaultValue;
    }
}
