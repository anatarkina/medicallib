package com.medical.app.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ImageInfoUtil {

  public static final String DATE_FORMAT = "yyyyMMdd_HHmmss";

  private static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
      DATE_FORMAT, Locale.US);

  public static File createImageFile(Context context) throws IOException {
    // Create an image file name
    String timeStamp = DATE_FORMATTER.format(new Date());
    String imageFileName = "JPEG_" + timeStamp + "_";
    File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    File image = File.createTempFile(
        imageFileName,  /* prefix */
        ".jpg",         /* suffix */
        storageDir      /* directory */
    );
    return image;
  }


  public static String getPathFromGooglePhotosUri(Uri uriPhoto, Context context) {
    if (uriPhoto == null) {
      return null;
    }

    FileInputStream input = null;
    FileOutputStream output = null;
    try {
      ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uriPhoto, "r");
      FileDescriptor fd = pfd.getFileDescriptor();
      input = new FileInputStream(fd);

      String tempFilename = getTempFilename(context);
      output = new FileOutputStream(tempFilename);

      int read;
      byte[] bytes = new byte[4096];
      while ((read = input.read(bytes)) != -1) {
        output.write(bytes, 0, read);
      }
      return tempFilename;
    } catch (IOException ignored) {
      // Nothing we can do
    } finally {
      closeSilently(input);
      closeSilently(output);
    }
    return null;
  }

  public static void closeSilently(Closeable c) {
    if (c == null) {
      return;
    }
    try {
      c.close();
    } catch (Throwable t) {
      // Do nothing
    }
  }

  private static String getTempFilename(Context context) throws IOException {
    File outputDir = context.getCacheDir();
    File outputFile = File.createTempFile("image", "tmp", outputDir);
    return outputFile.getAbsolutePath();
  }

  public static void copyFile(File sourceFile, File destFile) throws IOException {
    if (!sourceFile.exists()) {
      return;
    }

    FileChannel source = null;
    FileChannel destination = null;
    source = new FileInputStream(sourceFile).getChannel();
    destination = new FileOutputStream(destFile).getChannel();
    if (destination != null && source != null) {
      destination.transferFrom(source, 0, source.size());
    }
    if (source != null) {
      source.close();
    }
    if (destination != null) {
      destination.close();
    }
  }

}
        
