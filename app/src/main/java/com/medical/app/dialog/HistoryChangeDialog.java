package com.medical.app.dialog;


import static com.medical.app.dao.entity.Record.RecordType.ANALYSIS;
import static com.medical.app.dao.entity.Record.RecordType.CONCLUSION;
import static com.medical.app.dao.entity.Record.RecordType.RECEIPT;
import static com.medical.app.dao.entity.Record.RecordType.RESEARCH;
import static com.medical.app.utils.DateUtils.JODA_DATE_FORMATTER;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.dto.SnapshotDto;
import com.medical.app.features.ImageWorker;
import com.medical.app.features.adapter.history.HistoryEditAdapter;
import com.medical.app.features.home.ProfileDialog;
import com.medical.app.repository.RecordRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class HistoryChangeDialog extends ImageWorker {

    @NonNull
    private String history;
    private GridView imageGrid;
    private Button addBtn;
    private TextInputEditText topDescription;
    private TextInputLayout topInput;
    private Button saveBtn;
    private RecordRepository recordRepository;
    private HistoryEditAdapter adapter;
    private List<String> existingHistories = new ArrayList<>();
    private PopupMenu popupMenu;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_change_history, null);
        imageGrid = dialogLayout.findViewById(R.id.grid_view);
        topDescription = dialogLayout.findViewById(R.id.sp_history_text);
        topInput = dialogLayout.findViewById(R.id.sp_history_field);
        addBtn = dialogLayout.findViewById(R.id.add_btn);
        saveBtn = dialogLayout.findViewById(R.id.save_btn);
        adapter = HistoryEditAdapter.builder().context(getContext()).build();
        imageGrid.setAdapter(adapter);
        recordRepository = new RecordRepository(getActivity());
        saveBtn.setOnClickListener(getSaveOnClickListener());
        popupMenu = getPopUpMenu(addBtn);
        popupMenu.setOnMenuItemClickListener(getOnMenuClickListener());

        addBtn.setOnClickListener(view -> popupMenu.show());

        fillValues();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        dialog.setContentView(dialogLayout);
        return dialog;
    }


    private void fillValues() {
        topDescription.setText(history);
        topDescription.addTextChangedListener(getValidationListener());
        recordRepository.getAllRecords().observe(this, records -> {
            adapter.setSnapshots(getSnapshots(records));
            existingHistories = records
                    .stream()
                    .map(Record::getHistory)
                    .filter(value -> !value.equalsIgnoreCase(history))
                    .distinct()
                    .collect(Collectors.toList());
        });
    }

    private TextWatcher getValidationListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (existingHistories.contains(s.toString())) {
                    topInput.setError("История болезни с таким наименованием уже существует!");
                    topInput.setErrorEnabled(true);
                    saveBtn.setEnabled(false);
                    addBtn.setEnabled(false);
                } else {
                    topInput.setError(null);
                    topInput.setErrorEnabled(false);
                    topDescription.setError(null);
                    saveBtn.setEnabled(true);
                    addBtn.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @Override
    public void doWithImagePath(Uri path, Bundle bundle) {
        final RecordType recordType = RecordType.valueOf(bundle.getString(RecordType.class.getName()));
        SnapshotDialog
                .builder()
                .imageUrl(path)
                .recordType(recordType)
                .historyText(history)
                .build().show(getFragmentManager(), SnapshotDialog.TAG);
    }


    private OnMenuItemClickListener getOnMenuClickListener() {
        return item -> {
            final int itemId = item.getItemId();
            Bundle bundle = new Bundle();
            switch (itemId) {
                case R.id.action_receipt:
                    bundle
                            .putString(RecordType.class.getName(), RECEIPT.name());
                    break;
                case R.id.action_analysis:
                    bundle
                            .putString(RecordType.class.getName(), ANALYSIS.name());
                    break;
                case R.id.action_research:
                    bundle
                            .putString(RecordType.class.getName(), RESEARCH.name());
                    break;
                case R.id.action_conclusion:
                    bundle
                            .putString(RecordType.class.getName(), CONCLUSION.name());
                    break;
            }
            saveCurrentHistory();
            showPictureDialogWithBundle(bundle);
            return true;
        };
    }

    private OnClickListener getSaveOnClickListener() {
        return view -> {
            if (saveCurrentHistory()) {
                this.dismiss();
            }
        };
    }

    private boolean saveCurrentHistory() {
        if (!topInput.isErrorEnabled()) {
            final String topDescription = this.topDescription.getText().toString();
            boolean onlyUpdatable = history.equalsIgnoreCase(topDescription) ? true : false;
            String previousHistory = history;
            history = topDescription;
            final List<SnapshotDto> snapshots = adapter
                    .getSnapshots()
                    .stream()
                    .filter(snapshotDto -> onlyUpdatable ? snapshotDto.isUpdated() : true)
                    .collect(Collectors.toList());
            updateHistories(snapshots, onlyUpdatable, previousHistory);
            if (!snapshots.isEmpty()) {
                Toast
                        .makeText(getContext(),
                                "История сохранена! ",
                                Toast.LENGTH_LONG)
                        .show();
            }
            return true;
        } else {
            Toast
                    .makeText(getContext(),
                            "Не можем сохранить изменения! ",
                            Toast.LENGTH_LONG)
                    .show();
            return false;
        }
    }

    private void updateHistories(List<SnapshotDto> snapshotDto, boolean onlyUpdatable,
                                 String previousDescription) {
        recordRepository
                .updateHistories(snapshotDto, history, onlyUpdatable, previousDescription);
    }

    private List<SnapshotDto> getSnapshots(List<Record> records) {
        return records.stream()
                .filter(
                        record -> history.equalsIgnoreCase(record.getHistory()) || TextUtils
                                .isEmpty(record.getHistory()))
                .sorted(Comparator.comparing(Record::getDate).reversed())
                .map(record -> SnapshotDto.builder()
                        .recordId(record.getId())
                        .imageUrl(record.getImageUrl())
                        .description(record.getValue())
                        .chipSelected(record.getType().getName())
                        .selected(history.equalsIgnoreCase(record.getHistory()))
                        .date(record.getDate().toString(JODA_DATE_FORMATTER)).build())
                .collect(Collectors.toList());
    }

    private PopupMenu getPopUpMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this.getContext(),
                view);
        popupMenu.inflate(R.menu.records_menu);
        return popupMenu;
    }

}
