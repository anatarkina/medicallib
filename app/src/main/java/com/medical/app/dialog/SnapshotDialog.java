package com.medical.app.dialog;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.medical.app.R;
import com.medical.app.dao.entity.History;
import com.medical.app.dao.entity.HistoryWithRecords;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.features.ImageWorker;
import com.medical.app.repository.RecordRepository;
import com.medical.app.view.DateEditTextView;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static com.medical.app.dao.entity.Record.RecordType.CONCLUSION;
import static com.medical.app.utils.TextUtils.getHideKeyboardFocusChanged;
import static java.util.Optional.ofNullable;

@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class SnapshotDialog extends ImageWorker {

    public static final String TAG = "SnapshotDialog";

    @NonNull
    @Setter
    private Uri imageUrl;
    @NonNull
    @Setter
    private RecordType recordType;
    @Setter
    private String historyText;
    @Setter
    private Action action = Action.NEW;
    @Setter
    private Record record;
    private TextView topText, replayPhotoBtn, addOneMoreBtn;
    private Button saveBtn;
    private ImageView recordImage;
    private TextInputLayout valueField, ownerField, historyField;
    private TextInputEditText ownerText;
    private DateEditTextView dateText;
    private AutoCompleteTextView historyEditableText, valueText;
    private Dialog dialog;
    private RecordRepository recordRepository;
    private ArrayAdapter<String> historyAdapter;

    public static SnapshotDialogBuilder builder() {
        return new SnapshotDialogBuilder();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_snapshot_create, null);
        recordImage = dialogLayout.findViewById(R.id.record_img);
        valueField = dialogLayout.findViewById(R.id.sp_value_field);
        ownerField = dialogLayout.findViewById(R.id.sp_owner_field);
        dateText = dialogLayout.findViewById(R.id.sp_date_text);
        dateText.setFormattedText(LocalDate.now());
        topText = dialogLayout.findViewById(R.id.sp_top_txt);
        replayPhotoBtn = dialogLayout.findViewById(R.id.replay_photo_btn);
        addOneMoreBtn = dialogLayout.findViewById(R.id.add_one_more_btn);
        saveBtn = dialogLayout.findViewById(R.id.save_btn);
        recordRepository = new RecordRepository(activity);

        saveBtn.setOnClickListener(getSaveClickListener());
        addOneMoreBtn.setOnClickListener(getOneMoreClickListener());
        replayPhotoBtn.setOnClickListener(getReplayClickListener());

        ownerText = dialogLayout.findViewById(R.id.sp_owner_text);
        valueText = dialogLayout.findViewById(R.id.sp_value_text);

        historyEditableText = dialogLayout.findViewById(R.id.sp_history_text);
        historyField = dialogLayout.findViewById(R.id.sp_history_field);

        ownerText.setOnFocusChangeListener(getHideKeyboardFocusChanged(getActivity()));
        historyEditableText.setOnFocusChangeListener(getHideKeyboardFocusChanged(getActivity()));
        dateText.setOnFocusChangeListener(getHideKeyboardFocusChanged(getActivity()));
        valueText.setOnFocusChangeListener(getHideKeyboardFocusChanged(getActivity()));

        historyAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1,
                new ArrayList<>());
        historyEditableText.setAdapter(historyAdapter);

        dialog.setContentView(dialogLayout);
        fillValues();
        this.dialog = dialog;
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return dialog;
    }

    private OnClickListener getReplayClickListener() {
        return view -> takePhotoFromCamera();
    }

    private OnClickListener getOneMoreClickListener() {
        return view -> {
            saveNewRecord();
            showPictureDialog();
        };
    }

    private OnClickListener getSaveClickListener() {
        return view -> {
            if (action.equals(Action.NEW)) {
                saveNewRecord();
            } else {
                updateRecord();
            }
            dialog.dismiss();
        };
    }

    private void updateRecord() {
        if (record != null) {
            recordRepository.safeUpdateRecord(record.getId(),
                    dateText.getLocalDateTime(),
                    ownerText.getText().toString(),
                    historyEditableText.getText().toString(),
                    imageUrl.toString(),
                    valueText.getText().toString());
            Toast.makeText(getContext(), String.format("Запись изменена!"),
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private Record saveNewRecord() {
        final Record record = getRecord();
        recordRepository.insertRecord(record);
        return record;
    }

    private Record getRecord() {
        return Record.builder()
                .userId(FirebaseAuth.getInstance().getUid())
                .date(dateText.getLocalDateTime())
                .clinic(ownerText.getText().toString())
                .history(historyEditableText.getText().toString())
                .type(recordType)
                .imageUrl(ofNullable(imageUrl).map(Object::toString).orElse(""))
                .value(valueText.getText().toString())
                .build();
    }


    private void fillValues() {
        Glide.with(getActivity())
                .load(imageUrl)
                .into(recordImage);
        topText.setText(recordType.getStuff());
        valueField.setHint(recordType.getValue());
        ownerField.setHint(recordType.getOwnerField());
        if (CONCLUSION.equals(recordType)) {
            String[] decease = getResources().getStringArray(R.array.list_of_historyText);
            ArrayAdapter<String> deceaseAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_list_item_1,
                    decease);
            valueText.setAdapter(deceaseAdapter);
        }

        if (!Strings.isNullOrEmpty(historyText)) {
            historyEditableText.setText(historyText);
            historyField.setEnabled(false);
            historyEditableText.setEnabled(false);
        }
        if (record != null) {
            dateText.setFormattedText(record.getDate());
            ownerText.setText(record.getClinic());
            valueText.setText(record.getValue());
        }
        fillHistoryAdapter();
    }

    private void fillHistoryAdapter() {
        recordRepository.getHistories().observe(this, records -> {
            if (records != null) {
                historyAdapter.clear();
                historyAdapter.addAll(records.stream().map(HistoryWithRecords::getHistory)
                        .map(History::getId).collect(Collectors.toList()));
            }
        });
    }

    @Override
    public void doWithImagePath(Uri uri, Bundle bundle) {
        imageUrl = uri;
        fillValues();
    }

    @NoArgsConstructor
    public static class SnapshotDialogBuilder {

        private @NonNull Uri imageUrl;
        private @NonNull Record.RecordType recordType;
        private String historyText;
        private Action action = Action.NEW;
        private Record record;

        public SnapshotDialogBuilder imageUrl(@NonNull Uri imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public SnapshotDialogBuilder recordType(@NonNull Record.RecordType recordType) {
            this.recordType = recordType;
            return this;
        }

        public SnapshotDialogBuilder historyText(String historyText) {
            this.historyText = historyText;
            return this;
        }

        public SnapshotDialogBuilder action(Action action) {
            this.action = action;
            return this;
        }

        public SnapshotDialogBuilder record(Record record) {
            this.record = record;
            return this;
        }

        public SnapshotDialog build() {
            final SnapshotDialog snapshotDialog = new SnapshotDialog();
            snapshotDialog.setImageUrl(imageUrl);
            snapshotDialog.setRecordType(recordType);
            snapshotDialog.setHistoryText(historyText);
            snapshotDialog.setAction(action);
            snapshotDialog.setRecord(record);
            return snapshotDialog;
        }
    }
}
