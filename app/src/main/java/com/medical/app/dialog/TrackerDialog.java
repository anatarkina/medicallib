package com.medical.app.dialog;

import static com.medical.app.utils.LineChartInvoker.fillLineChart;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.charts.LineChart;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.Tracker.DataSet;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class TrackerDialog {

    private final Tracker tracker;
    private final Activity activity;
    private LineChart lineChart;
    private TableLayout historyTable;
    private TextView currentValues;
    private Button dismissButton;

    public Dialog getDialog() {
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_tracker, null);
        lineChart = dialogLayout.findViewById(R.id.line_chart);

        historyTable = dialogLayout.findViewById(R.id.table_tracker);
        currentValues = dialogLayout.findViewById(R.id.current_values);
        dismissButton = dialogLayout.findViewById(R.id.dismiss_btn);
        fill(tracker);
        dialog.setContentView(dialogLayout);
        dismissButton.setOnClickListener(view -> dialog.dismiss());
        FirebaseAnalytics.getInstance(activity).setCurrentScreen(activity, this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return dialog;
    }

    private void fill(Tracker tracker) {
        final List<DataSet> sortedDataSets = tracker.getDataSets()
                .stream()
                .sorted(Comparator.comparing(DataSet::getDate).reversed()).collect(Collectors.toList());
        final Map<String, List<DataSet>> dataSets = sortedDataSets
                .stream()
                .sorted(Comparator.comparing(DataSet::getDate).reversed())
                .collect(Collectors
                        .groupingBy(DataSet::getFormattedDate, LinkedHashMap::new, Collectors.toList()));
        if (!dataSets.isEmpty()) {
            fillCurrentTable(dataSets);
            fillHistoryTable(dataSets);
        }
        fillLineChart(lineChart, tracker);
    }

    private void fillCurrentTable(Map<String, List<DataSet>> values) {
        currentValues.setText(values.keySet().stream().findFirst()
                .map(lastDate -> values.get(lastDate))
                .orElse(Collections.emptyList())
                .stream()
                .map(value -> value.getValue() + " " + value.getType().getMeasure())
                .collect(Collectors.joining("  ")));
    }

    private void fillHistoryTable(
            Map<String, List<DataSet>> dataSets) {
        historyTable.removeAllViews();
        historyTable.setStretchAllColumns(true);
        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        final int textColor = ContextCompat.getColor(activity, R.color.icon_color);

        TableRow tableRow = getTableRow();
        tableRow.setLayoutParams(tableParams);// TableLayout is the parent view
        historyTable.addView(tableRow);
        dataSets
                .keySet()
                .stream()
                //   .sorted(Comparator.comparingLong(Date::getTime).reversed())
                .forEach(key -> {
                    TableRow row = getTableRow();
                    row.setLayoutParams(tableParams);// TableLayout is the parent view

                    TextView date = new TextView(activity);
                    date.setLayoutParams(rowParams);// TableRow is the parent view
                    date.setText(key);
                    date.setTextColor(textColor);
                    date.setTextSize(16);
                    row.addView(date);
                    dataSets.get(key).forEach(value -> {
                        TextView number = new TextView(activity);
                        number.setLayoutParams(rowParams);// TableRow is the parent view
                        number.setText(value.getValue().toString());
                        number.setTextColor(textColor);
                        number.setTextSize(16);
                        row.addView(number);
                    });

                    historyTable.addView(row);

                });
    }

    private TableRow getTableRow() {
        return new TableRow(activity);
    }


}
