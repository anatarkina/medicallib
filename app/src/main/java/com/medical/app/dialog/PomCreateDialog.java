package com.medical.app.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioGroup;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.chip.ChipGroup.OnCheckedChangeListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.common.base.Strings;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.Pill.UnitOfDosage;
import com.medical.app.dao.entity.Pill.UnitOfDuration;
import com.medical.app.dao.entity.Pill.Units;
import com.medical.app.dao.entity.Pom;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.repository.PomRepository;
import com.medical.app.scheduler.PillSchedulerManager;

import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;

import static com.medical.app.utils.NullFunctions.ifNotEmptyGet;
import static com.medical.app.utils.NullFunctions.ifNotNull;
import static java.util.Optional.ofNullable;

@NoArgsConstructor
public class PomCreateDialog extends DialogFragment {

    private TextInputEditText diagnosisTxt, pillText, dosageTxt,
            frequencyTxt, durationTxt, moreInfoTxt;
    private RadioGroup dosageGroup, durationGroup;
    private Button addPillBtn, saveBtn;
    private ChipGroup pillsGroup;

    private Map<String, Pill> pills = new HashMap<>();
    private List<Pill> removedPills = new ArrayList<>();
    private PomRepository repository;
    private String currentDiagnosis;
    private Pom pom;
    private Action action = Action.NEW;
    private Integer previousSelected;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_pom_create, null);
        pillText = dialogLayout.findViewById(R.id.pill_txt);
        dosageTxt = dialogLayout.findViewById(R.id.dosage_text);
        frequencyTxt = dialogLayout.findViewById(R.id.frequency_text);
        dosageGroup = dialogLayout.findViewById(R.id.dosage_group);
        durationGroup = dialogLayout.findViewById(R.id.duration_group);
        addPillBtn = dialogLayout.findViewById(R.id.add_btn);
        saveBtn = dialogLayout.findViewById(R.id.save_btn);
        diagnosisTxt = dialogLayout.findViewById(R.id.diagnosis_txt);
        durationTxt = dialogLayout.findViewById(R.id.duration_text);
        moreInfoTxt = dialogLayout.findViewById(R.id.more_info_txt);
        pillsGroup = dialogLayout.findViewById(R.id.pill_groups);
        addPillBtn.setOnClickListener(addPillListener());
        saveBtn.setOnClickListener(addSaveListener());
        repository = new PomRepository(activity);
        dialog.setContentView(dialogLayout);
        pillsGroup.setOnCheckedChangeListener(getChipsSelectedListener());
        if (pom != null) {
            currentDiagnosis = pom.getDiagnosis();
            diagnosisTxt.setText(pom.getDiagnosis());
        }
        final List<String> names = pills.values().stream().map(Pill::getName)
                .collect(Collectors.toList());
        names.forEach(value -> addChipsByName(value, inflater));
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return dialog;
    }

    @NotNull
    protected OnCheckedChangeListener getChipsSelectedListener() {
        return (chipGroup, checkId) -> {
            final String selectedChip = ofNullable((Chip) chipGroup.findViewById(checkId))
                    .map(Chip::getText)
                    .map(CharSequence::toString)
                    .orElse(null);
            if (previousSelected != null) {
                final Chip previousChip = pillsGroup.findViewById(previousSelected);
                updatePreviousSelectedPill(previousChip);
            }

            previousSelected = checkId;
            updateValues(selectedChip);
        };
    }


    private OnClickListener addSaveListener() {
        return v -> {
            final Chip previousChip = pillsGroup.findViewById(pillsGroup.getCheckedChipId());
            boolean saveHistory;
            if (previousChip != null) {
                saveHistory = updatePreviousSelectedPill(previousChip);
            } else {
                saveHistory = ignoreSavingPill() ? true : savePill();
            }
            if (saveHistory) {
                final String currentDiagnosis = diagnosisTxt.getText().toString();
                if (Action.NEW.equals(action)) {
                    repository.insert(pills, currentDiagnosis);
                } else {
                    pom.setDiagnosis(currentDiagnosis);
                    repository.deletePills(removedPills);
                    repository.update(pills, pom);
                }
                rescheduleNotifications();
                this.dismiss();
            }
        };
    }

    private void rescheduleNotifications() {
        PillSchedulerManager.builder()
                .context(getActivity().getApplicationContext())
                .reschedule();
    }


    private boolean updatePreviousSelectedPill(Chip previousChip) {
        if (previousChip != null) {
            final Pill remove = pills.remove(previousChip.getText().toString());
            if (Action.EDIT.equals(action)) {
                removedPills.add(remove);
            }
            remove.setName(pillText.getText().toString());
            previousChip.setText(remove.getName());
            pills.put(remove.getName(), remove);
            return savePill();
        }
        return false;
    }

    private OnClickListener addPillListener() {
        return v -> addPill();
    }

    private void addPill() {
        if (ignoreSavingPill()) {
            checkPillFields();
        } else {
            savePill();
        }
    }

    private boolean checkPillFields() {
        boolean shouldSave = true;
        final String dosageName = dosageTxt.getText().toString();
        final String frequencyName = frequencyTxt.getText().toString();
        final String durationName = durationTxt.getText().toString();
        final String newPillName = pillText.getText().toString();
        if (Strings.isNullOrEmpty(newPillName)) {
            pillText.setError("Название лекарства не может быть пустым");
            shouldSave = false;
        }
        if (Strings.isNullOrEmpty(dosageName)) {
            dosageTxt.setError("Дозировка лекарства не может быть пустой");
            shouldSave = false;
        }
        if (Strings.isNullOrEmpty(frequencyName)) {
            frequencyTxt.setError("Частота приема лекарства диагноза не может быть пустой");
            shouldSave = false;
        }
        if (Strings.isNullOrEmpty(durationName)) {
            durationTxt.setError("Продолжительность приема лекарства не может быть пустой");
            shouldSave = false;
        }
        return shouldSave;
    }

    private boolean savePill() {
        if (ignoreSavingPill() ? false : checkPillFields()) {
            final Pill pill = getPill();
            currentDiagnosis = diagnosisTxt.getText().toString();
            final String name = pill.getName();
            final boolean containsKey = pills.containsKey(name);
            if (!containsKey) {
                addChipsByName(name, this.getLayoutInflater());
            }
            pills.put(name, pill);
            pillText.setText("");
            dosageTxt.setText("");
            durationTxt.setText("");
            frequencyTxt.setText("");
            moreInfoTxt.setText("");
            return true;
        }
        return false;
    }

    private boolean ignoreSavingPill() {
        final String newPillName = pillText.getText().toString();
        return Strings.isNullOrEmpty(newPillName);
    }

    private void addChipsByName(String name, LayoutInflater inflater) {
        final Chip chip = (Chip) inflater
                .inflate(R.layout.item_entry_chip, null, false);
        chip.setText(name);
        chip.setOnCloseIconClickListener(v -> {
            final Pill remove = pills.remove(name);
            removedPills.add(remove);
            pillsGroup.removeView(chip);
        });
        pillsGroup.addView(chip);
    }

    private Pill getPill() {
        return pills.getOrDefault(dosageTxt.getText().toString(), Pill.builder()
                .dosage(dosageTxt.getText().toString())
                .duration(durationTxt.getText().toString())
                .frequency(ifNotEmptyGet(frequencyTxt.getText().toString(), frequencyTxt -> Integer.valueOf(frequencyTxt)))
                .pomDiagnosis(diagnosisTxt.getText().toString())
                .order(LocalDateTime.now())
                .name(pillText.getText().toString())
                .units(getUnits())
                .moreInfo(moreInfoTxt.getText().toString())
                .build());
    }

    private Units getUnits() {
        return Units.builder()
                .unitOfDosage(getDosageGroup())
                .unitOfDuration(getDurationGroup())
                .build();
    }

    private UnitOfDuration getDurationGroup() {
        return UnitOfDuration.valueOf(durationGroup.getCheckedRadioButtonId());
    }

    private UnitOfDosage getDosageGroup() {
        return UnitOfDosage.valueOf(dosageGroup.getCheckedRadioButtonId());
    }

    private void updateValues(String selectedValue) {
        final Pill pill = pills.getOrDefault(selectedValue, null);
        pillText.setText(ofNullable(pill).map(Pill::getName).orElse(""));
        dosageTxt.setText(ofNullable(pill).map(Pill::getDosage).orElse(""));
        durationTxt.setText((ofNullable(pill).map(Pill::getDuration).orElse("")));
        frequencyTxt.setText((ofNullable(pill).map(Pill::getFrequency)
                .map(Object::toString).orElse("")));
        moreInfoTxt.setText((ofNullable(pill).map(Pill::getMoreInfo).orElse("")));
        diagnosisTxt.setText(currentDiagnosis);
        ifNotNull((ofNullable(pill).map(Pill::getUnits)).map(Units::getUnitOfDosage).orElse(null),
                dosage -> dosageGroup.check(dosage.getType()));
        ifNotNull((ofNullable(pill).map(Pill::getUnits)).map(Units::getUnitOfDuration).orElse(null),
                duration -> durationGroup.check(duration.getType()));
    }

    public static PomCreateDialog withPills(PomWithPills pomWithPills) {
        final PomCreateDialog dialog = new PomCreateDialog();
        dialog.action = Action.EDIT;
        dialog.pills = pomWithPills
                .getPills()
                .stream()
                .collect(Collectors.toMap(Pill::getName, Function.identity()));
        dialog.pom = pomWithPills.getPom();
        return dialog;
    }

}
