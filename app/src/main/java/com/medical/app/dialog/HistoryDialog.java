package com.medical.app.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dto.RecordByDate;
import com.medical.app.features.adapter.history.HistoryAdapter;
import com.medical.app.features.adapter.history.HistoryByDateAdapter;
import com.medical.app.repository.RecordRepository;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.joda.time.LocalDate;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class HistoryDialog extends DialogFragment {

    public static final String TAG = "history_dialog";
    @NonNull
    private List<Record> records;
    @NonNull
    private String historyName;
    private List<RecordByDate> recordByDate;
    private TextView topDescription, editBtn;
    private RecyclerView listOfHistoriesByDate;
    private Button dismiss;
    private String[] months;
    private RecordRepository recordRepository;
    private HistoryByDateAdapter historyByDateAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_history, null);
        topDescription = dialogLayout.findViewById(R.id.top_description);
        listOfHistoriesByDate = dialogLayout.findViewById(R.id.list_of_histories_by_date);
        editBtn = dialogLayout.findViewById(R.id.edit_btn);
        recordRepository = new RecordRepository(activity);
        dismiss = dialogLayout.findViewById(R.id.dismiss_btn);
        dialog.setContentView(dialogLayout);
        dismiss.setOnClickListener(view -> this.dismiss());
        editBtn.setOnClickListener(getEditClickListener(activity));
        months = activity.getResources().getStringArray(R.array.months);
        recordByDate = getRecordByDate(records);
        listOfHistoriesByDate.setLayoutManager(new LinearLayoutManager(getActivity()));

        new Thread(() -> {
            historyByDateAdapter = HistoryByDateAdapter.builder().context(getActivity())
                    .records(recordByDate)
                    .build();
            activity.runOnUiThread(() -> listOfHistoriesByDate.setAdapter(historyByDateAdapter));
        }).start();

        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(activity, this.getClass().getSimpleName(), this.getClass().getSimpleName());
        fillValues();
        return dialog;
    }

    protected OnClickListener getEditClickListener(FragmentActivity activity) {
        return view -> {
            HistoryChangeDialog.of(historyName)
                    .show(activity.getSupportFragmentManager(), HistoryChangeDialog.class.getName());
            Handler handler = new Handler();
            handler.postDelayed(() -> this.dismiss(), 1000);
        };
    }

    private void fillValues() {
        topDescription.setText(historyName);
        recordRepository.getRecordsByIds(records.stream().map(Record::getId).collect(Collectors.toList()))
                .observe(this, ent -> {
                    historyByDateAdapter.updateValues(getRecordByDate(ent));
                });

    }

    private List<RecordByDate> getRecordByDate(List<Record> records) {
        return records.stream()
                .collect(Collectors.groupingBy(m -> getLocalizedDate(m.getDate().toLocalDate()),
                        TreeMap::new,
                        Collectors.toList()))
                .entrySet()
                .stream()
                .map(entry -> RecordByDate.of(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private String getLocalizedDate(LocalDate date) {

        return months[date.getMonthOfYear() - 1].toUpperCase() + " " + date.getYear();
    }

}
