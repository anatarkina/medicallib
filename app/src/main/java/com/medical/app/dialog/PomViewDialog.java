package com.medical.app.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.repository.PomRepository;
import com.medical.app.scheduler.PillSchedulerManager;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor(staticName = "of")
public class PomViewDialog extends DialogFragment {

    @NonNull
    private PomWithPills pom;

    private WebView content;
    private Button delete, edit, dismiss;
    private ImageButton share;
    private PomRepository pomRepository;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_pom_view, null);
        pomRepository = new PomRepository(activity);
        content = dialogLayout.findViewById(R.id.content);
        delete = dialogLayout.findViewById(R.id.delete_btn);
        edit = dialogLayout.findViewById(R.id.edit_btn);
        dismiss = dialogLayout.findViewById(R.id.dismiss_btn);
        share = dialogLayout.findViewById(R.id.share_button);
        share.setOnClickListener(view -> sendShareIntent());
        dismiss.setOnClickListener(v -> this.dismiss());
        delete.setOnClickListener(getOnDeleteListener());
        content.loadDataWithBaseURL(null, getContent(), "text/html", "utf-8", null);
        edit.setOnClickListener(v -> {
            PomCreateDialog.withPills(pom)
                    .show(activity.getSupportFragmentManager(), PomCreateDialog.class.getName());
            new Handler().postDelayed(() -> this.dismiss(), 1000);
        });
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        dialog.setContentView(dialogLayout);
        return dialog;
    }

    @NotNull
    protected OnClickListener getOnDeleteListener() {
        return v -> {
            pomRepository.delete(pom);
            Toast.makeText(getContext(),
                    String.format("План леченияе '%s' удален", pom.getPom().getDiagnosis()),
                    Toast.LENGTH_SHORT)
                    .show();
            rescheduleNotifications();
            this.dismiss();
        };
    }

    private String getContent() {
        AtomicInteger index = new AtomicInteger(1);
        return "<h2><font color='#4DB6AC'>" + pom.getPom().getDiagnosis() + "</font></h2>" +
                pom.getPills().stream()
                        .sorted(Comparator.comparing(Pill::getOrder))
                        .map(pill -> pill.getPillDescription(index.getAndIncrement())).collect(Collectors.joining());
    }

    private void sendShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                Html.fromHtml(getContent(), Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
        sendIntent.setType("text/html");
        getContext().startActivity(sendIntent);
    }

    private void rescheduleNotifications() {
        PillSchedulerManager.builder()
                .context(getActivity().getApplicationContext())
                .reschedule();
    }

}
