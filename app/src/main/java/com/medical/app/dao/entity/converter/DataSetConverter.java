package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.medical.app.dao.entity.Tracker.DataSet;

import org.joda.time.LocalDateTime;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class DataSetConverter {

    @TypeConverter
    public String fromListToString(List<DataSet> dataset) {
        if (dataset == null) {
            return (null);
        }
        final GsonBuilder builder = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalTimeSerializer());
        final Gson gson = builder.create();
        Type type = new TypeToken<List<DataSet>>() {
        }.getType();
        String json = gson.toJson(new ArrayList<>(dataset), type);
        return json;
    }

    @TypeConverter
    public List<DataSet> fromStringToList(String strDataset) {
        if (strDataset == null) {
            return (null);
        }
        final GsonBuilder builder = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalTimeSerializer());
        final Gson gson = builder.create();
        Type type = new TypeToken<List<DataSet>>() {
        }.getType();
        List<DataSet> countryLangList = gson.fromJson(strDataset, type);
        return countryLangList;
    }
}
