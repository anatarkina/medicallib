package com.medical.app.dao;

import static com.medical.app.dao.entity.History.DEFAULT_ID;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import com.medical.app.dao.entity.History;
import com.medical.app.dao.entity.HistoryWithRecords;
import com.medical.app.dao.entity.Record;
import java.util.List;

@Dao
public abstract class HistoryDao {

  @Transaction
  @Query("SELECT * FROM History WHERE id = :history and userId = :userId")
  public abstract LiveData<HistoryWithRecords> liveDataByHistory(String history, String userId);

  @Transaction
  @Query("SELECT * FROM History WHERE id = :history and userId = :userId")
  public abstract History getHistory(String history, String userId);

  @Transaction
  @Query("SELECT * FROM History WHERE id = :history and userId = :userId")
  public abstract HistoryWithRecords getByHistory(String history, String userId);

  @Transaction
  @Query("SELECT * FROM History WHERE userId=:userId")
  public abstract LiveData<List<HistoryWithRecords>> fetchAll(String userId);

  @Transaction
  public void deleteHistory(String history, String userId, String newValue) {
    insert(History.builder().id(newValue).userId(userId).build());
    final HistoryWithRecords historyWithRecords = getByHistory(history, userId);
    for (Record record : historyWithRecords.records) {
      record.setHistory(newValue);
      insert(record);
    }
    if (history.equalsIgnoreCase(DEFAULT_ID)) {
      final HistoryWithRecords defaultHistoryRecords = getByHistory(DEFAULT_ID, userId);
      if (defaultHistoryRecords.getRecords().isEmpty()) {
        delete(DEFAULT_ID, userId);
      }
    } else {
      delete(history, userId);
    }
  }

  @Transaction
  @Query("DELETE FROM History WHERE id = :history and userId = :userId")
  public abstract void delete(String history, String userId);


  @Transaction
  public void insert(HistoryWithRecords historyWithRecords) {
    insert(historyWithRecords.history);
    for (Record record : historyWithRecords.records) {
      insert(record);
    }
  }

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  public abstract long insert(History history);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  public abstract long insert(Record record);

}
