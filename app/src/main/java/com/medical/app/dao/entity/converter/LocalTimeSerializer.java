package com.medical.app.dao.entity.converter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

import static com.medical.app.utils.DateUtils.DATE_TIME_FORMATTER;

public class LocalTimeSerializer implements JsonDeserializer<LocalDateTime>, JsonSerializer<LocalDateTime> {

    private static final DateTimeFormatter TIME_FORMAT = DATE_TIME_FORMATTER;

    @Override
    public LocalDateTime deserialize(final JsonElement je, final Type type,
                                     final JsonDeserializationContext jdc) throws JsonParseException {
        final String dateAsString = je.getAsString();
        if (dateAsString.length() == 0) {
            return null;
        } else {
            return TIME_FORMAT.parseLocalDateTime(dateAsString);
        }
    }

    @Override
    public JsonElement serialize(final LocalDateTime src, final Type typeOfSrc,
                                 final JsonSerializationContext context) {
        String retVal;
        if (src == null) {
            retVal = "";
        } else {
            retVal = TIME_FORMAT.print(src);
        }
        return new JsonPrimitive(retVal);
    }

}