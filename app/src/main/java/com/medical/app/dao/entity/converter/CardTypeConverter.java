package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.Tracker.TrackerType;

import java.lang.reflect.Type;

public class CardTypeConverter {

  @TypeConverter
  public String fromListToString(CardType dataset) {
    if (dataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<CardType>() {
    }.getType();
    String json = gson.toJson(dataset, type);
    return json;
  }

  @TypeConverter
  public CardType fromStringToList(String strDataset) {
    if (strDataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<CardType>() {
    }.getType();
    CardType countryLangList = gson.fromJson(strDataset, type);
    return countryLangList;
  }
}
