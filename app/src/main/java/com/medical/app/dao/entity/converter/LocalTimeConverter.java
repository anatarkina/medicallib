package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;

import org.joda.time.LocalTime;

public class LocalTimeConverter {

    @TypeConverter
    public static LocalTime toDate(String dateString) {
        if (dateString == null) {
            return null;
        } else {
            return LocalTime.parse(dateString);
        }
    }

    @TypeConverter
    public static String toDateString(LocalTime date) {
        if (date == null) {
            return null;
        } else {
            return date.toString();
        }
    }

}
