package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.dao.entity.converter.RecordTypeConverter;
import java.util.List;

@Dao
public interface RecordDao {


  @Insert(onConflict = OnConflictStrategy.REPLACE)
  Long insertTask(Record record);

  @Query("SELECT * FROM Record WHERE userId=:userId")
  LiveData<List<Record>> fetchAll(String userId);

  @Query("SELECT * FROM Record WHERE id IN (:ids)")
  LiveData<List<Record>> getAllByIds(List<Long> ids);


  @Query("SELECT * FROM Record WHERE type =:type and userId=:userId")
  LiveData<List<Record>> getByTypes(@TypeConverters({RecordTypeConverter.class}) RecordType type,
      String userId);

  @Query("SELECT * FROM Record WHERE id=:id")
  LiveData<Record> get(Long id);

  @Query("SELECT * FROM Record WHERE id=:id")
  Record getCurrent(Long id);

  @Query("UPDATE Record SET history = :history WHERE id =:id")
  int updateHistory(Long id, String history);

  @Delete
  void delete(Record record);

  @Query("DELETE FROM Record WHERE id=:id")
  void deleteById(Long id);
}
