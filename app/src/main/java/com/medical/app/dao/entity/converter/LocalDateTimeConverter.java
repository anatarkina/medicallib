package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;
import org.joda.time.LocalDateTime;

public class LocalDateTimeConverter {

  @TypeConverter
  public static LocalDateTime toDate(String dateString) {
    if (dateString == null) {
      return null;
    } else {
      return LocalDateTime.parse(dateString);
    }
  }

  @TypeConverter
  public static String toDateString(LocalDateTime date) {
    if (date == null) {
      return null;
    } else {
      return date.toString();
    }
  }

}
