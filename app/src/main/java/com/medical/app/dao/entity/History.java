package com.medical.app.dao.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(indices = {
    @Index(value = {"id", "userId"}, unique = true)
},
    inheritSuperIndices = true,
    primaryKeys = {"id", "userId"})
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class History {

  public static final String DEFAULT_ID = "";
  @NonNull
  @ColumnInfo(defaultValue = DEFAULT_ID)
  String id = "";

  @NonNull
  String userId;
}
