package com.medical.app.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CardType {
    DOCTOR("Запись к врачу", 3),
    TABLETS("Прием лекарств", 1),
    TRACKER("Трекеры здоровья", 2),
    ONB_INFO("Информация о себе", 4),
    ONB_TRACKER("Трекер здоровья", 4),
    ONB_DOCTOR("Запись к врачу", 4),
    ONB_HISTORY("История болезни", 4);

    private String description;
    private int order;
}