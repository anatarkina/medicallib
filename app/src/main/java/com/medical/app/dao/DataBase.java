package com.medical.app.dao;

import static com.medical.app.configs.ApiConstants.CommonParams.DB_NAME;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.History;
import com.medical.app.dao.entity.HistoryWithRecords;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.Pom;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.StreamEntity;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.User;

@Database(entities = {User.class, Tracker.class, Record.class,
        History.class, Pom.class, Pill.class, EventEntity.class, StreamEntity.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {

    private static DataBase INSTANCE;


    public abstract UserDao getUserDao();

    public abstract TrackerDao getTrackerDao();

    public abstract RecordDao getRecordDao();

    public abstract HistoryDao getHistoryDao();

    public abstract PomDao getPomDao();

    public abstract EventDao getEventDao();

    public abstract StreamDao getStreamDao();

    public static DataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    DataBase.class,
                                    DB_NAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
