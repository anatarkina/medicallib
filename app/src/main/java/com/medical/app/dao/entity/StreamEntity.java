package com.medical.app.dao.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.medical.app.dao.entity.converter.LocalDateTimeConverter;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@TypeConverters({LocalDateTimeConverter.class})
public class StreamEntity {
    @PrimaryKey(autoGenerate = true)
    Long id;
    String subDescription;
    String description;
    LocalDateTime createTime;

    public LocalDate getDate() {
        return createTime.toLocalDate();
    }
}
