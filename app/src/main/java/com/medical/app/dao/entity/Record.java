package com.medical.app.dao.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import com.medical.app.dao.entity.converter.LocalDateTimeConverter;
import com.medical.app.dao.entity.converter.RecordTypeConverter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@TypeConverters({RecordTypeConverter.class, LocalDateTimeConverter.class})
@Entity(
    foreignKeys = @ForeignKey(entity = History.class,
        parentColumns = {"id", "userId"},
        childColumns = {"history", "userId"},
        onUpdate = ForeignKey.CASCADE
    ))
public class Record {

  @PrimaryKey(autoGenerate = true)
  Long id;
  String userId;
  RecordType type;
  LocalDateTime date;
  String clinic, value;
  String history;
  String imageUrl;

  @Getter
  @AllArgsConstructor
  @NoArgsConstructor
  public enum RecordType {

    RECEIPT("Снимок рецепта", "Рецепт на лекарства", "Описание", "Врач"),
    ANALYSIS("Снимок анализ", "Результат анализа", "Анализ", "Клиника"),
    RESEARCH("Снимок исследования", "Протокол исследования", "Описание", "Врач"),
    CONCLUSION("Снимок заключения", "Заключение врача", "Диагноз", "Врач");

    String name;
    String stuff;
    String value;
    String ownerField;

    public static List<RecordType> recordTypes() {
      return Stream.of(RecordType.values()).collect(Collectors.toList());
    }
  }

}
