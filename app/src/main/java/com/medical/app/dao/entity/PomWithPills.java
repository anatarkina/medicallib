package com.medical.app.dao.entity;

import androidx.room.Embedded;
import androidx.room.Relation;
import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class PomWithPills {

  @Embedded
  public Pom pom;
  @Relation(parentColumn = "id", entity = Pill.class, entityColumn = "planId")
  @Builder.Default
  public List<Pill> pills = new ArrayList<>();

}
