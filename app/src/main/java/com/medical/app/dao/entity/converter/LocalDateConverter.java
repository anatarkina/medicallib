package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import org.joda.time.LocalDate;

public class LocalDateConverter {

    @TypeConverter
    public static LocalDate toDate(String dateString) {
        if (dateString == null) {
            return null;
        } else {
            return LocalDate.parse(dateString);
        }
    }

    @TypeConverter
    public static String toDateString(LocalDate date) {
        if (date == null) {
            return null;
        } else {
            return date.toString();
        }
    }
}
