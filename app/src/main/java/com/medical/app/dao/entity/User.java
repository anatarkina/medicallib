package com.medical.app.dao.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.medical.app.dao.entity.converter.LocalDateConverter;
import com.medical.app.dao.entity.converter.LocalDateTimeConverter;
import com.medical.app.dao.entity.converter.LocalTimeConverter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@TypeConverters({LocalDateConverter.class, LocalDateTimeConverter.class, LocalTimeConverter.class})
public class User {

    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String lastName;
    private String middleName;
    private String fullName;
    private String email;
    private LocalDate birthDate;
    private String address;
    private String avatarUrl;
    private String phone;
    private Integer gender;
    private String oms;
    private String dms;
    private String snils;
    @ColumnInfo(name = "current_user")
    private Boolean currentUser;
    private Integer docCityId;
    @Builder.Default
    private LocalTime notifyStart = new LocalTime(8, 0);
    @Builder.Default
    private LocalTime notifyEnd = new LocalTime(22, 0);
    @Builder.Default
    private Boolean notificationPills = true;
    @Builder.Default
    private Boolean notificationDoctor = true;
    @Builder.Default
    private Boolean doctorSaveToCalendar = true;
    @Builder.Default
    private Integer numberNotifications = 3;
}
