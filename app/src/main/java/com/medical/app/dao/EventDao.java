package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.medical.app.dao.entity.EventEntity;

import java.util.List;

@Dao
public interface EventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertTask(EventEntity record);

    @Query("SELECT * FROM EventEntity")
    LiveData<List<EventEntity>> fetchAll();

    @Query("SELECT * FROM EventEntity")
    List<EventEntity> fetchAllSync();

    @Transaction
    @Query("SELECT * FROM EventEntity WHERE id = :id")
    EventEntity getById(Long id);


    @Delete
    void delete(EventEntity record);
}
