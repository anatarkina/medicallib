package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.Pom;
import com.medical.app.dao.entity.PomWithPills;

import java.util.List;

@Dao
public abstract class PomDao {

    @Transaction
    @Query("SELECT * FROM Pom WHERE userId=:userId")
    public abstract LiveData<List<PomWithPills>> fetchAll(String userId);

    @Transaction
    @Query("SELECT * FROM Pill")
    public abstract List<Pill> fetchAll();

    @Transaction
    public void insert(PomWithPills pomWithPills) {
        final long pomId = insert(pomWithPills.pom);
        for (Pill pill : pomWithPills.pills) {
            pill.setPlanId(pomId);
            pill.setPomDiagnosis(pomWithPills.pom.getDiagnosis());
            insert(pill);
        }
    }

    @Transaction
    public void update(PomWithPills pomWithPills) {
        update(pomWithPills.pom);
        for (Pill pill : pomWithPills.pills) {
            pill.setPlanId(pomWithPills.pom.getId());
            pill.setPomDiagnosis(pomWithPills.pom.getDiagnosis());
            insert(pill);
        }
    }

    @Update
    @Transaction
    protected abstract void update(Pom pom);

    @Delete
    public abstract void delete(Pom pom);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    @Transaction
    public abstract long insert(Pom pom);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Transaction
    public abstract long insert(Pill pill);

    @Delete
    public abstract void delete(List<Pill> removedPills);
}
