package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.medical.app.dao.entity.StreamEntity;

import java.util.List;

@Dao
public interface StreamDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertTask(StreamEntity record);

    @Query("SELECT * FROM StreamEntity")
    LiveData<List<StreamEntity>> fetchAll();

    @Query("SELECT * FROM StreamEntity")
    List<StreamEntity> fetchAllSync();

    @Delete
    void delete(StreamEntity record);
}
