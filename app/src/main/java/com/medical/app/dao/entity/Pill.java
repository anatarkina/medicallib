package com.medical.app.dao.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.medical.app.R;
import com.medical.app.dao.entity.converter.LocalDateConverter;
import com.medical.app.dao.entity.converter.LocalDateTimeConverter;
import com.medical.app.dao.entity.converter.UnitsConverter;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.HashMap;
import java.util.Map;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.medical.app.utils.NullFunctions.ifNotEmptyGetString;
import static com.medical.app.utils.NullFunctions.ifNotNullGet;


@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Entity(
        foreignKeys = @ForeignKey(entity = Pom.class,
                parentColumns = {"id"},
                childColumns = {"planId"},
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE
        ))
@TypeConverters({LocalDateTimeConverter.class, LocalDateConverter.class})
public class Pill {

    @PrimaryKey(autoGenerate = true)
    Long id;

    Long planId;
    String pomDiagnosis;
    String dosage, duration;
    @NonNull
    @TypeConverters({LocalDateTimeConverter.class})
    LocalDateTime order;
    String name;
    String moreInfo;
    Integer frequency, count;
    LocalDateTime endDate;
    LocalDate updateTime;

    public void refreshCount() {
        count = frequency;
        updateTime = LocalDate.now();
    }

    @TypeConverters({UnitsConverter.class})
    Units units = new Units();


    public String getPillDescription(Integer order) {
        return "<p><b>" + order + ". " + this.getName() + ":</b> " +
                ifNotEmptyGetString(this.getDosage(),
                        dosage -> dosage + " " + this.getUnits().getUnitOfDosage().getShortDescription()
                ) +
                ifNotNullGet(this.getFrequency(),
                        frequency -> ", " + frequency + " в день") +
                ifNotEmptyGetString(this.getDuration(),
                        duration -> ", " + duration + " " + this.getUnits().getUnitOfDuration()
                                .getShortDescription()) + "</p>";
    }

    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @Builder
    @Data
    public static class Units {
        UnitOfDosage unitOfDosage;
        UnitOfDuration unitOfDuration;
    }

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public enum UnitOfDosage {
        PILL("Таблетки", "таб", R.id.dosage_pill),
        ML("Миллилитры", "мл", R.id.dosage_ml),
        UNIT("Штук", "шт", R.id.dosage_unit);

        private String description, shortDescription;
        private int type;
        private static final Map<Integer, UnitOfDosage> BY_TYPE = new HashMap<>();

        static {
            for (UnitOfDosage e : values()) {
                BY_TYPE.put(e.type, e);
            }
        }

        public static UnitOfDosage valueOf(Integer type) {
            return BY_TYPE.getOrDefault(type, null);
        }
    }

    @Getter
    @AllArgsConstructor
    public enum UnitOfDuration {
        DAY("дн.", R.id.duration_day), WEEK("нед.", R.id.duration_week), MONTH("мес.",
                R.id.duration_month);

        private String shortDescription;
        private int type;

        private static final Map<Integer, UnitOfDuration> BY_TYPE = new HashMap<>();

        static {
            for (UnitOfDuration e : values()) {
                BY_TYPE.put(e.type, e);
            }
        }

        public static UnitOfDuration valueOf(Integer type) {
            return BY_TYPE.getOrDefault(type, null);
        }
    }

}
