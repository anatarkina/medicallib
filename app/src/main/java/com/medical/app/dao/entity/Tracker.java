package com.medical.app.dao.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.medical.app.dao.entity.converter.DataSetConverter;
import com.medical.app.dao.entity.converter.TrackerTypeConverter;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity(primaryKeys = {"type", "userId"})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@TypeConverters({TrackerTypeConverter.class, DataSetConverter.class})
public class Tracker {

    public static final DateTimeFormatter FORMAT = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
    @NonNull
    TrackerType type;
    @Builder.Default
    List<DataSet> dataSets = new ArrayList<>();
    @NonNull
    String userId;
    Boolean show;

    @Getter
    @AllArgsConstructor
    public enum TrackerType {
        SUGAR_TRACKER("Уровень сахара"),
        WEIGHT_TRACKER("Вес тела"),
        HEIGHT_TRACKER("Рост"),
        IMPULSE_TRACKER("Давление");

        private String name;

        public static List<TrackerType> trackerTypes() {
            return Stream.of(TrackerType.values()).collect(Collectors.toList());
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor(staticName = "of")
    public static class DataSet {

        LocalDateTime date;
        DataSetType type;
        BigDecimal value;


        public String getFormattedDate() {
            return date.toString(FORMAT);
        }

        @Getter
        @AllArgsConstructor
        public enum DataSetType {
            SUGAR("Уровень сахара", "ммоль", "Текущий уровень", ""),
            WEIGHT("Вес", "кг", "Текущий вес", "Лучше утром натощак"),
            HEIGHT("Рост", "см", "Текущий рост", ""),
            UPPER_PRESSURE("Верхнее(систолическое)", "", "Верхнее(систолическое)", "В норме 100 - 130"),
            LOWER_PRESSURE("Нижнее(диастолическое)", "", "Нижнее(диастолическое)", "В норме 70 - 90"),
            PULSE("Пульс", "", "Пульс", "");

            private String name;
            private String measure;
            private String hint;
            private String helperText;
        }
    }
}
