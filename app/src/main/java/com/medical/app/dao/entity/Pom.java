package com.medical.app.dao.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import com.medical.app.dao.entity.converter.LocalDateTimeConverter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class Pom {

  @PrimaryKey(autoGenerate = true)
  Long id;

  @NonNull
  String userId;
  String diagnosis;
  @TypeConverters({LocalDateTimeConverter.class})
  LocalDateTime startDate, createDate, updateDate;

}
