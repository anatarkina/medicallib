package com.medical.app.dao.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.medical.app.dao.entity.converter.CardTypeConverter;
import com.medical.app.dao.entity.converter.LocalDateTimeConverter;

import org.joda.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@TypeConverters({LocalDateTimeConverter.class, CardTypeConverter.class})
public class EventEntity {
    @PrimaryKey(autoGenerate = true)
    Long id;
    private String topDescription;
    private String content;
    private CardType type;
    private boolean active;
    private LocalDateTime from;
    private LocalDateTime to;
}
