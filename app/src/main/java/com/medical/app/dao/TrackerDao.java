package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.Tracker.TrackerType;
import com.medical.app.dao.entity.converter.TrackerTypeConverter;
import java.util.List;

@Dao
public interface TrackerDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  Long insertTask(Tracker tracker);

  @Query("SELECT * FROM Tracker WHERE userId=:userId")
  LiveData<List<Tracker>> fetchAll(String userId);

  @Query("SELECT * FROM Tracker WHERE type =:type and userId=:userId")
  LiveData<Tracker> get(@TypeConverters({TrackerTypeConverter.class}) TrackerType type,
      String userId);

  @Update
  void update(Tracker user);

  @Delete
  void delete(Tracker user);

}
