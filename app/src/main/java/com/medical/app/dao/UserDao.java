package com.medical.app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import com.medical.app.dao.entity.User;
import java.util.List;

@Dao
public interface UserDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertTask(User user);


  @Query("SELECT * FROM User")
  LiveData<List<User>> fetchAll();


  @Query("SELECT * FROM User WHERE id =:userId")
  LiveData<User> getLiveData(String userId);

  @Query("SELECT * FROM User WHERE id =:userId")
  User get(String userId);

  @Update
  void update(User user);

  @Delete
  void delete(User user);
}
