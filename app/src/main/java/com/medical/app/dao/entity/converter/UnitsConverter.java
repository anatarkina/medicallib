package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.medical.app.dao.entity.Pill.Units;
import java.lang.reflect.Type;

public class UnitsConverter {

  @TypeConverter
  public String fromListToString(Units dataset) {
    if (dataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<Units>() {
    }.getType();
    String json = gson.toJson(dataset, type);
    return json;
  }

  @TypeConverter
  public Units fromStringToList(String strDataset) {
    if (strDataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<Units>() {
    }.getType();
    Units countryLangList = gson.fromJson(strDataset, type);
    return countryLangList;
  }

}
