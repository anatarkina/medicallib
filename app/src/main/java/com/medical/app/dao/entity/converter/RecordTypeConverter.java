package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.medical.app.dao.entity.Record.RecordType;
import java.lang.reflect.Type;

public class RecordTypeConverter {

  @TypeConverter
  public String fromListToString(RecordType dataset) {
    if (dataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<RecordType>() {
    }.getType();
    String json = gson.toJson(dataset, type);
    return json;
  }

  @TypeConverter
  public RecordType fromStringToList(String strDataset) {
    if (strDataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<RecordType>() {
    }.getType();
    RecordType countryLangList = gson.fromJson(strDataset, type);
    return countryLangList;
  }
}
