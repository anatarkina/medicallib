package com.medical.app.dao.entity;

import androidx.room.Embedded;
import androidx.room.Relation;
import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class HistoryWithRecords {

  @Embedded
  public History history;
  @Relation(parentColumn = "id", entity = Record.class, entityColumn = "history")
  @Builder.Default
  public List<Record> records = new ArrayList<>();
}
