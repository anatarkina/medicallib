package com.medical.app.dao.entity.converter;

import androidx.room.TypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.medical.app.dao.entity.Tracker.DataSet;
import com.medical.app.dao.entity.Tracker.TrackerType;
import java.lang.reflect.Type;
import java.util.List;

public class TrackerTypeConverter {

  @TypeConverter
  public String fromListToString(TrackerType dataset) {
    if (dataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<TrackerType>() {
    }.getType();
    String json = gson.toJson(dataset, type);
    return json;
  }

  @TypeConverter
  public TrackerType fromStringToList(String strDataset) {
    if (strDataset == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<TrackerType>() {
    }.getType();
    TrackerType countryLangList = gson.fromJson(strDataset, type);
    return countryLangList;
  }
}
