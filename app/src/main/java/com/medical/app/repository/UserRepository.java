package com.medical.app.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.medical.app.dao.DataBase;
import com.medical.app.dao.UserDao;
import com.medical.app.dao.entity.User;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

public class UserRepository {

    private UserDao userDao;

    public UserRepository(Context context) {
        DataBase db = DataBase.getDatabase(context);
        userDao = db.getUserDao();
    }


    public void insert(User newUser) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                userDao.insertTask(newUser);
                return null;
            }
        }.execute();
    }

    public void insertIfNotExists(User newUser) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                final User user = userDao.get(FirebaseAuth.getInstance().getUid());
                if (user == null) {
                    userDao.insertTask(newUser);
                }
                return null;
            }
        }.execute();
    }

    public LiveData<User> getCurrentUser() {
        return userDao.getLiveData(FirebaseAuth.getInstance().getUid());
    }

    public User getCurrent() {
        return userDao.get(FirebaseAuth.getInstance().getUid());
    }

    public User getUser(String uid) {
        return userDao.get(uid);
    }

    public void updateDocCityId(Integer cityId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                final User user = userDao.get(FirebaseAuth.getInstance().getUid());
                user.setDocCityId(cityId);
                userDao.insertTask(user);
                return null;
            }
        }.execute();
    }

    public void updateSettingsFields(LocalTime notifyStart, LocalTime notifyEnd, Boolean notificationPills,
                                     Boolean notificationDoctor, Boolean doctorSaveToCalendar,
                                     Integer notificationNumber) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                final User user = userDao.get(FirebaseAuth.getInstance().getUid());
                user.setNotifyStart(notifyStart);
                user.setNotifyEnd(notifyEnd);
                user.setNotificationPills(notificationPills);
                user.setNotificationDoctor(notificationDoctor);
                user.setDoctorSaveToCalendar(doctorSaveToCalendar);
                user.setNumberNotifications(notificationNumber);
                userDao.insertTask(user);
                return null;
            }
        }.execute();
    }

    public void updateCurrentFields(String lastNameText, String nameText, String middleNameText,
                                    LocalDate birthDayText, String addressText, String phoneText, String emailText,
                                    int genderGroup, String omsText, String dmsText, String snilsText) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                final User user = userDao.get(FirebaseAuth.getInstance().getUid());
                user.setLastName(lastNameText);
                user.setName(nameText);
                user.setMiddleName(middleNameText);
                user.setBirthDate(birthDayText);
                user.setAddress(addressText);
                user.setPhone(phoneText);
                user.setGender(genderGroup);
                user.setOms(omsText);
                user.setDms(dmsText);
                user.setEmail(emailText);
                user.setSnils(snilsText);
                user.setFullName(nameText + " " + lastNameText);
                userDao.insertTask(user);
                return null;
            }
        }.execute();
    }

    public void updateAvatar(String path) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                final User user = userDao.get(FirebaseAuth.getInstance().getUid());
                if (path != null) {
                    user.setAvatarUrl(path);
                }
                userDao.insertTask(user);
                return null;
            }
        }.execute();
    }

}
