package com.medical.app.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.medical.app.dao.DataBase;
import com.medical.app.dao.EventDao;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.EventEntity;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public class EventRepository {
    private EventDao eventDao;

    public EventRepository(Context context) {
        DataBase db = DataBase.getDatabase(context);
        eventDao = db.getEventDao();
    }

    public LiveData<List<EventEntity>> getAll() {
        return eventDao.fetchAll();
    }

    public List<EventEntity> getAllSync() {
        return eventDao.fetchAllSync();
    }

    public EventEntity findById(Long id) {
        return eventDao.getById(id);
    }

    public void insert(EventEntity eventEntity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                eventDao.insertTask(eventEntity);
                return null;
            }
        }.execute();
    }

    public Single<EventEntity> insertSingle(EventEntity eventEntity) {
        return Single.fromCallable(() -> {
            final Long id = eventDao.insertTask(eventEntity);
            eventEntity.setId(id);
            return eventEntity;
        });
    }

    public void deleteByType(CardType type) {
        eventDao.fetchAllSync()
                .stream()
                .filter(en -> en.getType().equals(type))
                .forEach(en -> eventDao.delete(en));
    }

}
