package com.medical.app.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.medical.app.dao.DataBase;
import com.medical.app.dao.StreamDao;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.StreamEntity;

import java.util.List;

public class StreamRepository {
    private StreamDao streamDao;

    public StreamRepository(Context context) {
        DataBase db = DataBase.getDatabase(context);
        streamDao = db.getStreamDao();
    }

    public LiveData<List<StreamEntity>> getAll() {
        return streamDao.fetchAll();
    }

    public void insert(StreamEntity streamEntity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                streamDao.insertTask(streamEntity);
                return null;
            }
        }.execute();
    }
}
