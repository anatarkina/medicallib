package com.medical.app.repository;

import static com.medical.app.dao.entity.History.DEFAULT_ID;
import static java.util.Optional.ofNullable;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.medical.app.dao.DataBase;
import com.medical.app.dao.HistoryDao;
import com.medical.app.dao.RecordDao;
import com.medical.app.dao.entity.History;
import com.medical.app.dao.entity.HistoryWithRecords;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.dto.SnapshotDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NonNull;

import org.joda.time.LocalDateTime;

public class RecordRepository {

    @Getter
    private LiveData<List<Record>> allRecords;

    private RecordDao recordDao;
    private HistoryDao historyDao;


    public RecordRepository(Context context) {
        DataBase db = DataBase.getDatabase(context);
        recordDao = db.getRecordDao();
        historyDao = db.getHistoryDao();
        final String uid = FirebaseAuth.getInstance().getUid();
        allRecords = recordDao.fetchAll(uid);
    }


    public void insertRecord(Record newRecord) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... records) {
                final HistoryWithRecords historyWithRecords = ofNullable(
                        historyDao.getByHistory(newRecord.getHistory(), newRecord.getUserId()))
                        .orElse(HistoryWithRecords.builder()
                                .history(History.builder()
                                        .id(newRecord.getHistory())
                                        .userId(newRecord.getUserId())
                                        .build())
                                .records(new ArrayList<>())
                                .build());
                historyWithRecords.getRecords().add(newRecord);
                historyDao.insert(historyWithRecords);
                return null;
            }
        }.execute();
    }

    public void deleteRecordById(Long id) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                recordDao.deleteById(id);
                return null;
            }
        }.execute();
    }

    public void deleteHistory(String history) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... trackers) {
                historyDao.deleteHistory(history, FirebaseAuth.getInstance().getUid(), "");
                return null;
            }
        }.execute();
    }

    public LiveData<List<Record>> getRecords(RecordType type) {
        return recordDao.getByTypes(type, FirebaseAuth.getInstance().getUid());
    }

    public LiveData<List<Record>> getRecordsByIds(List<Long> ids) {
        return recordDao.getAllByIds(ids);
    }

    public LiveData<List<HistoryWithRecords>> getHistories() {
        return historyDao.fetchAll(FirebaseAuth.getInstance().getUid());
    }

    public void updateHistories(List<SnapshotDto> snapshots,
                                @NonNull String history, boolean onlyUpdatable, @NonNull String previousHistory) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                snapshots.forEach(snapshotDto -> updateHistory(snapshotDto.getRecordId(),
                        snapshotDto.isSelected() ? history : DEFAULT_ID));
                if (!onlyUpdatable) {
                    deleteHistory(previousHistory);
                }
                return null;
            }
        }.execute();

    }

    private void updateHistory(Long id, String history) {
        final String uid = FirebaseAuth.getInstance().getUid();
        final History historyEntity = History.builder().id(history).userId(uid).build();
        historyDao.insert(historyEntity);
        recordDao.updateHistory(id, history);
    }

    public void safeUpdateRecord(Long id, LocalDateTime date,
                                 String owner, String historyInput, String imageUrl, String valueText) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                final Record current = recordDao.getCurrent(id);
                final String history = historyInput == null ? DEFAULT_ID : historyInput;
                current.setDate(date);
                current.setClinic(owner);
                current.setHistory(history);
                current.setImageUrl(imageUrl);
                current.setValue(valueText);
                final History historyEntity = History.builder()
                        .id(history)
                        .userId(FirebaseAuth.getInstance()
                                .getUid())
                        .build();
                historyDao.insert(historyEntity);
                historyDao.insert(current);
                return null;
            }
        }.execute();
    }

    public LiveData<Record> getRecord(Long id) {
        return recordDao.get(id);
    }


}
