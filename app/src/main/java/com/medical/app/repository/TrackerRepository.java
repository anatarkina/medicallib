package com.medical.app.repository;

import android.content.Context;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.medical.app.dao.DataBase;
import com.medical.app.dao.TrackerDao;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.Tracker.TrackerType;
import java.util.List;
import lombok.Getter;

public class TrackerRepository {

  @Getter
  private LiveData<List<Tracker>> allTrackers;

  private TrackerDao trackerDao;


  public TrackerRepository(Context context) {
    DataBase db = DataBase.getDatabase(context);
    trackerDao = db.getTrackerDao();
    final String uid = FirebaseAuth.getInstance().getUid();
    allTrackers = trackerDao.fetchAll(uid);
  }


  public void insertTracker(Tracker newTracker) {
    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... trackers) {
        trackerDao.insertTask(newTracker);
        return null;
      }
    }.execute();
  }

  public void deleteTracker(Tracker tracker) {
    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... trackers) {
        trackerDao.delete(tracker);
        return null;
      }
    }.execute();
  }

  public LiveData<Tracker> getTracker(TrackerType type) {
    return trackerDao.get(type, FirebaseAuth.getInstance().getUid());
  }
}
