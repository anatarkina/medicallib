package com.medical.app.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.medical.app.dao.DataBase;
import com.medical.app.dao.PomDao;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.Pom;
import com.medical.app.dao.entity.PomWithPills;

import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.medical.app.utils.NullFunctions.isBlankString;

public class PomRepository {

    private PomDao pomDao;

    public PomRepository(Context context) {
        DataBase db = DataBase.getDatabase(context);
        pomDao = db.getPomDao();
    }

    public LiveData<List<PomWithPills>> getPomsWithPills() {
        return pomDao.fetchAll(FirebaseAuth.getInstance().getUid());
    }

    public List<Pill> getPills() {
        return pomDao.fetchAll();
    }

    public void delete(PomWithPills pomWithPills) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                pomDao.delete(pomWithPills.getPom());
                return null;
            }
        }.execute();
    }

    public void insert(Map<String, Pill> pills, String currentDiagnosis) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                pomDao.insert(PomWithPills.builder()
                        .pom(Pom.builder()
                                .userId(FirebaseAuth.getInstance().getUid())
                                .diagnosis(currentDiagnosis)
                                .build())
                        .pills(getPills(pills))
                        .build());
                return null;
            }
        }.execute();
    }

    @NotNull
    private List<Pill> getPills(Map<String, Pill> pills) {
        return pills.values().stream()
                .peek(pill -> {
                    pill.setCount(pill.getFrequency());
                    pill.setUpdateTime(LocalDate.now());
                    Pill.UnitOfDuration duration = pill.getUnits().getUnitOfDuration();
                    LocalDateTime order = pill.getOrder();
                    Integer durationInteger = Integer.valueOf(pill.getDuration());
                    switch (duration) {
                        case DAY:
                            pill.setEndDate(order.plusDays(durationInteger));
                        case WEEK:
                            pill.setEndDate(order.plusWeeks(durationInteger));
                        case MONTH:
                            pill.setEndDate(order.plusMonths(durationInteger));
                    }
                })
                .collect(Collectors.toList());
    }

    public void update(Map<String, Pill> pills, Pom pom) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                pomDao.update(PomWithPills.builder()
                        .pom(Pom.builder()
                                .id(pom.getId())
                                .userId(FirebaseAuth.getInstance().getUid())
                                .diagnosis(pom.getDiagnosis())
                                .build())
                        .pills(getPills(pills))
                        .build());
                return null;
            }
        }.execute();
    }

    public void update(Pill pill) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                pomDao.insert(pill);
                return null;
            }
        }.execute();
    }

    public void deletePills(List<Pill> removedPills) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                pomDao.delete(removedPills);
                return null;
            }
        }.execute();
    }

    @NotNull
    public static Predicate<Pill> getPillDurationPredicate() {
        return pill -> {
            Pill.UnitOfDuration duration = pill.getUnits().getUnitOfDuration();
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime order = pill.getOrder();
            String durationParam = pill.getDuration();
            if (!isBlankString(durationParam)) {
                Integer durationInteger = Integer.valueOf(durationParam);
                switch (duration) {
                    case DAY:
                        return order.plusDays(durationInteger).isAfter(now);
                    case WEEK:
                        return order.plusWeeks(durationInteger).isAfter(now);
                    case MONTH:
                        return order.plusMonths(durationInteger).isAfter(now);
                }
                return false;
            }
            return false;
        };
    }

    public static Predicate<? super Pill> getPillCountPredicate() {
        return pill -> pill.getCount() > 0;
    }

}
