package com.medical.app;

import android.app.Application;
import android.util.Log;

import io.reactivex.rxjava3.plugins.RxJavaPlugins;

public class MimiApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        RxJavaPlugins.setErrorHandler(throwable -> {
            Log.e(this.getClass().getSimpleName(), "Error!", throwable);
        });
    }
}
