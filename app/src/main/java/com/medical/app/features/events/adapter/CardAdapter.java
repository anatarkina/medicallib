package com.medical.app.features.events.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.view.DoctorEventLayout;
import com.medical.app.view.OnBoardingLayout;
import com.medical.app.view.ListEventLayout;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder> {

    private List<EventEntity> dataModelList;
    private FragmentActivity mContext;

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_dashboard, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        final EventEntity card = dataModelList.get(position);
        holder.bindData(card, mContext);
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    public void removeAt(int position) {
        dataModelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataModelList.size());
    }

    public void setValues(List<EventEntity> eventEntities) {
        this.dataModelList = eventEntities;
        this.notifyDataSetChanged();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        public TextView topDescription;
        public RelativeLayout content;
        public MaterialCardView root;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.card_view);
            topDescription = itemView.findViewById(R.id.top_description);
            content = itemView.findViewById(R.id.content_container);
        }

        public void bindData(EventEntity dataModel, FragmentActivity context) {
            topDescription.setText(dataModel.getTopDescription());
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            params.addRule(RelativeLayout.BELOW, R.id.top_description);
            content.removeAllViews();
            content.addView(topDescription);
            switch (dataModel.getType()) {
                case DOCTOR:
                    DoctorEventLayout doctorEvent = new DoctorEventLayout(root, context, dataModel);
                    doctorEvent.setLayoutParams(params);
                    content.addView(doctorEvent);
                    break;
                case TABLETS:
                case TRACKER:
                    ListEventLayout tabletsEvent = new ListEventLayout(root, context, dataModel);
                    tabletsEvent.setLayoutParams(params);
                    content.addView(tabletsEvent);
                    break;
                case ONB_HISTORY:
                case ONB_TRACKER:
                case ONB_DOCTOR:
                case ONB_INFO:
                    OnBoardingLayout onBoardingLayout = new OnBoardingLayout(root, context, dataModel);
                    onBoardingLayout.setLayoutParams(params);
                    content.addView(onBoardingLayout);
                    break;
            }
        }
    }
}
