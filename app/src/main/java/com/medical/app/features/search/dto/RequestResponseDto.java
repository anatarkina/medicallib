package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestResponseDto {
    @SerializedName("Response")
    @Expose
    private Response response;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public class Response {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("id")
        @Expose
        private Integer id;
    }
}
