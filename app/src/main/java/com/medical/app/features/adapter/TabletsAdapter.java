package com.medical.app.features.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.google.android.material.chip.Chip;
import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.StreamEntity;
import com.medical.app.repository.PomRepository;
import com.medical.app.repository.StreamRepository;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class TabletsAdapter extends RecyclerView.Adapter<TabletsAdapter.TabletsViewHolder> {

    private List<Pill> data;
    private Context context;


    @NonNull
    @Override
    public TabletsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tablet, parent, false);
        return new TabletsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TabletsViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class TabletsViewHolder extends ViewHolder {

        public Chip view;

        public TabletsViewHolder(@NonNull View itemView) {
            super(itemView);
            view = (Chip) itemView;
        }

        public void bindData(Pill tablet) {
            view.setText(tablet.getName() + " " + tablet.getDosage() + " " + tablet.getUnits().getUnitOfDosage().getShortDescription());
            view.setOnCheckedChangeListener((buttonView, isChecked) -> {
                PomRepository pomRepository = new PomRepository(context);
                StreamRepository streamRepository = new StreamRepository(context);
                streamRepository.insert(StreamEntity.builder()
                        .description(tablet.getName() + " принят")
                        .createTime(LocalDateTime.now())
                        .build());
                Integer count = tablet.getCount();
                if (count != 0) {
                    tablet.setCount(--count);
                    tablet.setUpdateTime(LocalDate.now());
                }
                pomRepository.update(tablet);
                if (isChecked) {
                    Toast.makeText(view.getContext(), tablet.getName() + " принят", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
