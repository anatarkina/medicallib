package com.medical.app.features.adapter.history;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.dialog.Action;
import com.medical.app.dialog.SnapshotDialog;
import com.medical.app.repository.RecordRepository;
import com.medical.app.view.ImageOverlayView;
import com.stfalcon.imageviewer.StfalconImageViewer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.Singular;

import static com.medical.app.utils.DateUtils.JODA_DATE_FORMATTER;

public class HistoryRecordsAdapter extends
        RecyclerView.Adapter<HistoryRecordsAdapter.ImageViewHolder> {

    @Singular
    private List<Record> records = new ArrayList<>();
    @Singular
    private List<String> urls = new ArrayList<>();
    private FragmentActivity context;
    private RecordRepository recordRepository;

    @Builder
    public HistoryRecordsAdapter(List<Record> records, FragmentActivity context) {
        this.records = records;
        this.context = context;
        this.recordRepository = new RecordRepository(context);
        this.urls = records.stream().map(Record::getImageUrl)
                .collect(Collectors.toList());
    }


    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View layout = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history_record, viewGroup, false);
        layout.setPadding(0, 10, 10, 0);
        return new HistoryRecordsAdapter.ImageViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.bindData(context, recordRepository, records, urls, records.get(position), position);

    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public static class ImageViewHolder extends ViewHolder {

        private ImageView imageView;
        private TextView content;
        private MaterialCardView cardView;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.history_record_img);
            content = itemView.findViewById(R.id.content_txt);
            cardView = itemView.findViewById(R.id.card_item_history);
        }

        public void bindData(FragmentActivity context, RecordRepository recordRepository, List<Record> records, List<String> urls, Record record, int position) {
            Glide.with(context)
                    .load(record.getImageUrl())
                    .centerCrop()
                    .into(imageView);
            final RecordType type = record.getType();
            String contentTxt = null;
            switch (type) {
                case RECEIPT:
                case RESEARCH:
                case CONCLUSION:
                case ANALYSIS:
                    contentTxt = "<b><p>" + type.getStuff() + "</p></b>" +
                            "<p>" + record.getValue() + "</p>" +
                            type.getOwnerField() + ": " + record.getClinic();
                    break;

            }
            cardView.setOnClickListener(v -> {
                ImageOverlayView imageOverlayView = new ImageOverlayView(context);
                final StfalconImageViewer<String> dialog = getImageViewBuilder(context, records, urls, imageOverlayView, position)
                        .show();
                imageOverlayView.setPopUpClickListener(
                        getEditMenuListener(context, recordRepository, dialog, record));
            });
            content.setText(
                    Html.fromHtml(contentTxt, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
        }
    }

    private static PopupMenu.OnMenuItemClickListener getEditMenuListener(FragmentActivity context, RecordRepository recordRepository, StfalconImageViewer<String> dialog, Record dto) {
        return item -> {
            final int itemId = item.getItemId();
            switch (itemId) {
                case R.id.action_delete:
                    Toast
                            .makeText(context, String.format("Запись %s удалена!", dto.getHistory()),
                                    Toast.LENGTH_LONG)
                            .show();
                    recordRepository.deleteRecordById(dto.getId());
                    dialog.close();
                    break;
                case R.id.action_edit:
                    dialog.close();
                    SnapshotDialog
                            .builder()
                            .action(Action.EDIT)
                            .record(dto)
                            .imageUrl(Uri.parse(dto.getImageUrl()))
                            .recordType(dto.getType())
                            .historyText(dto.getHistory())
                            .build().show(context.getSupportFragmentManager(), SnapshotDialog.TAG);

                    break;
            }
            return true;
        };
    }

    private static StfalconImageViewer.Builder<String> getImageViewBuilder(FragmentActivity context, List<Record> records, List<String> urls,
                                                                           ImageOverlayView imageOverlayView, int position) {
        setOverlayContent(records, imageOverlayView, position);
        return new StfalconImageViewer.Builder<>(context, urls,
                (imageView, imageUrl) -> Glide.with(context).load(imageUrl).into(imageView))
                .withHiddenStatusBar(true)
                .withOverlayView(imageOverlayView)
                .withStartPosition(position)
                .allowZooming(true)
                .withImageChangeListener(num -> {
                    setOverlayContent(records, imageOverlayView, num);
                });
    }

    private static void setOverlayContent(List<Record> records, ImageOverlayView imageOverlayView, int position) {
        final Record dto = records.get(position);
        String url = dto.getImageUrl();
        imageOverlayView.setShareText(url);
        imageOverlayView.setBottomDescription(position + 1 + " / " + records.size());
        imageOverlayView.setTopDescription("#" + dto.getType().getStuff() + " #" + dto.getDate().toString(JODA_DATE_FORMATTER));
        imageOverlayView.setTopBottomDescription(dto.getClinic());
    }

}
