package com.medical.app.features.adapter;

import com.medical.app.dao.entity.Tracker.DataSet.DataSetType;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class ValuesBundle {

  private BigDecimal currentValue;
  private DataSetType dataSetType;

}
