package com.medical.app.features.home;

import static com.medical.app.utils.FabUtils.getFabItem;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.medical.app.R;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.features.MimiFragment;
import com.medical.app.features.adapter.TrackerAdapter;
import com.medical.app.repository.TrackerRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.Getter;

public class TrackerFragment extends Fragment implements SpeedDialView.OnActionSelectedListener,
        MimiFragment {

    @Getter
    private SpeedDialView fabMenu;
    private TrackerAdapter adapter;
    private TrackerRepository trackerRepository;
    private Map<Integer, Tracker> fabTrackers = new HashMap<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_tracker, null);
        RecyclerView recyclerView = view.findViewById(R.id.chart_list);
        fabMenu = view.findViewById(R.id.tracker_speed_dial);

        FragmentActivity activity = getActivity();
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        trackerRepository = new TrackerRepository(getContext());
        new Thread(() -> {
            adapter = TrackerAdapter.builder()
                    .context(getActivity())
                    .trackerRepository(trackerRepository)
                    .build();
            activity.runOnUiThread(() -> recyclerView.setAdapter(adapter));
        }).start();
        setupFabMenu();
        observerSetup();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return view;
    }

    private void setupFabMenu() {
        fabMenu.close(true);
        fabMenu.clearActionItems();
        fabMenu
                .setMainFabClosedDrawable(
                        ContextCompat.getDrawable(getContext(), R.drawable.icon_add));
        fabMenu.setOnActionSelectedListener(this);
    }


    private void observerSetup() {
        trackerRepository.getAllTrackers().observe(this, trackers -> {
            if (trackers != null) {
                if (trackers.size() == 0) {
                    fillTrackers(FirebaseAuth.getInstance().getUid());
                } else {
                    List<Tracker> trackersToShow = trackers.stream().filter(tracker -> tracker.getShow()).collect(Collectors.toList());
                    adapter.setTrackerList(
                            trackersToShow);
                    fabMenu.clearActionItems();
                    trackers.stream()
                            .forEach(this::addFabItem);
                    if (trackersToShow.isEmpty()) {
                        ViewTooltip
                                .on(this, fabMenu)
                                .autoHide(true, 1000)
                                .corner(50)
                                .color(getActivity().getColor(R.color.lightGrey))
                                .textColor(getActivity().getColor(R.color.back_button))
                                .textSize(TypedValue.COMPLEX_UNIT_SP, 16)
                                .position(ViewTooltip.Position.TOP)
                                .text("Нажми чтобы добавить")
                                .show();
                    }
                }
            }
        });
    }

    private void fillTrackers(String uid) {
        Tracker.TrackerType.trackerTypes()
                .stream()
                .forEach(
                        type -> trackerRepository
                                .insertTracker(Tracker.builder().userId(uid).show(false).type(type).build()));
    }

    private void addFabItem(Tracker tracker) {
        final Boolean show = tracker.getShow();
        final int id = View.generateViewId();
        final SpeedDialActionItem fabItem = getFabItem(id,
                show ? R.drawable.ic_check : R.drawable.icon_add, tracker.getType().getName(),
                ContextCompat.getColor(getContext(),
                        show ? android.R.color.white : R.color.colorExpandTextColor),
                ContextCompat.getColor(getContext(),
                        show ? R.color.colorExpandTextColor : android.R.color.white), getContext());
        fabTrackers.put(id, tracker);
        fabMenu.addActionItem(fabItem);
    }


    @Override
    public boolean onActionSelected(SpeedDialActionItem view) {
        int actionItemId = view.getId();
        final Tracker tracker = fabTrackers.get(actionItemId);
        tracker.setShow(!tracker.getShow());
        trackerRepository.insertTracker(tracker);
        return false;
    }
}





