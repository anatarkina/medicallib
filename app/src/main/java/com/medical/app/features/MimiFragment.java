package com.medical.app.features;

import static com.medical.app.utils.NullFunctions.ifNotNull;

import com.leinardi.android.speeddial.SpeedDialView;

public interface MimiFragment {


  default void clearFabMenu() {
    ifNotNull(getFabMenu(), fab -> fab.clearActionItems());
  }

  default SpeedDialView getFabMenu() {
    return null;
  }

}
