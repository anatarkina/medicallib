package com.medical.app.features.search.dto;


import java.util.Collection;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class DoctorsQuery {

    private String city;
    private String speciality;
    private String stations;
    private String near;
    private String order;
    private String lat;
    private String lng;
    private Integer radius;
    private Integer withSlots;
    private Integer slotsDays;
    private Map<String, MetroDto.Metro> metros;
}
