package com.medical.app.features.search.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.medical.app.R;
import com.medical.app.features.search.DoctorsViewHolder;
import com.medical.app.features.search.dto.DoctorListDto;

public class DoctorsAdapter extends PagedListAdapter<DoctorListDto.DoctorDto, DoctorsViewHolder> {

    private FragmentActivity activity;

    public DoctorsAdapter(DiffUtil.ItemCallback<DoctorListDto.DoctorDto> diffUtilCallback,
                          FragmentActivity activity) {
        super(diffUtilCallback);
        this.activity = activity;
    }

    @NonNull
    @Override
    public DoctorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = activity.getLayoutInflater().inflate(R.layout.item_doctor, null);
        DoctorsViewHolder holder = new DoctorsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorsViewHolder holder, int position) {
        final DoctorListDto.DoctorDto item = getItem(position);
        holder.bind(
                item, Lists.newArrayList(Iterables.limit(item.getStations(), 2)), activity,
                activity.getSupportFragmentManager());
    }
}