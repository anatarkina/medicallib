package com.medical.app.features.search.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.medical.app.R;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class MapDialog extends DialogFragment implements OnMapReadyCallback {
    private final String lat, lng;
    private final SearchOnMapClickListener onMapClickListener;
    private Button dismissBtn;
    private MapView mapView;
    private GoogleMap mMap;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_map, null);
        dismissBtn = dialogLayout.findViewById(R.id.dismiss_btn);
        mapView = dialogLayout.findViewById(R.id.map);
        mapView.getMapAsync(this);
        mapView.onCreate(savedInstanceState);
        dialog.setContentView(dialogLayout);
        dismissBtn.setOnClickListener((v) -> this.dismiss());
        return dialog;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMapClickListener(point -> {
            onMapClickListener.onMapClickListener(point.latitude, point.longitude);
            dismiss();
        });
        moveToMarker();
    }

    private void moveToMarker() {
        if (mMap != null) {
            MarkerOptions currentMarker = new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)))
                    .title("Текущее местоположение");
            mMap.clear();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentMarker.getPosition(), 15.0f));
            mMap.addMarker(currentMarker);
            mapView.onResume();
        }
    }
}
