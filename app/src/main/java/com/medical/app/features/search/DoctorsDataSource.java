package com.medical.app.features.search;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.paging.PositionalDataSource;

import com.medical.app.features.search.dialog.OnListLoadedCallback;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorListDto.ClinicsInfo;
import com.medical.app.features.search.dto.DoctorListDto.DoctorDto;
import com.medical.app.features.search.dto.DoctorListDto.Station;
import com.medical.app.features.search.dto.DoctorsQuery;
import com.medical.app.features.search.dto.MetroDto;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorsDataSource extends PositionalDataSource<DoctorDto> {

    private DocDocApi docDocApi;
    private DoctorsQuery query;
    private FragmentActivity context;
    private OnListLoadedCallback onListLoadedCallback;

    public DoctorsDataSource(DoctorsQuery query, FragmentActivity context,OnListLoadedCallback onListLoadedCallback) {
        this.docDocApi = DocDocService.getInstance().getJSONApi();
        this.query = query;
        this.context = context;
        this.onListLoadedCallback = onListLoadedCallback;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params,
                            @NonNull LoadInitialCallback<DoctorDto> callback) {
        Log.d(DoctorsDataSource.class.getSimpleName(),
                "loadInitial, requestedStartPosition = " + params.requestedStartPosition +
                        ", requestedLoadSize = " + params.requestedLoadSize);
        if (query != null) {
            Call<DoctorListDto> queryList = query.getStations() == null ?
                    docDocApi
                            .getDoctorListByLocation(params.requestedStartPosition, params.requestedLoadSize, query.getCity(),
                                    query.getSpeciality(), query.getNear(),
                                    query.getOrder(), query.getLat(), query.getLng(), query.getRadius(),
                                    query.getWithSlots(), query.getSlotsDays()) :
                    docDocApi
                            .getDoctorList(params.requestedStartPosition, params.requestedLoadSize, query.getCity(),
                                    query.getSpeciality(), query.getNear(),
                                    query.getOrder(), query.getRadius(),
                                    query.getWithSlots(), query.getSlotsDays(), query.getStations());
            queryList
                    .enqueue(new Callback<DoctorListDto>() {
                        @Override
                        public void onResponse(Call<DoctorListDto> call, Response<DoctorListDto> response) {
                            if (response.isSuccessful()) {
                                List<DoctorDto> doctorList = applyStations(response);
                                callback.onResult(doctorList, 0);
                                onListLoadedCallback.onListLoaded(doctorList);
                                if (doctorList.isEmpty()) {
                                    onResult("Поиск врачей не дал результата");
                                }
                            } else {
                                Log.e("Load doctors failed", response.message());
                                onListLoadedCallback.onListLoaded(null);
                                onResult("Не удалось получить список врачей");
                            }
                        }

                        @Override
                        public void onFailure(Call<DoctorListDto> call, Throwable t) {
                            String errorMessage = t == null ? "unknown error" : t.getMessage();
                            Log.e("Load doctors failed", errorMessage);
                            onListLoadedCallback.onListLoaded(null);
                            onResult("Не удалось получить список врачей");
                        }
                    });
        }
    }

    @NotNull
    private List<DoctorDto> applyStations(Response<DoctorListDto> response) {
        List<DoctorDto> doctorList = response.body().getDoctorList();
        doctorList.forEach(doctor -> {
            List<String> stations = doctor.getStations().stream().map(Station::getId).collect(Collectors.toList());
            doctor
                    .getClinicsInfo()
                    .stream()
                    .map(ClinicsInfo::getStations)
                    .flatMap(List::stream)
                    .filter(clinicStation -> !stations.contains(clinicStation))
                    .forEach(notContained -> {
                        MetroDto.Metro metro = query.getMetros().getOrDefault(notContained, null);
                        if (metro != metro) {
                            doctor.getStations().add(Station.builder()
                                    .id(metro.getId())
                                    .cityId(metro.getCityId())
                                    .lineColor(metro.getLineColor())
                                    .lineName(metro.getLineName())
                                    .name(metro.getName())
                                    .build());
                        }
                    });

        });
        return doctorList;
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params,
                          @NonNull LoadRangeCallback<DoctorDto> callback) {

        if (query != null) {
            Call<DoctorListDto> queryList = query.getStations() == null ?
                    docDocApi
                            .getDoctorListByLocation(params.startPosition, params.loadSize, query.getCity(),
                                    query.getSpeciality(), query.getNear(),
                                    query.getOrder(), query.getLat(), query.getLng(), query.getRadius(),
                                    query.getWithSlots(), query.getSlotsDays()) :
                    docDocApi
                            .getDoctorList(params.startPosition, params.loadSize, query.getCity(),
                                    query.getSpeciality(), query.getNear(),
                                    query.getOrder(), query.getRadius(),
                                    query.getWithSlots(), query.getSlotsDays(), query.getStations());
            queryList
                    .enqueue(new Callback<DoctorListDto>() {
                        @Override
                        public void onResponse(Call<DoctorListDto> call, Response<DoctorListDto> response) {
                            if (response.isSuccessful()) {
                                List<DoctorDto> doctorList = applyStations(response);
                                callback.onResult(doctorList);
                            } else {
                                Log.e("Load doctors failed", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<DoctorListDto> call, Throwable t) {
                            String errorMessage = t == null ? "unknown error" : t.getMessage();
                            Log.e("Load doctors failed", errorMessage);
                        }
                    });
        }
    }

    private void onResult(String message) {
        context.runOnUiThread(() -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show());
    }

}
