package com.medical.app.features.home;

public enum HomeTabs {
    DASHBOARD, TRACKER, STREAM;
}
