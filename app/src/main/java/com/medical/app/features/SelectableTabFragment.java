package com.medical.app.features;

import android.os.Bundle;

public interface SelectableTabFragment {
    void callWithArguments(Bundle bundle);
}
