package com.medical.app.features;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;

import java.util.ArrayList;

import static com.medical.app.configs.ApiConstants.CommonParams.PICK_CAMERA_IMAGE;

public abstract class ImageWorker extends DialogFragment {

    private Bundle bundle;

    public abstract void doWithImagePath(Uri uri, Bundle bundle);

    protected boolean showPictureDialogWithBundle(Bundle bundle) {
        this.bundle = bundle;
        takePhotoFromCamera();
        return true;
    }

    protected boolean showPictureDialog() {
        return showPictureDialogWithBundle(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PICK_CAMERA_IMAGE:
                    ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    if (!returnValue.isEmpty()) {
                        doWithImagePath(Uri
                                .parse("file:///" + returnValue.get(0)), bundle);
                    }
                    break;
            }
        }
    }

    protected void takePhotoFromCamera() {
        Options options = Options.init()
                .setRequestCode(PICK_CAMERA_IMAGE)
                .setCount(1)
                .setFrontfacing(false)
                .setExcludeVideos(true)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT);

        Pix.start(this, options);
    }
}
