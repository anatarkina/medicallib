package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Clinic {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ShortName")
    @Expose
    private String shortName;
    @SerializedName("RewriteName")
    @Expose
    private String rewriteName;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("StreetId")
    @Expose
    private String streetId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("House")
    @Expose
    private String house;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Logo")
    @Expose
    private String logo;
    @SerializedName("DistrictId")
    @Expose
    private String districtId;
    @SerializedName("Doctors")
    @Expose
    private List<String> doctors = null;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("ParentId")
    @Expose
    private Integer parentId;
    @SerializedName("BranchesId")
    @Expose
    private List<Object> branchesId = null;
    @SerializedName("OnlineRecordDoctor")
    @Expose
    private Boolean onlineRecordDoctor;
    @SerializedName("Stations")
    @Expose
    private List<Station> stations = null;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("RequestFormSurname")
    @Expose
    private Boolean requestFormSurname;
    @SerializedName("RequestFormBirthday")
    @Expose
    private Boolean requestFormBirthday;
    @SerializedName("RequestFormClientAge")
    @Expose
    private Boolean requestFormClientAge;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public class Station {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("LineId")
        @Expose
        private Integer lineId;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("LineColor")
        @Expose
        private String lineColor;
        @SerializedName("TimeWalking")
        @Expose
        private Integer timeWalking;
    }

}
