package com.medical.app.features.search;

import static com.medical.app.configs.ApiConstants.GoogleConst.GPS_REQUEST;
import static com.medical.app.configs.ApiConstants.GoogleConst.LOCATION_REQUEST;
import static com.medical.app.features.search.dto.DoctorListDto.DIFF_CALLBACK;
import static com.medical.app.utils.NullFunctions.checkNotNull;
import static java.util.Optional.ofNullable;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.features.search.adapter.DoctorsAdapter;
import com.medical.app.features.search.dialog.MapDialog;
import com.medical.app.features.search.dialog.OnListLoadedCallback;
import com.medical.app.features.search.dialog.SearchOnMapClickListener;
import com.medical.app.features.search.dto.City;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorSort;
import com.medical.app.features.search.dto.DoctorsQuery;
import com.medical.app.features.search.dto.MetroDto;
import com.medical.app.features.search.dto.SpecListDto.Spec;
import com.medical.app.features.search.dto.StationDto;
import com.medical.app.repository.UserRepository;
import com.medical.app.utils.GpsUtils;
import com.medical.app.view.ImageGetter;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.Getter;

public class SearchMainFragment extends Fragment implements SearchOnMapClickListener, OnListLoadedCallback {

    @Getter
    private AutoCompleteTextView listOfSpecialities, listOfStations, listOfCities;

    private DocDocApi docDocApi;
    private String defaultLat = "55.755826", defaultLng = "37.6172";
    private String lat = defaultLat;
    private String lng = defaultLng;

    private City currentCity;
    private Map<String, Spec> currentSpecs = new HashMap<>();
    private Map<String, MetroDto.Metro> metrosSpecs = new HashMap<>();
    private Map<String, City> citiesSpec = new HashMap<>();

    private RecyclerView doctorsList;
    private DoctorsAdapter doctorsAdapter;
    private DoctorsFactory doctorsFactory;
    private ChipGroup sortGroup;
    private UserRepository userRepository;
    private FusedLocationProviderClient client;
    private ArrayAdapter<String> specAdapter;
    private ArrayAdapter<String> metroAdapter;
    private ArrayAdapter<String> cityAdapter;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private GpsUtils gpsUtils;
    private TextView expandText;
    private Button searchBtn, mapBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_search_new, null);
        listOfSpecialities = view.findViewById(R.id.auto_complete_search);
        listOfStations = view.findViewById(R.id.auto_complete_search_st);
        listOfCities = view.findViewById(R.id.auto_complete_search_city);
        doctorsList = view.findViewById(R.id.doctors_list);
        sortGroup = view.findViewById(R.id.sort_group);
        expandText = view.findViewById(R.id.expand_txt_btn);
        searchBtn = view.findViewById(R.id.search_btn);
        mapBtn = view.findViewById(R.id.on_map_btn);

        slidingUpPanelLayout = view.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelState(PanelState.EXPANDED);
        checkExpandState(slidingUpPanelLayout.getPanelState());
        slidingUpPanelLayout.addPanelSlideListener(new PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {

            }

            @Override
            public void onPanelStateChanged(View view, PanelState panelState, PanelState panelState1) {
                checkExpandState(panelState1);
            }
        });

        userRepository = new UserRepository(getContext());
        client = LocationServices.getFusedLocationProviderClient(getContext());
        gpsUtils = new GpsUtils(getContext());

        specAdapter = new ArrayAdapter<>(getContext(),
                R.layout.dropdown_menu_popup_item, new ArrayList<>());
        metroAdapter = new ArrayAdapter<>(getContext(),
                R.layout.dropdown_menu_popup_item, new ArrayList<>());
        cityAdapter = new ArrayAdapter<>(getContext(),
                R.layout.dropdown_menu_popup_item, new ArrayList<>());
        listOfSpecialities.setAdapter(specAdapter);
        listOfStations.setAdapter(metroAdapter);
        listOfCities.setAdapter(cityAdapter);
        docDocApi = DocDocService
                .getInstance()
                .getJSONApi();
        createDoctorsFactory();
        searchBtn.setOnClickListener((v) -> fillDoctorsList());
        sortGroup.setOnCheckedChangeListener((chipGroup, checkId) -> fillDoctorsList());
        listOfSpecialities.setOnItemClickListener((adapterView, view1, i, l) -> {
            hideKeyBoard();
            listOfSpecialities.clearFocus();
            checkIfCanSearch();
        });
        listOfStations.setOnItemClickListener((adapterView, view1, i, l) -> {
            hideKeyBoard();
            listOfStations.clearFocus();
            checkIfCanSearch();
        });
        listOfCities.setOnItemClickListener((adapterView, view1, i, l) -> {
            hideKeyBoard();
            listOfCities.clearFocus();
            applySelectedCity();
            checkIfCanSearch();
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        doctorsAdapter = new DoctorsAdapter(DIFF_CALLBACK, getActivity());
        doctorsList.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                doctorsList.getContext(),
                layoutManager.getOrientation());
        doctorsList.addItemDecoration(dividerItemDecoration);
        doctorsList.setAdapter(doctorsAdapter);
        fillSpecList();
        fillCityList();
        FirebaseAnalytics.getInstance(getContext())
                .setCurrentScreen(getActivity(), this.getClass().getSimpleName(),
                        this.getClass().getSimpleName());
        gpsUtils.turnGPSOn(isGPSEnable -> {
            getLocation();
        });
        mapBtn.setOnClickListener(v -> {
            MapDialog.of(lat, lng, this).show(getChildFragmentManager(), MapDialog.class.getSimpleName());
        });
        checkIfCanSearch();
        return view;
    }

    @Override
    public void onListLoaded(List<DoctorListDto.DoctorDto> doctorList) {
        if (ofNullable(doctorList).orElse(Collections.emptyList()).isEmpty()) {
            sortGroup.setVisibility(View.GONE);
        } else {
            sortGroup.setVisibility(View.VISIBLE);
            slidingUpPanelLayout.setPanelState(PanelState.COLLAPSED);
            checkExpandState(slidingUpPanelLayout.getPanelState());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        slidingUpPanelLayout.setPanelState(PanelState.EXPANDED);
        checkExpandState(slidingUpPanelLayout.getPanelState());
        checkIfCanSearch();
        applyCurrentCity();
    }

    private void checkExpandState(PanelState panelState) {
        if (panelState.equals(PanelState.COLLAPSED)) {
            setExpandedText("<img src='ic_down_arrow' />");
        } else if (panelState.equals(PanelState.EXPANDED)) {
            setExpandedText("<img src='ic_up_arrow' />");
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST);

        } else {
            client.requestLocationUpdates(gpsUtils.getLocationRequest(), mLocationCallback, Looper.myLooper());
        }
    }

    protected void setExpandedText(String icon) {
        expandText.setText(
                Html.fromHtml(icon, Html.FROM_HTML_MODE_LEGACY,
                        ImageGetter.of(getContext()),
                        null));
    }

    private void applySelectedCity() {
        final City city = citiesSpec.getOrDefault(listOfCities.getText().toString(), null);
        if (city != null) {
            currentCity = city;
            lat = city.getLatitude();
            lng = city.getLongitude();
            docDocApi.getStations(currentCity.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(e -> onResult("Не удалось получить список станций"))
                    .subscribe(metroDto -> {
                        metrosSpecs = metroDto.getMetroList().stream()
                                .collect(
                                        Collectors.toMap(MetroDto.Metro::getName, Function.identity(), (p, q) -> p));
                        metroAdapter.clear();
                        metroAdapter
                                .addAll(metrosSpecs.keySet().stream().sorted().collect(Collectors.toList()));
                        listOfStations.setText("");
                    });
        }
    }

    private void fillCityList() {
        docDocApi.getCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citiesList -> {
                    citiesSpec = citiesList.getCityList().stream()
                            .collect(Collectors.toMap(City::getName, Function.identity()));
                    cityAdapter.clear();
                    cityAdapter.addAll(citiesSpec.keySet().stream().sorted().collect(Collectors.toList()));
                    listOfCities.setText("");
                }, e -> onResult("Не удалось получить список доступных городов"));
    }

    private void fillSpecList() {
        docDocApi.getSpeciality()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(specList -> {
                    currentSpecs = specList.getSpecList().stream()
                            .collect(Collectors.toMap(Spec::getName, Function.identity()));
                    specAdapter.clear();
                    specAdapter.addAll(currentSpecs.keySet().stream().sorted().collect(Collectors.toList()));
                    listOfSpecialities.setText("");
                }, e -> onResult("Не удалось получить список доступных специльностей"));
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (client != null) {
            client.removeLocationUpdates(mLocationCallback);
        }
    }

    private void createDoctorsFactory() {
        doctorsFactory = new DoctorsFactory(null, getActivity(), this);
        // PagedList
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(15)
                .build();

        LiveData<PagedList<DoctorListDto.DoctorDto>> pagedListLiveData = new LivePagedListBuilder<>(
                doctorsFactory,
                config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build();

        pagedListLiveData
                .observe(getViewLifecycleOwner(), pagedList -> doctorsAdapter.submitList(pagedList));
    }


    private String getSorting() {
        final String currentSelected = DoctorSort.byId(sortGroup.getCheckedChipId())
                .getSort();
        Log.i(SearchMainFragment.class.getSimpleName(), "Current selected chip: " + currentSelected);
        return currentSelected;
    }

    protected void applyCurrentCity() {
        if (checkNotNull(lat, lng)) {
            docDocApi.detectCity(lat, lng)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(e -> onResult("Не удалось получить текущий город"))
                    .flatMap(city -> {
                        currentCity = city.getCity();
                        return docDocApi.getStations(currentCity.getId());
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(metroDto -> {
                        metrosSpecs = metroDto.getMetroList().stream()
                                .collect(
                                        Collectors.toMap(MetroDto.Metro::getName, Function.identity(), (p, q) -> p));
                        metroAdapter.clear();
                        metroAdapter
                                .addAll(metrosSpecs.keySet().stream().sorted().collect(Collectors.toList()));
                        listOfStations.setText("");

                        return docDocApi.getNearestStationGeo(lat, lng);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(nearest -> {
                        ofNullable(nearest)
                                .map(StationDto::getStation)
                                .ifPresent(station -> {
                                    listOfStations.setText(station.getName());
                                    listOfCities.setText(currentCity.getName());
                                });
                    }, e -> onResult("Не удалось определить ближайшую к вам станцию метро"));
        }
    }

    private void checkIfCanSearch() {
        if (currentSpecs != null && citiesSpec != null) {
            final Spec spec = currentSpecs.getOrDefault(listOfSpecialities.getText().toString(), null);
            final City city = citiesSpec.getOrDefault(listOfCities.getText().toString(), null);
            if (spec != null && city != null && doctorsFactory != null) {
                searchBtn.setEnabled(true);
                return;
            }
        }
        searchBtn.setEnabled(false);
    }

    private boolean fillDoctorsList() {
        if (docDocApi != null && doctorsFactory != null) {
            applyListOfDoctors();
            return true;
        }
        return false;
    }

    private void applyListOfDoctors() {
        if (currentSpecs != null && metrosSpecs != null) {
            final Spec spec = currentSpecs.getOrDefault(listOfSpecialities.getText().toString(), null);
            final MetroDto.Metro metro = metrosSpecs
                    .getOrDefault(listOfStations.getText().toString(), null);
            final City city = citiesSpec.getOrDefault(listOfCities.getText().toString(), null);

            if (spec != null && currentCity != null && doctorsFactory != null) {
                userRepository.updateDocCityId(Integer.valueOf(currentCity.getId()));
                DoctorsQuery.DoctorsQueryBuilder builder = DoctorsQuery.builder()
                        .city(city != null ? city.getId() : currentCity.getId())
                        .speciality(spec.getId())
                        .near("strict")
                        .order(getSorting())
                        .radius(7)
                        .withSlots(1)
                        .metros(metrosSpecs.values().stream()
                                .collect(Collectors.toMap(MetroDto.Metro::getId, Function.identity())))
                        .slotsDays(30);
                if (metro != null) {
                    builder.stations(metro.getId());
                } else {
                    builder.lat(lat);
                    builder.lng(lng);
                }
                doctorsFactory.changeQuery(builder
                        .build());
            }
        }
    }


    private void onResult(String message) {
        Toast.makeText(getContext(),
                message,
                Toast.LENGTH_SHORT)
                .show();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapBtn.setEnabled(true);
                    client.requestLocationUpdates(gpsUtils.getLocationRequest(), mLocationCallback,
                            Looper.myLooper());
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                    mapBtn.setEnabled(false);
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GPS_REQUEST) {
                client.requestLocationUpdates(gpsUtils.getLocationRequest(), mLocationCallback,
                        Looper.myLooper());
                Toast.makeText(getActivity(), "Permission granted via GpsUtils", Toast.LENGTH_SHORT).show();
                mapBtn.setEnabled(true);
            }
        }
    }


    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i(this.getClass().getSimpleName(),
                        "Location: " + location.getLatitude() + " " + location.getLongitude());
                if (!checkNotNull(lat, lng) || (defaultLat.equalsIgnoreCase(lat) && defaultLng
                        .equalsIgnoreCase(lng))) {
                    lat = "" + location.getLatitude();
                    lng = "" + location.getLongitude();
                    applyCurrentCity();
                }
            }
        }
    };

    @Override
    public void onMapClickListener(double latitude, double longitude) {
        lat = "" + latitude;
        lng = "" + longitude;
        applyCurrentCity();
    }
}
