package com.medical.app.features.home;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.common.base.Strings;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.User;
import com.medical.app.features.ImageWorker;
import com.medical.app.repository.UserRepository;
import com.medical.app.view.DateEditTextView;

import org.joda.time.LocalDate;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.medical.app.utils.NullFunctions.ifNotNull;
import static com.medical.app.utils.NullFunctions.ifNotNullGet;
import static com.medical.app.utils.NullFunctions.isBlankString;
import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;

public class ProfileDialog extends ImageWorker {
    private static final String TAG = "PROFILE_EDITOR";

    @BindView(R.id.avatarImg)
    ImageView avatar;
    @BindView(R.id.nameText)
    TextView nameText;
    @BindView(R.id.lastNameText)
    TextView lastNameText;
    @BindView(R.id.middleNameText)
    TextView middleNameText;
    @BindView(R.id.fio)
    TextView fio;
    @BindView(R.id.birthDayText)
    DateEditTextView birthDayText;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.emailText)
    TextView emailText;
    @BindView(R.id.addressText)
    TextView addressText;
    @BindView(R.id.takePhotoImg)
    ImageView takePhoto;
    @BindView(R.id.omsText)
    TextView omsText;
    @BindView(R.id.dmsText)
    TextView dmsText;
    @BindView(R.id.snilsText)
    TextView snilsText;
    @BindView(R.id.genderGroup)
    RadioGroup genderGroup;
    @BindView(R.id.save_btn)
    Button save;

    @BindView(R.id.decline_btn)
    Button decline;

    private UserRepository userRepository;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_profile, null);
        userRepository = new UserRepository(getActivity());

        ButterKnife.bind(this, dialogLayout);
        loadText();
        save.setOnClickListener(v -> update());
        decline.setOnClickListener(v -> {
            this.dismiss();
        });
        takePhoto.setOnClickListener((btn) -> {
            showPictureDialog();
        });

        dialog.setContentView(dialogLayout);
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), TAG, ProfileDialog.class.getSimpleName());

        return dialog;
    }

    private void loadText() {
        userRepository.getCurrentUser().observe(this, current -> {
            if (current != null) {
                nameText.setText(current.getName());
                lastNameText.setText(current.getLastName());
                middleNameText.setText(current.getMiddleName());
                fio.setText(ofNullable(current).map(User::getLastName).orElse("") + " "
                        + ofNullable(current).map(User::getName).orElse("") + " "
                        + ofNullable(current).map(User::getMiddleName).orElse(""));
                ifNotNull(current.getBirthDate(), date -> birthDayText.setFormattedText(date));
                addressText.setText(current.getAddress());
                phoneText.setText(current.getPhone());
                emailText.setText(current.getEmail());
                genderGroup.check(ofNullable(current.getGender()).orElse(R.id.radioF));
                omsText.setText(current.getOms());
                dmsText.setText(current.getDms());
                snilsText.setText(current.getSnils());
                final String userAvatar = current.getAvatarUrl();
                if (!Strings.isNullOrEmpty(userAvatar)) {
                    Glide.with(this).load(userAvatar)
                            .error(R.drawable.ic_default_profile_avatar).into(avatar);
                }
            }
        });
    }

    public void update() {
        if (isValid()) {
            userRepository.updateCurrentFields(
                    lastNameText.getText().toString(),
                    nameText.getText().toString(),
                    middleNameText.getText().toString(),
                    birthDayText.getDate(),
                    addressText.getText().toString(),
                    phoneText.getText().toString(),
                    emailText.getText().toString(),
                    genderGroup.getCheckedRadioButtonId(),
                    omsText.getText().toString(),
                    dmsText.getText().toString(),
                    snilsText.getText().toString()
            );
            this.dismiss();
        }
    }

    private boolean isValid() {
        boolean shouldSave = true;
        String name = nameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        LocalDate birthDay = birthDayText.getDate();
        String phone = ifNotNullGet(phoneText.getText().toString(), ph -> ph.replaceAll("\\+7 \\(", ""));

        if (isBlankString(name)) {
            nameText.setError("Поле должно быть заполнено");
            shouldSave = false;
        }
        if (isBlankString(lastName)) {
            lastNameText.setError("Поле должно быть заполнено");
            shouldSave = false;
        }
        if (isNull(birthDay)) {
            birthDayText.setError("Поле должно быть заполнено");
            shouldSave = false;
        }
        if (isBlankString(phone)) {
            phoneText.setError("Поле должно быть заполнено");
            shouldSave = false;
        }
        return shouldSave;
    }

    @Override
    public void doWithImagePath(Uri uri, Bundle bundle) {
        ofNullable(uri).ifPresent(u -> saveImageUri(u.getPath()));
    }

    private void saveImageUri(String path) {
        userRepository.updateAvatar(path);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takePhoto.setEnabled(true);
            }
        }
    }
}
