package com.medical.app.features.adapter;

import static com.medical.app.utils.DateUtils.JODA_DATE_FORMATTER;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dialog.Action;
import com.medical.app.dialog.SnapshotDialog;
import com.medical.app.repository.RecordRepository;
import com.medical.app.view.ImageOverlayView;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.StfalconImageViewer.Builder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SnapshotsAdapter extends BaseAdapter {

  private FragmentActivity context;
  private List<Record> snapshots = new ArrayList<>();
  private List<String> urls = new ArrayList<>();
  private RecordRepository recordRepository;
  private ImageOverlayView imageOverlayView;

  private SnapshotsAdapter(FragmentActivity context, List<Record> snapshots) {
    this.context = context;
    this.snapshots = snapshots;
    this.recordRepository = new RecordRepository(context);
    imageOverlayView = new ImageOverlayView(context);
    urls = snapshots.stream().map(Record::getImageUrl)
        .collect(Collectors.toList());
  }


  public static SnapshotsAdapter of(FragmentActivity context, List<Record> imageList) {
    return new SnapshotsAdapter(context, imageList);
  }

  public int getCount() {
    if (snapshots != null) {
      return this.snapshots.size();
    }
    return 0;
  }

  public void setSnapshots(List<Record> snapshots) {
    this.snapshots = snapshots;
    urls = snapshots.stream().map(Record::getImageUrl)
        .collect(Collectors.toList());
    this.notifyDataSetChanged();
  }

  public Record getItem(int position) {
    return snapshots.get(position);
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }


  public View getView(int position, View convertView, ViewGroup parent) {
    final Record dto = snapshots.get(position);
    final View layout = LayoutInflater.from(context)
        .inflate(R.layout.item_snapshot, parent, false);
    final ViewSwitcher switcher = layout.findViewById(R.id.switcher);
    final ImageView image = layout.findViewById(R.id.snapshot_image);
    final TextView text = layout.findViewById(R.id.snapshot_title);
    text.setText(dto.getType().getValue());
    Glide.with(context)
        .load(dto.getImageUrl())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(image);

    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... trackers) {
        context.runOnUiThread(() -> layout.setOnClickListener(view -> {
          imageOverlayView = new ImageOverlayView(context);
          final StfalconImageViewer<String> dialog = getImageViewBuilder(imageOverlayView, position)
              .show();
          imageOverlayView.setPopUpClickListener(
              getEditMenuListener(dto.getId(), dialog, dto));
        }));
        return null;
      }

      @Override
      protected void onPostExecute(Void result) {
        switcher.showNext();
      }
    }.execute();

    return layout;
  }

  private OnMenuItemClickListener getEditMenuListener(Long recordId,
      StfalconImageViewer<String> dialog, Record dto) {
    return item -> {
      final int itemId = item.getItemId();
      switch (itemId) {
        case R.id.action_delete:
          Toast
              .makeText(context, String.format("Запись %s удалена!", dto.getHistory()),
                  Toast.LENGTH_LONG)
              .show();
          recordRepository.deleteRecordById(recordId);
          dialog.close();
          break;
        case R.id.action_edit:
          dialog.close();
          SnapshotDialog
              .builder()
              .action(Action.EDIT)
              .record(dto)
              .imageUrl(Uri.parse(dto.getImageUrl()))
              .recordType(dto.getType())
              .historyText(dto.getHistory())
              .build().show(context.getSupportFragmentManager(), SnapshotDialog.TAG);

          break;
      }
      return true;
    };
  }

  private Builder<String> getImageViewBuilder(
      ImageOverlayView imageOverlayView, int position) {
    setOverlayContent(imageOverlayView, position);
    return new Builder<>(context, urls,
        (imageView, imageUrl) -> Glide.with(context).load(imageUrl).into(imageView))
        .withHiddenStatusBar(true)
        .withOverlayView(imageOverlayView)
        .withStartPosition(position)
        .allowZooming(true)
        .withImageChangeListener(num -> {
          setOverlayContent(imageOverlayView, num);
        });
  }

  private void setOverlayContent(ImageOverlayView imageOverlayView, int position) {
    final Record dto = snapshots.get(position);
    String url = dto.getImageUrl();
    imageOverlayView.setShareText(url);
    imageOverlayView.setBottomDescription(position + 1 + " / " + snapshots.size());
    imageOverlayView.setTopDescription("#" + dto.getType().getStuff() + " #" + dto.getDate().toString(JODA_DATE_FORMATTER));
    imageOverlayView.setTopBottomDescription(dto.getClinic());
  }
}
