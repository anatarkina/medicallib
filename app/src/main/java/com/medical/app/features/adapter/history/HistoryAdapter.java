package com.medical.app.features.adapter.history;

import static com.medical.app.utils.NullFunctions.ifNotNullGet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.medical.app.R;
import com.medical.app.dialog.HistoryDialog;
import com.medical.app.dto.HistoryDto;
import com.medical.app.repository.RecordRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

  @Builder.Default
  @Getter
  private List<HistoryDto> values = new ArrayList<>();
  private Context context;
  private FragmentManager fragmentManager;
  private RecordRepository recordRepository;

  @NonNull
  @Override
  public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.item_card_history, viewGroup, false);
    return new HistoryViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
    holder.bindData(values.get(position), context, fragmentManager, recordRepository);
  }

  public void setHistories(List<HistoryDto> values) {
    this.values = values;
    this.notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    return values.size();
  }

  public void removeItem(int position) {
    values.remove(position);
    notifyDataSetChanged();
  }

  public void restoreItem(HistoryDto item, int position) {
    values.add(position, item);
    notifyDataSetChanged();
  }

  public static class HistoryViewHolder extends RecyclerView.ViewHolder {

    private TextView topDescription, content;
    private RecyclerView historyList;
    private Button expandButton, deleteButton;

    public HistoryViewHolder(@NonNull View itemView) {
      super(itemView);
      topDescription = itemView.findViewById(R.id.top_description);
      content = itemView.findViewById(R.id.content_txt);
      historyList = itemView.findViewById(R.id.history_list);
      expandButton = itemView.findViewById(R.id.expand_btn);
      deleteButton = itemView.findViewById(R.id.delete_btn);
    }

    public void bindData(HistoryDto model, Context context, FragmentManager fragmentManager,
        RecordRepository recordRepository) {
      topDescription.setText(model.getHistory());

      content.setText(ifNotNullGet(model.getValue(), value -> value + ": " + model
          .getValueDescription() + "\n", "") +
          ifNotNullGet(model.getDate(), date -> "Дата: " + date + "\n", "") +
          ifNotNullGet(model.getOwner(),
              owner -> owner + ": " + model.getOwnerDescription() + "\n", ""));
      historyList
          .setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
      historyList
          .setAdapter(HistoryImageAdapter.builder()
              .context(context)
              .size(model.getValues().size())
              .urls(model.getUrls())
              .build());
      expandButton.setOnClickListener(view -> {
        HistoryDialog.of(model.getValues(), model.getHistory())
            .show(fragmentManager, HistoryDialog.TAG);
      });

      deleteButton.setOnClickListener(view -> {
        recordRepository.deleteHistory(model.getHistory());
        Toast
            .makeText(context, String.format("История %s удалена!", model.getHistory()),
                Toast.LENGTH_LONG)
            .show();
      });
    }
  }
}
