package com.medical.app.features.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.StreamEntity;
import com.medical.app.dto.StreamDto;
import com.medical.app.features.adapter.stream.StreamByDateAdapter;
import com.medical.app.repository.StreamRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

public class StreamFragment extends Fragment {
    private RecyclerView historyList;
    private StreamRepository streamRepository;
    private StreamByDateAdapter streamAdapter;
    private TextView activeText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(
                R.layout.fragment_stream, container, false);
        historyList = view.findViewById(R.id.history_list);
        activeText = view.findViewById(R.id.active_txt);
        historyList.setLayoutManager(new LinearLayoutManager(getActivity()));
        streamAdapter = StreamByDateAdapter.builder().context(getContext()).build();
        historyList.setAdapter(streamAdapter);
        streamRepository = new StreamRepository(getContext());
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(),
                this.getClass().getSimpleName(), this.getClass().getSimpleName());

        fillValues();
        return view;
    }

    private void fillValues() {
        streamRepository.getAll().observe(getViewLifecycleOwner(), streamEntities -> {
            List<StreamDto> values = ofNullable(streamEntities).orElse(emptyList())
                    .stream()
                    .collect(Collectors.groupingBy(StreamEntity::getDate)).entrySet()
                    .stream()
                    .map(entry -> StreamDto.builder()
                            .date(entry.getKey())
                            .values(entry.getValue())
                            .build())
                    .sorted(Comparator.comparing(StreamDto::getDate).reversed())
                    .collect(Collectors.toList());
            streamAdapter.setValues(values);
            if (activeText != null) {
                if (values.isEmpty()) {
                    activeText.setVisibility(View.VISIBLE);
                } else {
                    activeText.setVisibility(View.GONE);
                }
            }
        });
    }

}
