package com.medical.app.features.search.adapter;

import android.widget.TextView;

import com.medical.app.R;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class AchievementGroup {

    private List<AchievementChild> children;
    private Type type;

    @NoArgsConstructor
    @Builder
    @AllArgsConstructor
    @Data
    public static class AchievementChild {
        private String startYear;
        private String endYear;
        private String position;
        private String organisation;
    }

    public enum Type {
        EXPERIENCE, EDUCATION, COURSES;
    }

}
