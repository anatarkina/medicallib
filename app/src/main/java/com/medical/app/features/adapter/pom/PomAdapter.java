package com.medical.app.features.adapter.pom;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.dialog.PomCreateDialog;
import com.medical.app.dialog.PomViewDialog;
import com.medical.app.repository.PomRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;

@AllArgsConstructor
@Builder
public class PomAdapter extends RecyclerView.Adapter<PomAdapter.ViewHolder> {

    @Builder.Default
    private List<PomWithPills> values = new ArrayList<>();
    private Context context;
    private FragmentManager fragmentManager;
    private PomRepository pomRepository;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card_pom, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(values.get(position), fragmentManager);
    }

    public void setPoms(List<PomWithPills> values) {
        this.values = values;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView historyList;
        private Button expandButton, editButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            historyList = itemView.findViewById(R.id.pills_list);
            expandButton = itemView.findViewById(R.id.expand_btn);
            editButton = itemView.findViewById(R.id.edit_btn);
        }

        public void bindData(PomWithPills pom, FragmentManager fragmentManager) {
            int limit = 3;
            AtomicInteger index = new AtomicInteger(1);
            historyList.setText(Html.fromHtml(
                    "<h1>" + pom.getPom().getDiagnosis() + "</h1>" +
                            pom.getPills()
                                    .stream()
                                    .sorted(Comparator.comparing(Pill::getOrder))
                                    .limit(limit)
                                    .map(pill -> pill.getPillDescription(index.getAndIncrement()))
                                    .collect(Collectors.joining()) +
                            getLimit(pom.getPills().size(), limit)
                    , Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
            expandButton.setOnClickListener(
                    v -> PomViewDialog.of(pom).show(fragmentManager, PomViewDialog.class.getName()));
            editButton.setOnClickListener(v -> PomCreateDialog.withPills(pom)
                    .show(fragmentManager, PomCreateDialog.class.getName()));
        }

        private String getLimit(int size, int limit) {
            if (size > limit) {
                final int dif = size - limit;
                String end = (dif == 1 ? "о" : dif < 5 ? "а" : "");
                return String.format("<b>+%d лекарств%s</b>", dif, end);
            }
            return "";
        }
    }
}
