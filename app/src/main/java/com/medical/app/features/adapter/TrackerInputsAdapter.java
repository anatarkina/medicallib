package com.medical.app.features.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.medical.app.R;
import com.medical.app.activity.bundle.InputsBundle;
import com.medical.app.dao.entity.Tracker.DataSet.DataSetType;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.medical.app.utils.NullFunctions.isBlankString;

@AllArgsConstructor(staticName = "of")
public class TrackerInputsAdapter extends
        RecyclerView.Adapter<TrackerInputsAdapter.InputsViewHolder> {

    @Getter
    private final List<InputsBundle> inputs = new ArrayList<>();
    private List<ValuesBundle> dataModelList;

    @NonNull
    @Override
    public InputsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.input_tracker, parent, false);
        // Return a new view holder
        return new InputsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InputsViewHolder holder, int position) {
        holder.bindData(dataModelList.get(position));
    }

    public void setInputs(List<InputsBundle> inputs) {
        this.inputs.clear();
        this.inputs.addAll(inputs);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    public void removeAt(int position) {
        dataModelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataModelList.size());
    }

    public class InputsViewHolder extends ViewHolder {

        private TextInputLayout passTextInputLayout;
        private TextInputEditText edtPass;
        private TextView currentValue;

        public InputsViewHolder(@NonNull View itemView) {
            super(itemView);
            passTextInputLayout = itemView.findViewById(R.id.add_new_value_field);
            edtPass = itemView.findViewById(R.id.add_new_value);
            currentValue = itemView.findViewById(R.id.current_value_txt);
        }

        public void bindData(ValuesBundle bundle) {
            final DataSetType type = bundle.getDataSetType();
            currentValue.setText(bundle.getCurrentValue().toString());
            inputs.add(InputsBundle.of(type, passTextInputLayout, edtPass));

            passTextInputLayout.setHint(type.getHint());
            passTextInputLayout.setHelperText(type.getHelperText());
            passTextInputLayout.setHelperTextEnabled(!isBlankString(type.getHelperText()));
        }
    }


}
