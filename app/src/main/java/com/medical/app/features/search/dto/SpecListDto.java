package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class SpecListDto {

  @SerializedName("SpecList")
  @Expose
  private List<Spec> specList;

  @NoArgsConstructor
  @AllArgsConstructor
  @Data
  public class Spec {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Alias")
    @Expose
    private String alias;
    @SerializedName("NameGenitive")
    @Expose
    private String nameGenitive;
    @SerializedName("NamePlural")
    @Expose
    private String namePlural;
    @SerializedName("NamePluralGenitive")
    @Expose
    private String namePluralGenitive;
    @SerializedName("IsSimple")
    @Expose
    private Boolean isSimple;
    @SerializedName("BranchName")
    @Expose
    private String branchName;
    @SerializedName("BranchAlias")
    @Expose
    private String branchAlias;
  }
}
