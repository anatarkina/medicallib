package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class RequestDto {
    @Expose
    private Integer city;
    @Expose
    private Integer doctor;
    @Expose
    private Integer clinic;
    @Expose
    private String slotId;
    @Expose
    private String phone;
    @Expose
    private String name;
    @Expose
    private Integer clientAge;
    @Expose
    private String clientBirthday;
}
