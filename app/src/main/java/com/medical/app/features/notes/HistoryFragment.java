package com.medical.app.features.notes;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.HistoryWithRecords;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.dto.HistoryDto;
import com.medical.app.features.adapter.SwipeToDeleteCallback;
import com.medical.app.features.adapter.history.HistoryAdapter;
import com.medical.app.repository.RecordRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.medical.app.dao.entity.History.DEFAULT_ID;
import static com.medical.app.utils.DateUtils.JODA_DATE_FORMATTER;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

public class HistoryFragment extends Fragment {

    private HistoryAdapter adapter;
    private RecordRepository recordRepository;
    private RecyclerView recyclerView;
    private TextView activeText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_history, container, false);
        recyclerView = view.findViewById(R.id.history_list);
        activeText = view.findViewById(R.id.active_txt);
        final FragmentActivity activity = getActivity();
        recordRepository = new RecordRepository(activity);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        new Thread(() -> {
            adapter = HistoryAdapter.builder().context(activity).fragmentManager(getChildFragmentManager())
                    .recordRepository(recordRepository)
                    .build();
            activity.runOnUiThread(() -> recyclerView.setAdapter(adapter));
        }).start();
        observerSetup();
        //enableSwipeToDeleteAndUndo();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return view;
    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(getContext()) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                final int position = viewHolder.getAdapterPosition();
                final HistoryDto item = adapter.getValues().get(position);

                adapter.removeItem(position);

                Snackbar snackbar = Snackbar
                        .make(recyclerView, "Item was removed from the list.", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", view -> {
                    adapter.restoreItem(item, position);
                    recyclerView.scrollToPosition(position);
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();

            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    private void observerSetup() {
        recordRepository.getHistories().observe(getViewLifecycleOwner(), records -> {
            List<HistoryWithRecords> values = ofNullable(records).orElse(emptyList());
            adapter.setHistories(getHistoryFromRecords(values));
            if (activeText != null) {
                if (values.isEmpty()) {
                    activeText.setVisibility(View.VISIBLE);
                } else {
                    activeText.setVisibility(View.GONE);
                }
            }
        });
    }

    private List<HistoryDto> getHistoryFromRecords(List<HistoryWithRecords> records) {
        return records
                .stream()
                .filter(history -> !DEFAULT_ID.equalsIgnoreCase(history.getHistory().getId()))
                .map(entry -> getHistoryFromEntry(entry))
                .collect(Collectors.toList());
    }

    private HistoryDto getHistoryFromEntry(HistoryWithRecords entry) {
        final List<Record> values = entry.getRecords();
        final Record lastRecord = values
                .stream()
                .sorted(Comparator.comparing(Record::getDate).reversed())
                .findAny().orElse(null);
        final List<String> imageUrls = values.stream().map(Record::getImageUrl)
                .limit(5)
                .collect(Collectors.toList());

        return HistoryDto.builder()
                .date(ofNullable(lastRecord).map(Record::getDate)
                        .map(date -> date.toString(JODA_DATE_FORMATTER)).orElse(null))
                .history(entry.getHistory().getId())
                .owner(
                        ofNullable(lastRecord).map(Record::getType).map(RecordType::getOwnerField).orElse(null))
                .ownerDescription(ofNullable(lastRecord).map(Record::getClinic).orElse(null))
                .value(ofNullable(lastRecord).map(Record::getType).map(RecordType::getValue).orElse(null))
                .valueDescription(ofNullable(lastRecord).map(Record::getValue).orElse(null))
                .urls(imageUrls)
                .values(values)
                .build();
    }
}
