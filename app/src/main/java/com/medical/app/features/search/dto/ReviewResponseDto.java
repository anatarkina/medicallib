package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class ReviewResponseDto {
    @SerializedName("ReviewList")
    @Expose
    private List<ReviewList> reviewList = null;
    @SerializedName("ReviewRating")
    @Expose
    private ReviewRating reviewRating;

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Rating {

        @SerializedName("RatingValue")
        @Expose
        private BigDecimal ratingValue;
        @SerializedName("RatingCount")
        @Expose
        private BigDecimal ratingCount;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class ReviewList {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Client")
        @Expose
        private String client;
        @SerializedName("RatingQlf")
        @Expose
        private BigDecimal ratingQlf;
        @SerializedName("RatingAtt")
        @Expose
        private BigDecimal ratingAtt;
        @SerializedName("RatingRoom")
        @Expose
        private BigDecimal ratingRoom;
        @SerializedName("Text")
        @Expose
        private String text;
        @SerializedName("Date")
        @Expose
        private LocalDate date;
        @SerializedName("DoctorId")
        @Expose
        private Integer doctorId;
        @SerializedName("ClinicId")
        @Expose
        private Integer clinicId;
        @SerializedName("Answer")
        @Expose
        private Object answer;
        @SerializedName("WaitingTime")
        @Expose
        private BigDecimal waitingTime;
        @SerializedName("RatingDoctor")
        @Expose
        private BigDecimal ratingDoctor;
        @SerializedName("RatingDoctorLabel")
        @Expose
        private String ratingDoctorLabel;
        @SerializedName("RatingClinic")
        @Expose
        private Integer ratingClinic;
        @SerializedName("TagClinicLocation")
        @Expose
        private Boolean tagClinicLocation;
        @SerializedName("TagClinicService")
        @Expose
        private Boolean tagClinicService;
        @SerializedName("TagClinicCost")
        @Expose
        private Boolean tagClinicCost;
        @SerializedName("TagClinicRecommend")
        @Expose
        private Boolean tagClinicRecommend;
        @SerializedName("TagDoctorAttention")
        @Expose
        private Boolean tagDoctorAttention;
        @SerializedName("TagDoctorExplain")
        @Expose
        private Boolean tagDoctorExplain;
        @SerializedName("TagDoctorQuality")
        @Expose
        private Boolean tagDoctorQuality;
        @SerializedName("TagDoctorRecommend")
        @Expose
        private Boolean tagDoctorRecommend;
        @SerializedName("TagDoctorSatisfied")
        @Expose
        private Boolean tagDoctorSatisfied;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class ReviewRating {
        @SerializedName("RatingValue")
        @Expose
        private BigDecimal ratingValue;
        @SerializedName("RatingCount")
        @Expose
        private BigDecimal ratingCount;
        @SerializedName("Rating")
        @Expose
        private List<Rating> rating = null;
    }
}
