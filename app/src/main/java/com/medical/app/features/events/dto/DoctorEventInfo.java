package com.medical.app.features.events.dto;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.medical.app.dao.entity.converter.LocalTimeSerializer;

import org.joda.time.LocalDateTime;

import java.lang.reflect.Type;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DoctorEventInfo {
    private String img;
    private String imgFormat;
    private String clinic;
    private List<Stations> stations;
    private String street;
    private LocalDateTime startTime;
    private String fioDoctor;
    private String ratingDescription;
    private Float rating;
    private String doctorPhoneNumber;
    private String doctorBranch;

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Stations {
        private String name;
        private String color;
        private Integer timeWalking;
        private Integer distanceWalking;
    }

    public static String toString(DoctorEventInfo dataset) {
        if (dataset == null) {
            return (null);
        }
        final GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalTimeSerializer());
        Gson gson = builder.create();
        Type type = new TypeToken<DoctorEventInfo>() {
        }.getType();
        String json = gson.toJson(dataset, type);
        return json;
    }

    public static DoctorEventInfo toObject(String str) {
        if (str == null) {
            return (null);
        }
        final GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalTimeSerializer());
        Gson gson = builder.create();
        Type type = new TypeToken<DoctorEventInfo>() {
        }.getType();
        DoctorEventInfo object = gson.fromJson(str, type);
        return object;
    }
}
