package com.medical.app.features.search;

import com.medical.app.features.search.dto.CitiesResponseDto;
import com.medical.app.features.search.dto.ClinicList;
import com.medical.app.features.search.dto.ClinicListResponseDto;
import com.medical.app.features.search.dto.DetectCityDto;
import com.medical.app.features.search.dto.ClinicListDto;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorResponseDto;
import com.medical.app.features.search.dto.MetroDto;
import com.medical.app.features.search.dto.RequestDto;
import com.medical.app.features.search.dto.RequestResponseDto;
import com.medical.app.features.search.dto.ReviewResponseDto;
import com.medical.app.features.search.dto.SpecListDto;

import com.medical.app.features.search.dto.StationDto;
import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DocDocApi {

    @GET(DocDocService.BASE_PATH + "detectCity/lat/{lat}/lng/{lng}?pid=" + DocDocService.PID)
    Observable<DetectCityDto> detectCity(@Path("lat") String lat, @Path("lng") String lng);

    @GET(DocDocService.BASE_PATH + "speciality?pid=" + DocDocService.PID)
    Observable<SpecListDto> getSpeciality();

    @GET(DocDocService.BASE_PATH + "nearestStationGeo/lat/{lat}/lng/{lng}?pid=" + DocDocService.PID)
    Observable<StationDto> getNearestStationGeo(@Path("lat") String lat, @Path("lng") String lng);


    @GET(DocDocService.BASE_PATH + "metro/city/{city}?pid=" + DocDocService.PID)
    Observable<MetroDto> getStations(@Path("city") String city);

    @GET(DocDocService.BASE_PATH + "doctor/list/start/{start}/count/{count}/city/{city}"
            + "/speciality/{speciality}/near/{near}/order/{order}/withSlots"
            + "/{withSlots}/slotsDays/{slotsDays}/radius/{radius}/stations/{stations}?pid="
            + DocDocService.PID)
    Call<DoctorListDto> getDoctorList(@Path("start") Integer start, @Path("count") Integer count,
                                      @Path("city") String city, @Path("speciality") String speciality,
                                      @Path("near") String near, @Path("order") String order,
                                      @Path("radius") Integer radius, @Path("withSlots") Integer withSlots,
                                      @Path("slotsDays") Integer slotsDays,
                                      @Path("stations") String stations);

    @GET(DocDocService.BASE_PATH + "doctor/list/start/{start}/count/{count}/city/{city}"
            + "/speciality/{speciality}/near/{near}/order/{order}/withSlots"
            + "/{withSlots}/slotsDays/{slotsDays}/lat/{lat}/lng/{lng}/radius/{radius}?pid="
            + DocDocService.PID)
    Call<DoctorListDto> getDoctorListByLocation(@Path("start") Integer start, @Path("count") Integer count,
                                                @Path("city") String city, @Path("speciality") String speciality,
                                                @Path("near") String near, @Path("order") String order, @Path("lat") String lat,
                                                @Path("lng") String lng,
                                                @Path("radius") Integer radius, @Path("withSlots") Integer withSlots,
                                                @Path("slotsDays") Integer slotsDays);


    @POST(DocDocService.BASE_PATH + "request?pid=" + DocDocService.PID)
    Call<RequestResponseDto> submitRequest(@Body RequestDto request);

    @GET(DocDocService.BASE_PATH + "review/doctor/{id}?pid=" + DocDocService.PID)
    Observable<ReviewResponseDto> getReview(@Path("id") Integer id);

    @GET(DocDocService.BASE_PATH + "clinic/{clinic}?pid=" + DocDocService.PID)
    Observable<ClinicListDto> getClinic(@Path("clinic") String clinic);

    @GET(DocDocService.BASE_PATH + "city?pid=" + DocDocService.PID)
    Observable<CitiesResponseDto> getCities();

    @GET(DocDocService.BASE_PATH + "clinic/list/clinicId/{clinic}?pid=" + DocDocService.PID)
    Observable<ClinicListResponseDto> getClinicList(@Path("clinic") Integer clinic);

    @GET(DocDocService.BASE_PATH + "doctor/{id}?pid="
            + DocDocService.PID)
    Observable<DoctorResponseDto> getDoctorInfo(@Path("id") Integer id);
}
