package com.medical.app.features.adapter.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.medical.app.R;
import com.medical.app.dto.RecordByDate;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Singular;

@AllArgsConstructor
@Builder
public class HistoryByDateAdapter extends
        RecyclerView.Adapter<HistoryByDateAdapter.ImageViewHolder> {

    private List<RecordByDate> records = new ArrayList<>();
    private FragmentActivity context;


    @NonNull
    @Override
    public HistoryByDateAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
                                                                   int i) {
        final View layout = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history_by_date, viewGroup, false);
        layout.setPadding(0, 10, 10, 0);
        return new HistoryByDateAdapter.ImageViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryByDateAdapter.ImageViewHolder holder, int position) {
        holder.bindData(context, records.get(position));
    }

    public void updateValues(List<RecordByDate> ent) {
        this.records.clear();
        records.addAll(ent);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public static class ImageViewHolder extends ViewHolder {

        private TextView topDescription;
        private RecyclerView listOfRecords;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            topDescription = itemView.findViewById(R.id.top_description);
            listOfRecords = itemView.findViewById(R.id.list_of_records);
        }

        public void bindData(FragmentActivity context, RecordByDate recordByDate) {
            listOfRecords.setLayoutManager(new LinearLayoutManager(context));
            topDescription
                    .setText(recordByDate.getDate());
            listOfRecords.setAdapter(
                    HistoryRecordsAdapter.builder().context(context).records(recordByDate.getRecords())
                            .build());
        }
    }
}
