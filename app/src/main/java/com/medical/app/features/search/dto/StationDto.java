package com.medical.app.features.search.dto;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class StationDto {

  @SerializedName("Station")
  @Expose
  private Station station;

  @NoArgsConstructor
  @AllArgsConstructor
  @Data
  public class Station {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("LineName")
    @Expose
    private String lineName;

    @SerializedName("LineColor")
    @Expose
    private String lineColor;

    @SerializedName("CityId")
    @Expose
    private String cityId;


    @SerializedName("Alias")
    @Expose
    private String alias;

    @SerializedName("DistrictIds")
    @Expose
    private List<String> districtId;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
  }

}
