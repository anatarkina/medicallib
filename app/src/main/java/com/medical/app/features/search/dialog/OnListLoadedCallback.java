package com.medical.app.features.search.dialog;

import com.medical.app.features.search.dto.DoctorListDto;

import java.util.List;

public interface OnListLoadedCallback {
    void onListLoaded(List<DoctorListDto.DoctorDto> doctorList);
}
