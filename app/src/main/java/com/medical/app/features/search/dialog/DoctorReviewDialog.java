package com.medical.app.features.search.dialog;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.medical.app.R;
import com.medical.app.features.search.DocDocApi;
import com.medical.app.features.search.DocDocService;
import com.medical.app.features.search.adapter.AchievementGroup;
import com.medical.app.features.search.adapter.DoctorReviewAdapter;
import com.medical.app.features.search.adapter.ExpListAdapter;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorResponseDto;
import com.medical.app.features.search.dto.ReviewResponseDto;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.medical.app.utils.NullFunctions.checkNotNull;
import static com.medical.app.utils.NullFunctions.isBlankString;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class DoctorReviewDialog extends DialogFragment {

    @NonNull
    private DoctorListDto.DoctorDto doctor;
    private DocDocApi docDocApi;
    private Button hide;
    private LinearLayout doctorInfo;
    private RecyclerView commentsList;
    private TextView fioDoctor;
    private ExpandableListView expListView;
    private List<RatingBundle> ratings = new ArrayList<>();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_doctor_review, null);
        commentsList = dialogLayout.findViewById(R.id.comments_list);
        doctorInfo = dialogLayout.findViewById(R.id.list_of_doctor_info);
        fioDoctor = dialogLayout.findViewById(R.id.fio_doctor);
        hide = dialogLayout.findViewById(R.id.hide_btn);
        expListView = dialogLayout.findViewById(R.id.expandableListView);
        expListView.setOnGroupClickListener((parent, v, groupPosition, id) -> {
            setListViewHeight(parent, groupPosition);
            return false;
        });
        ratings.add(RatingBundle.of(BigDecimal.valueOf(5.0), dialogLayout.findViewById(R.id.ratingTxt5)));
        ratings.add(RatingBundle.of(BigDecimal.valueOf(4.0), dialogLayout.findViewById(R.id.ratingTxt4)));
        ratings.add(RatingBundle.of(BigDecimal.valueOf(3.0), dialogLayout.findViewById(R.id.ratingTxt3)));
        ratings.add(RatingBundle.of(BigDecimal.valueOf(2.0), dialogLayout.findViewById(R.id.ratingTxt2)));
        ratings.add(RatingBundle.of(BigDecimal.valueOf(1.0), dialogLayout.findViewById(R.id.ratingTxt1)));
        ratings.add(RatingBundle.of(BigDecimal.valueOf(0.0), dialogLayout.findViewById(R.id.ratingTxt0)));


        hide.setOnClickListener(v -> dialog.dismiss());
        commentsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        docDocApi = DocDocService
                .getInstance()
                .getJSONApi();

        fillValues();
        dialog.setContentView(dialogLayout);
        return dialog;
    }

    private void fillValues() {
        fioDoctor.setText(doctor.getName());
        if (!isBlankString(doctor.getDegree()) || !isBlankString(doctor.getCategory())) {
            LinearLayout.LayoutParams lpView = getLinearLayoutParams();
            TextView degree = new TextView(getContext());
            degree.setText(isBlankString(doctor.getDegree()) ? doctor.getCategory() : doctor.getDegree());
            lpView.setMargins(5, 10, 0, 0);
            degree.setLayoutParams(lpView);
            degree.setTextSize(16);
            doctorInfo.addView(degree);
        }
        if (checkNotNull(doctor.getSpecialities())) {
            LinearLayout.LayoutParams lpView = getLinearLayoutParams();
            TextView branchFullInfo = new TextView(getContext());
            lpView.setMargins(0, 10, 5, 0);
            branchFullInfo.setText(ofNullable(doctor.getSpecialities()).orElse(emptyList())
                    .stream().map(sp -> sp.getName()).collect(Collectors.joining(", ")));
            branchFullInfo.setLayoutParams(lpView);
            branchFullInfo.setTextSize(16);
            branchFullInfo.setBreakStrategy(Layout.BREAK_STRATEGY_HIGH_QUALITY);
            branchFullInfo.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getContext(),
                    R.drawable.ic_icons8_medical_doctor_18), null, null, null);
            branchFullInfo.setCompoundDrawablePadding(5);
            branchFullInfo.setPadding(5, 0, 0, 0);
            doctorInfo.addView(branchFullInfo);
        }
        if (checkNotNull(doctor.getExperienceYear())) {
            LinearLayout.LayoutParams lpView = getLinearLayoutParams();
            TextView experience = new TextView(getContext());
            lpView.setMargins(0, 10, 0, 0);
            experience.setText(doctor.getExperienceYear() + " г. практики");
            experience.setLayoutParams(lpView);
            experience.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getContext(),
                    R.drawable.ic_icons8_cv), null, null, null);
            experience.setCompoundDrawablePadding(5);
            experience.setPadding(5, 0, 0, 0);
            experience.setTextSize(16);
            doctorInfo.addView(experience);
        }
        if (!isBlankString(doctor.getDescription())) {
            LinearLayout.LayoutParams lpView = getLinearLayoutParams();
            TextView description = new TextView(getContext());
            lpView.setMargins(0, 10, 0, 0);
            description.setText(doctor.getDescription());
            description.setLayoutParams(lpView);
            description.setTextSize(16);
            doctorInfo.addView(description);
        }
        callInfo();
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        BaseExpandableListAdapter listAdapter = (BaseExpandableListAdapter) expListView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
                //Add Divider Height
                totalHeight += listView.getDividerHeight() * (listAdapter.getChildrenCount(i) - 1);
            }
        }
        //Add Divider Height
        totalHeight += listView.getDividerHeight() * (listAdapter.getGroupCount() - 1);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void callInfo() {

        docDocApi.getDoctorInfo(doctor.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(doctorResponseDto -> {
                    ofNullable(doctorResponseDto)
                            .map(DoctorResponseDto::getDoctorList)
                            .orElse(emptyList())
                            .stream()
                            .findAny()
                            .ifPresent(doctor -> {
                                DoctorResponseDto.Achievements achievements = doctor.getAchievements();
                                List<DoctorResponseDto.Course> courses = ofNullable(achievements).map(DoctorResponseDto.Achievements::getCourses).orElse(emptyList());
                                List<DoctorResponseDto.Education> educations = ofNullable(achievements).map(DoctorResponseDto.Achievements::getEducation).orElse(emptyList());
                                List<DoctorResponseDto.Experience> experiences = ofNullable(achievements).map(DoctorResponseDto.Achievements::getExperience).orElse(emptyList());
                                List<AchievementGroup> mGroups = new ArrayList<>();
                                if (!experiences.isEmpty()) {
                                    mGroups.add(AchievementGroup.builder()
                                            .type(AchievementGroup.Type.EXPERIENCE)
                                            .children(experiences.stream()
                                                    .map(expValue -> AchievementGroup.AchievementChild.builder()
                                                            .startYear(isBlankString(expValue.getYearBegin()) ? "      " : expValue.getYearBegin() + " г.")
                                                            .endYear(isBlankString(expValue.getYearEnd()) ? "н.в." : expValue.getYearEnd() + " г.")
                                                            .position(expValue.getCity() + "." + " " + expValue.getPosition())
                                                            .organisation(expValue.getOrganization())
                                                            .build()).collect(Collectors.toList()))
                                            .build());
                                }
                                if (!educations.isEmpty()) {
                                    mGroups.add(AchievementGroup.builder()
                                            .type(AchievementGroup.Type.EDUCATION)
                                            .children(educations.stream()
                                                    .map(expValue -> AchievementGroup.AchievementChild.builder()
                                                            .startYear(isBlankString(expValue.getYear()) ? "      " : expValue.getYear() + " г.")
                                                            .position(expValue.getType() + ". " + expValue.getSpecialization())
                                                            .organisation(expValue.getName())
                                                            .build()).collect(Collectors.toList()))
                                            .build());
                                }

                                if (!courses.isEmpty()) {
                                    mGroups.add(AchievementGroup.builder()
                                            .type(AchievementGroup.Type.COURSES)
                                            .children(courses.stream()
                                                    .map(expValue -> AchievementGroup.AchievementChild.builder()
                                                            .startYear(isBlankString(expValue.getYear()) ? "      " : expValue.getYear() + " г.")
                                                            .position(expValue.getName())
                                                            .organisation(expValue.getOrganization())
                                                            .build()).collect(Collectors.toList()))
                                            .build());
                                }
                                expListView.setAdapter(ExpListAdapter.of(mGroups, getContext().getApplicationContext()));
                                setListViewHeight(expListView,-1);
                            });
                }).flatMap((status) -> docDocApi.getReview(doctor.getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()))
                .doOnNext(review -> fillReview(review))
                .subscribe();
    }

    @NotNull
    private LinearLayout.LayoutParams getLinearLayoutParams() {
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }


    private void fillReview(ReviewResponseDto response) {
        if (response != null) {
            commentsList
                    .setAdapter(
                            DoctorReviewAdapter.of(getContext(), ofNullable(response.getReviewList()).orElse(emptyList())
                                    .stream()
                                    .sorted(Comparator.comparing(ReviewResponseDto.ReviewList::getDate).reversed())
                                    .limit(100)
                                    .collect(Collectors.toList())));
            ratings.forEach(rb -> ofNullable(response).map(ReviewResponseDto::getReviewRating).map(ReviewResponseDto.ReviewRating::getRating)
                    .flatMap(ratings -> ratings.stream().filter(rv -> rb.getOrder().compareTo(rv.getRatingValue()) == 0)
                            .findFirst())
                    .ifPresent(rating -> rb.getTxt().setText(rating.getRatingValue().toString() + " (" + rating.getRatingCount() + ")")));
        }
    }

    @AllArgsConstructor(staticName = "of")
    @Data
    public static class RatingBundle {
        private BigDecimal order;
        private TextView txt;
    }
}
