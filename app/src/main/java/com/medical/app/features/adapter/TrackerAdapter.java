package com.medical.app.features.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.activity.bundle.InputsBundle;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.dao.entity.Tracker.DataSet;
import com.medical.app.dao.entity.Tracker.DataSet.DataSetType;
import com.medical.app.dao.entity.Tracker.TrackerType;
import com.medical.app.dialog.TrackerDialog;
import com.medical.app.repository.TrackerRepository;

import org.joda.time.LocalDateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

@AllArgsConstructor
@Builder
public class TrackerAdapter extends RecyclerView.Adapter<TrackerAdapter.TrackerViewHolder> {

    @Builder.Default
    private List<Tracker> dataModelList = new ArrayList<>();
    private Activity context;
    private TrackerRepository trackerRepository;


    @NonNull
    @Override
    public TrackerAdapter.TrackerViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                               int viewType) {
        // Inflate out card list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tracker, parent, false);
        // Return a new view holder
        return new TrackerAdapter.TrackerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrackerAdapter.TrackerViewHolder holder, int position) {
        holder.bindData(dataModelList.get(position), context, trackerRepository);
    }

    public void addItem(Tracker tracker) {
        dataModelList.add(tracker);
        this.notifyDataSetChanged();
    }

    public void setTrackerList(List<Tracker> trackers) {
        dataModelList = ofNullable(trackers).orElse(emptyList()).stream()
                .sorted(Comparator.comparing(Tracker::getType)).collect(Collectors.toList());
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    public static class TrackerViewHolder extends RecyclerView.ViewHolder {

        private MaterialCardView cardView;
        private TextView topTextView;
        private TextView historyButton;
        private LinearLayout listOfInputs;
        private Button updateButton;
        private RecyclerView editTextList;

        public TrackerViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.tracker_view);
            topTextView = itemView.findViewById(R.id.top_text_desc);
            listOfInputs = itemView.findViewById(R.id.list_tracker_inputs);
            historyButton = itemView.findViewById(R.id.history_btn);
            updateButton = itemView.findViewById(R.id.update_btn);
            editTextList = itemView.findViewById(R.id.inputs_item_list);
        }

        public void bindData(Tracker dataModel, Activity context, TrackerRepository trackerRepository) {
            final TrackerType trackerType = dataModel.getType();

            List<ValuesBundle> inputs = createMeasureList(dataModel);
            editTextList.setLayoutManager(new LinearLayoutManager(context));
            final TrackerInputsAdapter inputsAdapter = TrackerInputsAdapter.of(inputs);
            editTextList.setAdapter(inputsAdapter);
            updateButton
                    .setOnClickListener(
                            getUpdateClickListener(inputsAdapter, dataModel, trackerRepository, dataModel, context));
            historyButton.setOnClickListener(view -> TrackerDialog.of(dataModel, context).getDialog().show());
            switch (trackerType) {
                case SUGAR_TRACKER:
                    topTextView.setText("Сахар");
                    topTextView
                            .setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    context.getDrawable(R.drawable.ic_icons8_insulin_pen), null, null,
                                    null);
                    break;
                case WEIGHT_TRACKER:
                    topTextView.setText("Вес");
                    topTextView
                            .setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    context.getDrawable(R.drawable.ic_icons8_weight_kg), null, null,
                                    null);
                    break;
                case IMPULSE_TRACKER:
                    topTextView.setText("Давление");
                    topTextView
                            .setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    context.getDrawable(R.drawable.icons8_sphygmomanometer), null, null,
                                    null);
                    break;
                case HEIGHT_TRACKER:
                    topTextView.setText("Рост");
                    topTextView
                            .setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    context.getDrawable(R.drawable.ic_icons8_height), null, null,
                                    null);
                    break;
            }
        }

        private OnClickListener getUpdateClickListener(TrackerInputsAdapter inputsAdapter,
                                                       Tracker tracker, TrackerRepository trackerRepository,
                                                       Tracker dataModel, Activity context) {
            return (view) -> {
                Optional<InputsBundle> anyOfAdded = inputsAdapter.getInputs().stream()
                        .filter(input -> !TextUtils.isEmpty(input.getValue().getText().toString()))
                        .findAny();
                if (anyOfAdded.isPresent()) {
                    inputsAdapter.setInputs(inputsAdapter.getInputs()
                            .stream()
                            .map(input -> {
                                final String inputText = input.getValue().getText().toString();
                                final BigDecimal newValue = new BigDecimal(
                                        TextUtils.isEmpty(inputText) ? "0" : inputText);
                                final LocalDateTime date = LocalDateTime.now();
                                List<DataSet> datasets = tracker.getDataSets();
                                datasets.removeIf(
                                        value -> value.getFormattedDate().equalsIgnoreCase(date.toString(Tracker.FORMAT))
                                                && value.getType().equals(input.getType()));
                                datasets
                                        .add(DataSet.of(date, input.getType(), newValue));
                                tracker.setDataSets(new ArrayList<>(datasets));
                                trackerRepository.insertTracker(tracker);
                                this.itemView.clearFocus();
                                return input;
                            }).collect(Collectors.toList()));
                    TrackerDialog.of(dataModel, context).getDialog().show();
                }
            };
        }
    }

    private static List<ValuesBundle> createMeasureList(Tracker tracker) {
        List<ValuesBundle> inputs = new ArrayList<>();
        final TrackerType type = tracker.getType();
        final LocalDateTime date = tracker.getDataSets()
                .stream().map(DataSet::getDate).max(LocalDateTime::compareTo).orElse(LocalDateTime.now());
        switch (type) {
            case SUGAR_TRACKER:
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.SUGAR),
                        DataSetType.SUGAR));
                break;
            case HEIGHT_TRACKER:
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.HEIGHT),
                        DataSetType.HEIGHT));
                break;
            case WEIGHT_TRACKER:
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.WEIGHT),
                        DataSetType.WEIGHT));
                break;
            case IMPULSE_TRACKER:
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.UPPER_PRESSURE),
                        DataSetType.UPPER_PRESSURE));
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.LOWER_PRESSURE),
                        DataSetType.LOWER_PRESSURE));
                inputs.add(ValuesBundle.of(getCurrentValueByType(tracker, date, DataSetType.PULSE),
                        DataSetType.PULSE));
                break;
        }
        return inputs;
    }

    private static BigDecimal getCurrentValueByType(Tracker tracker, LocalDateTime date,
                                                    DataSetType inputType) {
        return tracker.getDataSets().stream()
                .filter(data -> data.getDate().equals(date) && data.getType().equals(inputType))
                .findAny().map(DataSet::getValue).orElse(BigDecimal.ZERO);
    }
}

