package com.medical.app.features.search;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.paging.DataSource;
import androidx.paging.DataSource.Factory;

import com.medical.app.features.search.dialog.OnListLoadedCallback;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorsQuery;

public class DoctorsFactory extends Factory<Integer, DoctorListDto.DoctorDto> {

    private DoctorsQuery query;
    private DoctorsDataSource dataSource;
    private FragmentActivity context;
    private OnListLoadedCallback onListLoadedCallback;

    public DoctorsFactory(DoctorsQuery query, FragmentActivity context, OnListLoadedCallback onListLoadedCallback) {
        this.query = query;
        this.context = context;
        this.onListLoadedCallback = onListLoadedCallback;
    }


    @NonNull
    @Override
    public DataSource<Integer, DoctorListDto.DoctorDto> create() {
        this.dataSource = new DoctorsDataSource(query, context, onListLoadedCallback);
        return dataSource;
    }

    public void changeQuery(DoctorsQuery query) {
        this.query = query;
        if (dataSource != null) {
            dataSource.invalidate();
        }
    }
}
