package com.medical.app.features.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.medical.app.R;
import com.medical.app.features.search.dto.ReviewResponseDto;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class DoctorReviewAdapter extends RecyclerView.Adapter<DoctorReviewAdapter.ImageViewHolder> {
    private Context context;
    private List<ReviewResponseDto.ReviewList> reviewList;


    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.item_review, parent, false);
        layout.setPadding(0, 10, 10, 0);
        return new ImageViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.bindData(context, reviewList.get(position));
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        private TextView name, date, description;
        private RatingBar ratingBar;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            date = itemView.findViewById(R.id.date);
            description = itemView.findViewById(R.id.description);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }

        public void bindData(Context context, ReviewResponseDto.ReviewList review) {
            name.setText(review.getClient());
            date.setText(review.getDate().toString());
            description.setText(review.getText());
            ratingBar.setRating(review.getRatingQlf().floatValue());
        }
    }


}
