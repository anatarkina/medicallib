package com.medical.app.features.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.medical.app.features.MimiFragment;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private FragmentManager manager;

    public MainFragmentAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.manager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        final Fragment fragment = mFragmentList.get(position);
        Log.w(MainFragmentAdapter.class.getName(), "Destroy Item: " + fragment.getId());
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public void clear() {
        List<Fragment> fragments = manager.getFragments();
        if (fragments != null) {
            FragmentTransaction ft = manager.beginTransaction();
            for (Fragment f : fragments) {
                if (f instanceof MimiFragment) {
                    ft.remove(f);
                }

            }
            mFragmentList.clear();
            mFragmentTitleList.clear();
            ft.commitNowAllowingStateLoss();
            this.notifyDataSetChanged();
        }
    }
}


