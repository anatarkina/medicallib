package com.medical.app.features.search.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.medical.app.R;
import com.medical.app.features.search.dialog.DoctorInfoDialog;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class SlotsAdapter extends BaseAdapter {
    private List<DoctorInfoDialog.SlotBundle> slots;
    private Context context;
    public static DateTimeFormatter TIME_FORMAT = DateTimeFormat
            .forPattern("HH:mm");

    @Override
    public int getCount() {
        return slots.size();
    }

    @Override
    public DoctorInfoDialog.SlotBundle getItem(int position) {
        return slots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(context);
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
        textView.setText(slots.get(position).getStartTime().toLocalTime().toString(TIME_FORMAT));
        textView.setClickable(false);
        textView.setFocusable(false);
        textView.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        textView.setGravity(Gravity.CENTER);
        textView.setBackgroundColor(ContextCompat.getColor(context, R.color.lightGrey));
        textView.setTextSize(18);
        textView.setForeground(ContextCompat.getDrawable(context, typedValue.resourceId));
        return textView;
    }
}
