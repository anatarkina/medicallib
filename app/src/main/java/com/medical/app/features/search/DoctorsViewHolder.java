package com.medical.app.features.search;

import static com.medical.app.utils.NullFunctions.ifNotNull;
import static java.util.Optional.ofNullable;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.medical.app.R;
import com.medical.app.features.search.dialog.DoctorInfoDialog;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.view.StationWithColor;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DoctorsViewHolder extends
        ViewHolder {

    private TextView name, opinions, doctorSlot, price, firstStationTxt, secondStationTxt;

    private CircleImageView doctorImage;
    public static DateTimeFormatter DOC_DOC_FORMAT = DateTimeFormat
            .forPattern("yyyy-MM-dd HH:mm:ss");
    private static DateTimeFormatter FORMAT_SLOT = DateTimeFormat.forPattern("dd.MM.yy HH:mm");


    public DoctorsViewHolder(@NonNull View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.doctor_name);
        doctorImage = itemView.findViewById(R.id.doctor_image);
        opinions = itemView.findViewById(R.id.opinion_count);
        doctorSlot = itemView.findViewById(R.id.doctor_slot);
        price = itemView.findViewById(R.id.price);
        firstStationTxt = itemView.findViewById(R.id.firstStation);
        secondStationTxt = itemView.findViewById(R.id.secondStation);
    }

    public void bind(DoctorListDto.DoctorDto item, ArrayList<DoctorListDto.Station> stations, Context context,
                     FragmentManager fragmentManager) {
        name.setText(item.getName());
        this.itemView.setOnClickListener(getDoctorClickListener(item, fragmentManager));
        Glide.with(context)
                .load(item.getImg())
                .centerCrop()
                .into(doctorImage);
        final Map<String, List<DoctorListDto.Slot>> slots = item.getSlots();
        SpannableString slotTxt = slots.keySet().stream()
                .map(key -> slots.get(key))
                .map(slotList -> slotList.stream()
                        .map(DoctorListDto.Slot::getStartTime)
                        .map(slot -> LocalDateTime.parse(slot, DOC_DOC_FORMAT))
                        .collect(Collectors.toList()))
                .flatMap(List::stream)
                .min(LocalDateTime::compareTo)
                .map(min -> SpannableString.valueOf(min.toString(FORMAT_SLOT)))
                .orElse(getNoSlotText());
        doctorSlot.setText(slotTxt);


        switch (stations.size()) {
            case 0:
                hide(firstStationTxt, secondStationTxt);
                break;
            case 1:
                final DoctorListDto.Station firstStation = stations.get(0);
                initStation(context, firstStation, firstStationTxt);
                show(firstStationTxt);
                hide(secondStationTxt);
                break;
            case 2:
                final DoctorListDto.Station secondStation = stations.get(1);
                final DoctorListDto.Station firstStation1 = stations.get(0);
                initStation(context, secondStation, secondStationTxt);
                initStation(context, firstStation1, firstStationTxt);
                show(firstStationTxt, secondStationTxt);
                break;


        }
        final StringBuilder sb = new StringBuilder();
        price.setText(ofNullable(item.getPrice()).map(doctorPrice -> "₽ " + doctorPrice).orElse(""));
        ifNotNull(item.getRating(), rating -> sb.append(rating + " "));
        ifNotNull(item.getOpinionCount(),
                opinionCount -> sb.append("(" + opinionCount.toString() + ")"));

        opinions.setText(sb.toString());
    }

    private SpannableString getNoSlotText() {
        SpannableString noSlot = SpannableString.valueOf("нет записи");
        noSlot.setSpan(
                new ForegroundColorSpan(Color.RED),
                0, noSlot.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return noSlot;
    }

    protected void initStation(Context context, DoctorListDto.Station firstStation, TextView firstStationTxt) {
        StationWithColor
                .of(firstStation.getName(), firstStation.getLineColor(), context,
                        new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT))
                .init(firstStationTxt);
    }

    private OnClickListener getDoctorClickListener(DoctorListDto.DoctorDto item,
                                                   FragmentManager fragmentManager) {
        return view -> DoctorInfoDialog.of(item).show(fragmentManager, DoctorInfoDialog.class.getSimpleName());
    }

    private void hide(View... views) {
        for (View view : views) {
            view.setVisibility(View.GONE);
        }
    }

    private void show(View... views) {
        for (View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void main(String[] args) {
        System.out.println("");
    }
}
