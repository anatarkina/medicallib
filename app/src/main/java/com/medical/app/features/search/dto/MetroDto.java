package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MetroDto {
    @SerializedName("MetroList")
    @Expose
    private List<Metro> metroList = null;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class Metro {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("LineName")
        @Expose
        private String lineName;
        @SerializedName("LineColor")
        @Expose
        private String lineColor;
        @SerializedName("DistrictIds")
        @Expose
        private List<String> districtIds = null;
        @SerializedName("CityId")
        @Expose
        private String cityId;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
    }
}
