package com.medical.app.features.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.medical.app.R;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class ExpListAdapter extends BaseExpandableListAdapter {

    private List<AchievementGroup> mGroups;
    private Context mContext;

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mGroups.get(groupPosition).getChildren().size();
    }

    @Override
    public AchievementGroup getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public AchievementGroup.AchievementChild getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_group, null);
        }
        AchievementGroup achievementGroup = getGroup(groupPosition);
        TextView expText = convertView
                .findViewById(R.id.listTitle);
        String text = null;
        Integer id = null;
        switch (achievementGroup.getType()) {
            case EDUCATION:
                text = "ОБРАЗОВАНИЕ";
                id = R.drawable.ic_icons8_graduation_cap;
                break;
            case COURSES:
                text = "КУРСЫ";
                id = R.drawable.ic_icons8_diploma;
                break;
            case EXPERIENCE:
                text = "ОПЫТ РАБОТЫ";
                id = R.drawable.ic_icons8_company;
                break;
        }

        expText.setText(text);
        expText.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext,
                id), null, null, null);
        expText.setCompoundDrawablePadding(5);
        expText.setPadding(5, 0, 0, 0);
        expText.setTextSize(16);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_achievement, null);
        }
        AchievementGroup.AchievementChild achievementChild = getChild(groupPosition, childPosition);

        TextView startYear = convertView.findViewById(R.id.start_year);
        TextView position = convertView.findViewById(R.id.position);
        TextView endYear = convertView.findViewById(R.id.end_year);
        TextView organisation = convertView.findViewById(R.id.organisation);
        startYear.setText(achievementChild.getStartYear());
        position.setText(achievementChild.getPosition());
        organisation.setText(achievementChild.getOrganisation());
        endYear.setText(achievementChild.getEndYear());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}