package com.medical.app.features.search;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import java.lang.reflect.Type;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DocDocService {

    private static DocDocService INSTANCE;
    private static final String BASE_URL = "https://api.docdoc.ru/";
    public static final String BASE_PATH = "/public/rest/1.0.12/";
    public static final String PID = "22732";
    private Retrofit mRetrofit;

    private DocDocService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        final Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>) (json, typeOfT, context) -> LocalDate.parse(json.getAsString(), DateTimeFormat.forPattern("dd.MM.yyyy"))).create();


        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
    }

    public static DocDocService getInstance() {
        if (INSTANCE == null) {
            synchronized (DocDocService.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            new DocDocService();
                }
            }
        }
        return INSTANCE;
    }

    public DocDocApi getJSONApi() {
        return mRetrofit.create(DocDocApi.class);
    }
}
