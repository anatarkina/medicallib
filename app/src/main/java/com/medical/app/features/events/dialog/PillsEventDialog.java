package com.medical.app.features.events.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.features.events.adapter.PillsAdapter;
import com.medical.app.repository.PomRepository;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.medical.app.repository.PomRepository.getPillCountPredicate;
import static com.medical.app.repository.PomRepository.getPillDurationPredicate;

public class PillsEventDialog extends DialogFragment {
    private TextView date;
    private RecyclerView listOfPills;
    private PillsAdapter pillsAdapter;
    private PomRepository pomRepository;
    private Button dismiss;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_pills, null);
        date = dialogLayout.findViewById(R.id.date);
        dismiss = dialogLayout.findViewById(R.id.dismiss_btn);
        listOfPills = dialogLayout.findViewById(R.id.list_of_poms);
        listOfPills.setLayoutManager(new LinearLayoutManager(getContext()));
        pillsAdapter = PillsAdapter.of(new ArrayList<>(), getContext());
        listOfPills.setAdapter(pillsAdapter);
        pomRepository = new PomRepository(getContext());
        date.setText(LocalDate.now().toString("dd.MM.yyyy"));
        dialog.setContentView(dialogLayout);
        fillValues();
        dismiss.setOnClickListener(v -> dismiss());
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return dialog;
    }

    private void fillValues() {
        pomRepository.getPomsWithPills().observe(getActivity(), listOfPills -> {
            if (listOfPills != null) {
                listOfPills.stream().map(PomWithPills::getPills)
                        .flatMap(List::stream)
                        .filter(pill->LocalDate.now().isAfter(pill.getUpdateTime()))
                        .forEach(Pill::refreshCount);
                pillsAdapter.setValues(listOfPills.stream()
                        .filter(pom -> pom.getPills().stream()
                                .filter(getPillDurationPredicate().and(getPillCountPredicate()))
                                .findAny().isPresent())
                        .collect(Collectors.toList()));
            }

        });
    }
}
