package com.medical.app.features.notes;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.medical.app.configs.ApiConstants.GoogleConst.SELECTED_TAB;
import static com.medical.app.dao.entity.History.DEFAULT_ID;
import static com.medical.app.dao.entity.Record.RecordType.recordTypes;
import static com.medical.app.utils.FabUtils.getFabItem;
import static java.util.Optional.ofNullable;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.User;
import com.medical.app.dialog.HistoryChangeDialog;
import com.medical.app.dialog.PomCreateDialog;
import com.medical.app.dialog.SnapshotDialog;
import com.medical.app.features.ImageWorker;
import com.medical.app.features.MimiFragment;
import com.medical.app.features.adapter.MainFragmentAdapter;
import com.medical.app.features.home.ProfileDialog;
import com.medical.app.features.home.SettingsDialog;
import com.medical.app.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.Getter;

public class NotesPageFragment extends ImageWorker implements MimiFragment, SpeedDialView.OnActionSelectedListener {
    @Getter
    private SpeedDialView fabMenu;
    private Map<Integer, Record.RecordType> fabItemsByType = new HashMap<>();
    private Map<Integer, NotesAddType> notesByType = new HashMap<>();

    private TextView topFrameText;
    private ImageView profileImage;
    private UserRepository userRepository;
    private MainFragmentAdapter fapAdapter;
    private TabLayout tabs;
    private ViewPager viewPager;
    private MaterialToolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_notes, null);
        viewPager = view.findViewById(R.id.viewpager);
        tabs = view.findViewById(R.id.tabs);
        topFrameText = view.findViewById(R.id.top_frame_text);
        profileImage = view.findViewById(R.id.profile_image);
        userRepository = new UserRepository(getContext());
        toolbar = view.findViewById(R.id.my_toolbar);
        toolbar.inflateMenu(R.menu.settings_menu);
        toolbar.setOnMenuItemClickListener(getOnMenuClickListener());
        fabMenu = view.findViewById(R.id.history_speed_dial);
        fapAdapter = new MainFragmentAdapter(getChildFragmentManager(),
                BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        if (savedInstanceState == null) {
            setupNotesViewPager();
        }
        setupFabMenu();
        loadData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupFabMenu();
    }

    private void setupFabMenu() {
        fabMenu.close();
        fabMenu.clearActionItems();
        fabMenu.setMainFabClosedDrawable(
                ContextCompat.getDrawable(getContext(), R.drawable.icon_add));
        final List<SpeedDialActionItem> actionItems = recordTypes().stream()
                .map(type -> {
                    final int id = View.generateViewId();
                    fabItemsByType.put(id, type);
                    return getFabItem(id,
                            R.drawable.icon_add, type.getName(),
                            ContextCompat.getColor(getContext(),
                                    R.color.colorExpandTextColor),
                            ContextCompat.getColor(getContext(),
                                    android.R.color.white
                            ), getContext());
                })
                .collect(Collectors.toList());
        final int historyId = View.generateViewId();
        notesByType.put(historyId, NotesAddType.HISTORY);
        actionItems.add(getFabItem(historyId,
                R.drawable.icon_add, "Историю болезни",
                ContextCompat.getColor(getContext(),
                        R.color.colorExpandTextColor),
                ContextCompat.getColor(getContext(),
                        android.R.color.white
                ), getContext()));
        final int pomId = View.generateViewId();
        notesByType.put(pomId, NotesAddType.POM);
        actionItems.add(getFabItem(pomId,
                R.drawable.icon_add, "План приема лекарств",
                ContextCompat.getColor(getContext(),
                        R.color.colorExpandTextColor),
                ContextCompat.getColor(getContext(),
                        android.R.color.white
                ), getContext()));
        fabMenu.addAllActionItems(actionItems);
        fabMenu.setOnActionSelectedListener(this);
    }

    private Toolbar.OnMenuItemClickListener getOnMenuClickListener() {
        return item -> {
            int itemId = item.getItemId();
            switch (itemId) {
                case R.id.action_profile:
                    new ProfileDialog().show(getChildFragmentManager(), ProfileDialog.class.getSimpleName());
                    return true;
                case R.id.action_settings:
                    new SettingsDialog().show(getChildFragmentManager(), SettingsDialog.class.getSimpleName());
                    return true;
            }
            return false;
        };
    }

    private void setupNotesViewPager() {
        fapAdapter.clear();
        fapAdapter.addFragment(PomFragment.builder().fabMenu(fabMenu).build(), getString(R.string.treatment));
        fapAdapter.addFragment(new HistoryFragment(), getString(R.string.histories));
        fapAdapter.addFragment(new SnapshotsFragment(), getString(R.string.snapshots));
        viewPager.setAdapter(fapAdapter);
        tabs.setupWithViewPager(viewPager);
        Bundle arguments = getArguments();
        if (arguments != null) {
            tabs.getTabAt(arguments.getInt(SELECTED_TAB)).select();
        }
    }

    private void loadData() {
        userRepository.getCurrentUser().observe(getActivity(), user -> {
            if (user != null) {
                topFrameText.setText(user.getFullName());
                if (getContext() != null) {
                    Glide.with(getContext()).load(ofNullable(user).map(User::getAvatarUrl)
                            .orElse(""))
                            .error(ContextCompat.getDrawable(getContext(), R.drawable.default_avatar))
                            .into(profileImage);
                }
            }
        });
    }

    @Override
    public boolean onActionSelected(SpeedDialActionItem view) {
        int actionItemId = view.getId();
        if (fabItemsByType.containsKey(actionItemId)) {
            Bundle bundle = new Bundle();
            bundle.putString(Record.RecordType.class.getName(), fabItemsByType.get(actionItemId).name());
            showPictureDialogWithBundle(bundle);
            return true;
        }
        if (notesByType.containsKey(actionItemId)) {
            NotesAddType notesAddType = notesByType.get(actionItemId);
            switch (notesAddType) {
                case POM:
                    new PomCreateDialog()
                            .show(getActivity().getSupportFragmentManager(), PomCreateDialog.class.getName());
                    return false;
                case HISTORY:
                    HistoryChangeDialog.of(DEFAULT_ID)
                            .show(getActivity().getSupportFragmentManager(), HistoryChangeDialog.class.getSimpleName());
                    return false;

            }
        }
        return true;
    }

    @Override
    public void doWithImagePath(Uri path, Bundle bundle) {
        final Record.RecordType recordType = Record.RecordType.valueOf(bundle.getString(Record.RecordType.class.getName()));
        if (path != null) {
            SnapshotDialog.of(path, recordType).show(getChildFragmentManager(), SnapshotDialog.TAG);
        }
    }
}
