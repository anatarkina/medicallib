package com.medical.app.features.events.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.medical.app.R;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.features.adapter.TabletsAdapter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class PillsAdapter extends RecyclerView.Adapter<PillsAdapter.PillsViewHolder> {
    private List<PomWithPills> data;
    private Context context;

    @NonNull
    @Override
    public PillsAdapter.PillsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pom, parent, false);
        return new PillsAdapter.PillsViewHolder(view);
    }

    public void setValues(List<PomWithPills> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull PillsAdapter.PillsViewHolder holder, int position) {
        final PomWithPills pom = data.get(position);
        holder.bindData(pom, context);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class PillsViewHolder extends RecyclerView.ViewHolder {
        private TextView topDescription;
        private RecyclerView listOfPills;

        public PillsViewHolder(@NonNull View itemView) {
            super(itemView);
            topDescription = itemView.findViewById(R.id.top_description);
            listOfPills = itemView.findViewById(R.id.list_of_pills);
        }

        public void bindData(PomWithPills pom, Context context) {
            listOfPills.setLayoutManager(new LinearLayoutManager(context));
            topDescription.setText(pom.getPom().getDiagnosis());
            List<Pill> newPillsList = pom.getPills().stream()
                    .map(pill -> IntStream.range(0, pill.getCount())
                            .mapToObj(num -> pill).collect(Collectors.toList()))
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
            listOfPills.setAdapter(TabletsAdapter.of(newPillsList, context));
        }
    }
}
