package com.medical.app.features.adapter.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.medical.app.R;
import com.medical.app.dto.SnapshotDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Builder
public class HistoryEditAdapter extends BaseAdapter {

    private Context context;
    @Builder.Default
    @Getter
    private List<SnapshotDto> snapshots = new ArrayList<>();

    public int getCount() {
        if (snapshots != null) {
            return this.snapshots.size();
        }
        return 0;
    }

    public void setSnapshots(List<SnapshotDto> snapshots) {
        this.snapshots = snapshots;
        this.notifyDataSetChanged();
    }

    public SnapshotDto getItem(int position) {
        return snapshots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        final SnapshotDto dto = snapshots.get(position);
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.item_selectable_image, parent, false);
        final ImageView image = layout.findViewById(R.id.snapshot_image);
        final TextView text = layout.findViewById(R.id.snapshot_title);
        final CheckBox checkBox = layout.findViewById(R.id.snapshot_checkbox);
        checkBox.setId(position);
        checkBox.setChecked(dto.isSelected());
        checkBox.setOnClickListener(view -> {
            CheckBox cb = (CheckBox) view;
            dto.setUpdated(true);
            dto.setSelected(cb.isChecked());
        });

        Glide.with(context)
                .load(dto.getImageUrl())
                .into(image);
        image.invalidate();
        text.setText(dto.getChipSelected());
        return layout;
    }

}
