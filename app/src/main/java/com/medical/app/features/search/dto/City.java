package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class City {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Alias")
    @Expose
    private String alias;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("SearchType")
    @Expose
    private Integer searchType;
    @SerializedName("HasDiagnostic")
    @Expose
    private Boolean hasDiagnostic;
    @SerializedName("TimeZone")
    @Expose
    private Integer timeZone;
}