package com.medical.app.features.search.dto;

import com.medical.app.R;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DoctorSort {
  EXPERIENCE(R.id.experience_sort, "-experience"),
  PRICE(R.id.price_sort, "price"),
  DISTANCE(R.id.distance_sort, "distance"),
  RATING(R.id.rating_sort, "-rating");

  private static final Map<Integer, DoctorSort> BY_ID = new HashMap<>();

  static {
    for (DoctorSort e : values()) {
      BY_ID.put(e.id, e);
    }
  }

  private Integer id;
  private String sort;

  public static DoctorSort byId(Integer id) {
    if (id != null) {
      return BY_ID.getOrDefault(id, PRICE);
    }
    return PRICE;
  }
}
