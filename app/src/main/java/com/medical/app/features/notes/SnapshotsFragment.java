package com.medical.app.features.notes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.Record;
import com.medical.app.dao.entity.Record.RecordType;
import com.medical.app.features.adapter.SnapshotsAdapter;
import com.medical.app.repository.RecordRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

public class SnapshotsFragment extends Fragment {

    private GridView imageGrid;
    private Map<Integer, RecordType> fabItemsByType = new HashMap<>();
    private RecordRepository recordRepository;
    private SnapshotsAdapter snapshotsAdapter;
    private TextView activeText;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_snapshots, container, false);
        imageGrid = view.findViewById(R.id.grid_view);
        activeText = view.findViewById(R.id.active_txt);
        snapshotsAdapter = SnapshotsAdapter.of(getActivity(), new ArrayList<>());
        imageGrid.setAdapter(snapshotsAdapter);

        recordRepository = new RecordRepository(getActivity());
        recordRepository.getAllRecords().observe(getActivity(), records -> {
            List<Record> values = ofNullable(records).orElse(emptyList());
            snapshotsAdapter.setSnapshots(values.stream()
                    .sorted(Comparator.comparing(Record::getDate).reversed())
                    .collect(Collectors.toList()));
            if (activeText != null) {
                if (values.isEmpty()) {
                    activeText.setVisibility(View.VISIBLE);
                } else {
                    activeText.setVisibility(View.GONE);
                }
            }
        });
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return view;
    }
}
