package com.medical.app.features.adapter.stream;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.medical.app.R;
import com.medical.app.dao.entity.StreamEntity;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
public class StreamByTimeAdapter extends RecyclerView.Adapter<StreamByTimeAdapter.StreamViewHolder> {
    @Builder.Default
    @Getter
    private List<StreamEntity> values = new ArrayList<>();

    @NonNull
    @Override
    public StreamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_stream_by_time, parent, false);
        return new StreamByTimeAdapter.StreamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StreamViewHolder holder, int position) {
        holder.bindData(values.get(position));
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void setValues(List<StreamEntity> values) {
        this.values = values;
        this.notifyDataSetChanged();
    }

    public static class StreamViewHolder extends RecyclerView.ViewHolder {
        private TextView time, description, subDescription;

        public StreamViewHolder(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            description = itemView.findViewById(R.id.description);
            subDescription = itemView.findViewById(R.id.sub_description);

        }

        public void bindData(StreamEntity dto) {
            time.setText(dto.getCreateTime().toLocalTime().toString("HH:mm"));
            description.setText(dto.getDescription());
            subDescription.setText(dto.getSubDescription());
        }
    }
}
