package com.medical.app.features.events.dialog;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.medical.app.R;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.StreamEntity;
import com.medical.app.features.events.dto.DoctorEventInfo;
import com.medical.app.repository.EventRepository;
import com.medical.app.repository.StreamRepository;
import com.medical.app.scheduler.DoctorSchedulerManager;
import com.medical.app.view.StationWithColor;

import org.joda.time.LocalDateTime;

import java.util.List;

import lombok.RequiredArgsConstructor;

import static com.medical.app.utils.NullFunctions.isBlankString;

@RequiredArgsConstructor(staticName = "of")
public class DoctorEventDialog extends DialogFragment {
    private final EventEntity dataModel;
    private DoctorEventInfo info;
    private ImageView doctorImage;
    private ImageButton share;
    private TextView ratingDescription, clinicName, doctorBranch, date, time, phone, address,
            fioDoctor;
    private RatingBar ratingBar;
    private EventRepository eventRepository;
    private Button end, dismissBtn;
    private LinearLayout stations;
    private StreamRepository streamRepository;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_doctor_event, null);
        info = DoctorEventInfo.toObject(dataModel.getContent());
        eventRepository = new EventRepository(getContext());
        streamRepository = new StreamRepository(getContext());
        doctorImage = dialogLayout.findViewById(R.id.doctor_info_img);
        end = dialogLayout.findViewById(R.id.end_btn);
        dismissBtn = dialogLayout.findViewById(R.id.dismiss_btn);
        share = dialogLayout.findViewById(R.id.share_button);
        stations = dialogLayout.findViewById(R.id.stations);

        fioDoctor = dialogLayout.findViewById(R.id.fio_doctor);
        ratingBar = dialogLayout.findViewById(R.id.ratingBar);
        ratingDescription = dialogLayout.findViewById(R.id.rating_description);
        clinicName = dialogLayout.findViewById(R.id.clinic_name);
        doctorBranch = dialogLayout.findViewById(R.id.doctor_branch);
        date = dialogLayout.findViewById(R.id.date);
        time = dialogLayout.findViewById(R.id.time);
        phone = dialogLayout.findViewById(R.id.phone);
        address = dialogLayout.findViewById(R.id.address);

        phone.setOnClickListener(getCallClick());
        address.setOnClickListener(getMakeRoute());
        end.setOnClickListener(getEndClick());
        share.setOnClickListener(getShareClick());
        fillValues();

        dismissBtn.setOnClickListener(v -> dismiss());
        dialog.setContentView(dialogLayout);
        requestMultiplePermissions();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return dialog;
    }

    private View.OnClickListener getShareClick() {
        return v -> {
            Intent shareIntent = new Intent();
            Spanned text = Html.fromHtml("<p>" + "Запись к врачу" + "</p>" +
                    "<p>" + fioDoctor.getText().toString() + "<br>" +
                    doctorBranch.getText().toString() + "</p>" +
                    "<br>" + "Дата: " + date.getText().toString() + "<br>" +
                    "Время: " + time.getText().toString() + "<br>" +
                    "<br>" + "Клиника: " + clinicName.getText().toString() + "<br>" +
                    "Адрес: " + address.getText().toString() + "<br>" +
                    "Телефон: " + phone.getText().toString() + "<br>");

            shareIntent.putExtra(Intent.EXTRA_TEXT, text.toString());
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.setType("*/*");
            startActivity(shareIntent);
        };
    }

    private View.OnClickListener getEndClick() {
        return v -> {
            dataModel.setActive(false);
            eventRepository.insert(dataModel);
            streamRepository.insert(StreamEntity.builder()
                    .description(info.getFioDoctor())
                    .subDescription(info.getClinic())
                    .createTime(LocalDateTime.now())
                    .build());
            Toast.makeText(getContext(), "Запись к врачу завершена!",
                    Toast.LENGTH_SHORT).show();
            DoctorSchedulerManager.builder()
                    .context(getContext())
                    .cancel(dataModel);
            dismiss();
        };
    }

    private View.OnClickListener getMakeRoute() {
        return v -> {
            String addressStr = address.getText().toString();
            if (!isBlankString(addressStr)) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + addressStr);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(mapIntent);
                } else {
                    Toast.makeText(getContext(), "Не получается найти подходящее приложение! ",
                            Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private View.OnClickListener getCallClick() {
        return v -> {
            String phone = this.phone.getText().toString();
            if (!isBlankString(phone)) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        };
    }


    private void requestMultiplePermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            phone.setEnabled(true);
                        }
                        if (report
                                .isAnyPermissionPermanentlyDenied()) {
                            phone.setEnabled(false);
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(
                        error -> Toast.makeText(getContext(), "Some Error! ", Toast.LENGTH_SHORT)
                                .show())
                .onSameThread()
                .check();
    }

    private void fillValues() {
        ratingBar.setRating(info.getRating());
        ratingDescription.setText(info.getRatingDescription());
        clinicName.setText(info.getClinic());
        doctorBranch.setText(info.getDoctorBranch());
        date.setText(info.getStartTime().toLocalDate().toString("dd.MM.yyyy"));
        time.setText(info.getStartTime().toLocalTime().toString("HH:mm"));
        String phoneNum = info.getDoctorPhoneNumber();
        if (!isBlankString(phoneNum)) {
            phone.setText(phoneNum);
            phone.setVisibility(View.VISIBLE);
        }
        address.setText(info.getStreet());
        fioDoctor.setText(info.getFioDoctor());
        info.getStations().forEach(station -> {
            stations.addView(StationWithColor
                    .of(station.getName(), station.getColor(), getContext(),
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT))
                    .initWithDistanceWalking(station.getDistanceWalking(), station.getTimeWalking()));
        });
        Glide.with(this).load(info.getImgFormat()).into(doctorImage);
    }
}
