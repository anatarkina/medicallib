package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClinicList {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ShortName")
    @Expose
    private String shortName;
    @SerializedName("RewriteName")
    @Expose
    private String rewriteName;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("StreetId")
    @Expose
    private String streetId;
    @SerializedName("House")
    @Expose
    private String house;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("IsDiagnostic")
    @Expose
    private String isDiagnostic;
    @SerializedName("isClinic")
    @Expose
    private String isClinic;
    @SerializedName("IsDoctor")
    @Expose
    private String isDoctor;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneAppointment")
    @Expose
    private String phoneAppointment;
    @SerializedName("logoPath")
    @Expose
    private String logoPath;
    @SerializedName("ScheduleState")
    @Expose
    private String scheduleState;
    @SerializedName("DistrictId")
    @Expose
    private String districtId;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ReplacementPhone")
    @Expose
    private Object replacementPhone;
    @SerializedName("MinPrice")
    @Expose
    private Object minPrice;
    @SerializedName("MaxPrice")
    @Expose
    private Object maxPrice;
    @SerializedName("Logo")
    @Expose
    private String logo;
    @SerializedName("Order")
    @Expose
    private Double order;
    @SerializedName("Rating")
    @Expose
    private Double rating;
    @SerializedName("TypeOfInstitution")
    @Expose
    private String typeOfInstitution;
    @SerializedName("ParentId")
    @Expose
    private Integer parentId;
    @SerializedName("BranchesId")
    @Expose
    private List<String> branchesId = null;
    @SerializedName("HighlightDiscount")
    @Expose
    private Integer highlightDiscount;
    @SerializedName("RequestFormSurname")
    @Expose
    private Boolean requestFormSurname;
    @SerializedName("RequestFormBirthday")
    @Expose
    private Boolean requestFormBirthday;
    @SerializedName("RequestFormClientAge")
    @Expose
    private Boolean requestFormClientAge;
}
