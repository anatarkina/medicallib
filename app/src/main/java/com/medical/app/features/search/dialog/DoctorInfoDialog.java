package com.medical.app.features.search.dialog;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.medical.app.R;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.User;
import com.medical.app.features.events.dto.DoctorEventInfo;
import com.medical.app.features.search.DocDocApi;
import com.medical.app.features.search.DocDocService;
import com.medical.app.features.search.adapter.SlotsAdapter;
import com.medical.app.features.search.dto.Clinic;
import com.medical.app.features.search.dto.ClinicList;
import com.medical.app.features.search.dto.DoctorListDto;
import com.medical.app.features.search.dto.DoctorListDto.ClinicsInfo;
import com.medical.app.features.search.dto.DoctorListDto.StationsDistance;
import com.medical.app.features.search.dto.RequestDto;
import com.medical.app.features.search.dto.RequestResponseDto;
import com.medical.app.repository.EventRepository;
import com.medical.app.repository.UserRepository;
import com.medical.app.scheduler.DoctorSchedulerManager;
import com.medical.app.view.ImageGetter;
import com.medical.app.view.StationWithColor;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.medical.app.features.search.DoctorsViewHolder.DOC_DOC_FORMAT;
import static com.medical.app.utils.DateUtils.ONLY_TIME_FORMATTER;
import static com.medical.app.utils.NullFunctions.ifNotNull;
import static com.medical.app.utils.NullFunctions.ifNotNullGet;
import static com.medical.app.utils.NullFunctions.isBlankString;
import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;

@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class DoctorInfoDialog extends DialogFragment {

    @NonNull
    private DoctorListDto.DoctorDto doctor;
    private ImageView doctorInfoImage;
    private TextView fioDoctor, ratingDescription, doctorBranch, branchFullInfo,
            exp;
    private DocDocApi docDocApi;
    private RatingBar ratingBar;
    private LinearLayout clinicsList, slotsList;
    private Button moreInfo, listBtn;
    private EventRepository eventRepository;

    public static DateTimeFormatter DATE_FORMAT = DateTimeFormat
            .forPattern("dd MMMM");

    private UserRepository userRepository;

    private static ClinicsInfo apply(ClinicsInfo cl) {
        return cl;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_doctor_details, null);
        fioDoctor = dialogLayout.findViewById(R.id.fio_doctor);
        ratingDescription = dialogLayout.findViewById(R.id.rating_description);
        doctorBranch = dialogLayout.findViewById(R.id.doctor_branch);
        branchFullInfo = dialogLayout.findViewById(R.id.doctor_branch_full_info);
        exp = dialogLayout.findViewById(R.id.doctor_exp);
        ratingBar = dialogLayout.findViewById(R.id.ratingBar);
        clinicsList = dialogLayout.findViewById(R.id.doctor_clinics_list);
        slotsList = dialogLayout.findViewById(R.id.doctor_slots_list);
        moreInfo = dialogLayout.findViewById(R.id.doctor_more_info_btn);
        listBtn = dialogLayout.findViewById(R.id.doctors_list_btn);
        doctorInfoImage = dialogLayout.findViewById(R.id.doctor_info_img);
        eventRepository = new EventRepository(getContext());
        docDocApi = DocDocService
                .getInstance()
                .getJSONApi();
        userRepository = new UserRepository(getContext());
        listBtn.setOnClickListener(v -> dialog.dismiss());
        moreInfo.setOnClickListener(v -> DoctorReviewDialog.of(doctor)
                .show(activity.getSupportFragmentManager(), DoctorReviewDialog.class.getSimpleName()));
        fillValues();

        dialog.setContentView(dialogLayout);
        return dialog;
    }

    private void fillValues() {
        if (doctor != null) {
            Context context = getContext();
            Glide.with(context)
                    .load(doctor.getImgFormat())
                    .into(doctorInfoImage);
            fioDoctor.setText(doctor.getName());
            final StringBuilder sb = new StringBuilder();
            ifNotNull(doctor.getRating(), rating -> {
                ratingBar.setRating(Float.valueOf(rating));
                sb.append(rating + " ");
            });
            ifNotNull(doctor.getOpinionCount(),
                    opinionCount -> sb.append("(" + opinionCount.toString() + ")"));
            ratingDescription.setText(sb.toString());
            if (!isBlankString(doctor.getCategory()) || !isBlankString(doctor.getDegree())) {
                doctorBranch.setText(isBlankString(doctor.getCategory()) ? doctor.getDegree() : doctor.getCategory());
            } else {
                clinicsList.removeView(doctorBranch);
            }
            ifNotNull(doctor.getExperienceYear(), expr -> {
                exp.setText(expr + " г. практики");
                exp.setCompoundDrawablePadding(5);
                exp.setPadding(5, 0, 0, 0);
            });
            branchFullInfo.setText(ofNullable(doctor.
                    getSpecialities())
                    .orElse(Collections.emptyList())
                    .stream()
                    .map(sp -> sp.getName())
                    .collect(Collectors.joining(", ")));
            branchFullInfo.setCompoundDrawablePadding(5);
            branchFullInfo.setPadding(5, 0, 0, 0);
            List<Observable<Boolean>> obs = doctor
                    .getSlots()
                    .keySet()
                    .stream()
                    .map(key -> getClinicInfo(key))
                    .collect(Collectors.toList());
            obs.add(Observable.just(true));

            Observable.merge(obs)
                    .toList()
                    .map(results -> true)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success -> {
                        ofNullable(doctor.getClinicsInfo())
                                .orElse(Collections.emptyList())
                                .stream()
                                .forEach(fillClinicInfo(doctor.getStations(), context));
                        fillSlots();
                    });
        }
    }

    private void fillSlots() {
        Map<Integer, ClinicsInfo> clinics = ofNullable(doctor).map(DoctorListDto.DoctorDto::getClinicsInfo).orElse(Collections.emptyList())
                .stream().collect(Collectors.toMap(ClinicsInfo::getClinicId, DoctorInfoDialog::apply));
        ofNullable(doctor)
                .map(DoctorListDto.DoctorDto::getSlots)
                .orElse(Collections.emptyMap())
                .entrySet()
                .stream()
                .filter(slot -> clinics.containsKey(Integer.valueOf(slot.getKey())))
                .flatMap(slot -> slot.getValue().stream()
                        .map(dc -> SlotBundle
                                .of(clinics.get(Integer.valueOf(slot.getKey())),
                                        LocalDateTime.parse(dc.getStartTime(), DOC_DOC_FORMAT), dc.getId(),
                                        doctor.getId(), doctor.getName(), doctor.getImg(), doctor.getImgFormat(),
                                        doctor.getStations(), doctor.getAddPhoneNumber())))
                .collect(Collectors.groupingBy(bundle -> clinics.size() == 1 ? bundle.getClinic().getClinicId() : bundle.getStartTime().toLocalDate(), TreeMap::new, Collectors.toList()))
                .entrySet()
                .stream()
                .forEach(slots -> {
                    View view = getActivity().getLayoutInflater().inflate(R.layout.item_doctor_schedule, null);
                    TextView clinicInfo = view.findViewById(R.id.clinic_info);
                    LinearLayout clinicInfoGrid = view.findViewById(R.id.clinic_info_grid);
                    slots.getValue().stream().collect(Collectors.groupingBy(b -> b.getStartTime().toLocalDate(), TreeMap::new, Collectors.toList()))
                            .entrySet()
                            .forEach(entry -> {
                                LayoutParams schLp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                schLp.setMargins(0, 5, 0, 0);
                                LayoutParams gridLp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                                gridLp.setMargins(0, 0, 0, 10);
                                TextView scheduleDay = new TextView(getContext());
                                scheduleDay.setTypeface(ResourcesCompat.getFont(getContext(), R.font.roboto_light));
                                scheduleDay.setLayoutParams(schLp);
                                GridView grid = new GridView(getContext());
                                grid.setGravity(Gravity.CENTER);
                                grid.setHorizontalSpacing(5);
                                grid.setNumColumns(4);
                                grid.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
                                grid.setVerticalSpacing(5);
                                grid.setLayoutParams(gridLp);
                                SlotsAdapter slotsAdapter = SlotsAdapter.of(entry.getValue(),
                                        getContext());
                                grid.setAdapter(slotsAdapter);
                                clinicInfo.setText(entry.getValue().get(0).getClinic().getClinicName());
                                scheduleDay.setText((DATE_FORMAT.withLocale(new Locale("ru")).print(entry.getKey())));
                                grid.setOnItemClickListener(getOnItemClickListener(slotsAdapter));
                                clinicInfoGrid.addView(scheduleDay);
                                clinicInfoGrid.addView(grid);
                            });
                    slotsList.addView(view);
                });
    }

    private Observable<Boolean> getClinicInfo(String key) {
        return docDocApi
                .getClinic(key)
                .doOnNext(clinicListDto -> {
                    Clinic clinic = clinicListDto
                            .getClinic()
                            .stream()
                            .filter(cl -> cl.getId().equalsIgnoreCase(key))
                            .findAny()
                            .orElseThrow(() -> new RuntimeException("Не найдена клиника"));
                    doctor.getClinicsInfo().clear();
                    doctor.getClinicsInfo().add(ClinicsInfo.builder()
                            .clinicId(Integer.valueOf(key))
                            .clinicName(clinic.getName())
                            .house(clinic.getHouse())
                            .street(clinic.getStreet())
                            .stations(clinic.getStations().stream().map(Clinic.Station::getId)
                                    .map(Object::toString)
                                    .collect(Collectors.toList()))
                            .stationsDistance(clinic
                                    .getStations()
                                    .stream()
                                    .map(st -> StationsDistance
                                            .builder()
                                            .station(st.getId())
                                            .distanceWalking((int) (5 * (double) st.getTimeWalking() / 3600 * 1000))
                                            .timeWalking(st.getTimeWalking())
                                            .build())
                                    .collect(Collectors.toList()))
                            .build());
                })
                .map(cl -> true);

    }

    private AdapterView.OnItemClickListener getOnItemClickListener(SlotsAdapter slotsAdapter) {
        return (parent, view, position, id) -> {
            SlotBundle item = slotsAdapter.getItem(position);
            String text = String.format("<img src='ic_icons8_medical_doctor_18' />%s<br/><img src='ic_icons8_clinic_18' />%s<br/><img src='ic_icons8_calendar_18' />%s", item.getDoctorName(),
                    item.getClinic().getClinicName(),
                    DATE_FORMAT.withLocale(new Locale("ru")).print(item.getStartTime()) + " в " + item.getStartTime().toString(ONLY_TIME_FORMATTER));
            showConfirm("ЗАПИСЬ К ВРАЧУ",
                    Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY, ImageGetter.of(getContext()), null),
                    (dialog, which) -> {
                        sendConfirmRequest(item);
                        dialog.dismiss();
                    });

        };
    }

    private void sendConfirmRequest(SlotBundle item) {
        userRepository.getCurrentUser().observe(this, user -> {
            if (checkUser(user)) {
                docDocApi.submitRequest(RequestDto.builder()
                        .clientAge(Years.yearsBetween(user.getBirthDate(), LocalDate.now()).getYears())
                        .clientBirthday(user.getBirthDate().toString())
                        .clinic(item.getClinic().getClinicId())
                        .slotId(item.getSlotId())
                        .name(user.getFullName())
                        .doctor(item.getDoctorId())
                        .phone(user.getPhone())
                        .city(user.getDocCityId())
                        .build()).enqueue(new Callback<RequestResponseDto>() {
                    @Override
                    public void onResponse(Call<RequestResponseDto> call, Response<RequestResponseDto> response) {
                        Optional<RequestResponseDto.Response> docResponse = ofNullable(response.body()).map(RequestResponseDto::getResponse);
                        if (docResponse.isPresent()) {
                            onResult(docResponse.get().getMessage());
                            scheduleEvent(item, user);
                        } else {
                            onResult("Не удалось создать заявку на запись");
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestResponseDto> call, Throwable t) {
                        onResult("Не удалось создать заявку на запись");
                    }
                });
            }
        });
    }

    private void scheduleEvent(SlotBundle item, User user) {
        docDocApi.getClinicList(item.getClinic().getClinicId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(clinicList -> {
                    EventEntity.EventEntityBuilder eventBuild = EventEntity.builder()
                            .topDescription(item.getDoctorName())
                            .from(LocalDateTime.now())
                            .to(item.getStartTime())
                            .type(CardType.DOCTOR)
                            .active(true);
                    DoctorEventInfo.DoctorEventInfoBuilder infoBuilder = DoctorEventInfo.builder()
                            .clinic(item.getClinic().getClinicName())
                            .startTime(item.getStartTime())
                            .img(item.getDoctorImage())
                            .imgFormat(item.getDoctorImageFormat())
                            .street(item.getClinic().getStreet() + " " + item.getClinic().getHouse())
                            .fioDoctor(item.getDoctorName())
                            .ratingDescription(ratingDescription.getText().toString())
                            .rating(ratingBar.getRating())
                            .doctorBranch(doctorBranch.getText().toString())
                            .stations(doctor.getStations().stream()
                                    .filter(dcs -> ofNullable(item).map(SlotBundle::getClinic).map(ClinicsInfo::getStations).orElse(Collections.emptyList()).contains(dcs.getId()))
                                    .map(dcs -> {
                                        Optional<StationsDistance> stationDistance = ofNullable(item)
                                                .map(SlotBundle::getClinic)
                                                .map(ClinicsInfo::getStationsDistance)
                                                .orElse(Collections.emptyList())
                                                .stream()
                                                .filter(cls -> cls.getStation().toString().equalsIgnoreCase(dcs.getId()))
                                                .findFirst();
                                        return DoctorEventInfo.Stations.builder()
                                                .name(dcs.getName())
                                                .color(dcs.getLineColor())
                                                .timeWalking(stationDistance.map(StationsDistance::getTimeWalking).orElse(null))
                                                .distanceWalking(stationDistance.map(StationsDistance::getDistanceWalking).orElse(null))
                                                .build();
                                    }).collect(Collectors.toList()));
                    Optional<ClinicList> clinicOptional = clinicList.getClinicLists().stream().findFirst();
                    if (clinicOptional.isPresent()) {
                        ClinicList clinic = clinicOptional.get();
                        infoBuilder.doctorPhoneNumber(clinic.getPhoneAppointment());
                    }
                    EventEntity event = eventBuild
                            .content(DoctorEventInfo.toString(infoBuilder.build()))
                            .build();
                    eventRepository.insertSingle(event)
                            .subscribeOn(Schedulers.io())
                            .subscribe(eventEntity -> DoctorSchedulerManager.builder()
                                    .context(getContext())
                                    .reschedule(eventEntity));

                });

        if (user.getDoctorSaveToCalendar()) {
            Dexter.withActivity(getActivity())
                    .withPermissions(
                            Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
                    .withListener(new MultiplePermissionsListener() {

                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                Cursor cur = getContext().getContentResolver().query(CalendarContract.Calendars.CONTENT_URI, null, null, null, null);
                                if (cur.moveToFirst()) {
                                    long calendarID = cur.getLong(cur.getColumnIndex(CalendarContract.Calendars._ID));
                                    ContentValues eventValues = new ContentValues();
                                    eventValues.put(CalendarContract.Events.CALENDAR_ID, calendarID);
                                    eventValues.put(CalendarContract.Events.TITLE, "Запись к " + item.getDoctorName());
                                    eventValues.put(CalendarContract.Events.DESCRIPTION, " Врач: " + item.getDoctorName() + " Клиника: " + item.getClinic().getClinicName());
                                    eventValues.put(CalendarContract.Events.DTSTART, item.getStartTime().toDateTime().getMillis());
                                    eventValues.put(CalendarContract.Events.DTEND, item.getStartTime().plusMinutes(30).toDateTime().getMillis());
                                    eventValues.put(CalendarContract.Events.EVENT_LOCATION, item.getClinic().getStreet() + " " + item.getClinic().getHouse());
                                    String timeZone = TimeZone.getDefault().getID();
                                    eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone);
                                    getContext().getContentResolver().insert(CalendarContract.Events.CONTENT_URI, eventValues);
                                }
                            }
                            if (report
                                    .isAnyPermissionPermanentlyDenied()) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                       PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).
                    withErrorListener(
                            error -> Toast.makeText(getContext(), "Some Error! ", Toast.LENGTH_SHORT)
                                    .show())
                    .onSameThread()
                    .check();
        }
    }

    private boolean checkUser(User user) {
        StringJoiner sb = new StringJoiner(", ");
        if (isNull(user.getBirthDate())) {
            sb.add("Дату Рождения");
        }
        if (isBlankString(user.getFullName())) {
            sb.add("ФИО");
        }
        if (isBlankString(ifNotNullGet(user.getPhone(), phone -> phone.replaceAll("\\+7 \\(", "")))) {
            sb.add("Телефон");
        }
        if (sb.length() > 0) {
            showErrorDialog("Ошибка!", "Для создания заявки," +
                    "заполните пожалуйста следующие данные в профиле: " + sb.toString());
            return false;
        }
        return true;
    }

    private void showConfirm(final String title, final Spanned message, DialogInterface.OnClickListener confirm) {
        androidx.appcompat.app.AlertDialog aDialog = new MaterialAlertDialogBuilder(getContext()).setMessage(message).setTitle(title)
                .setNegativeButton("ОТМЕНИТЬ", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("ЗАПИСАТЬСЯ", confirm).create();
        aDialog.show();
    }

    private void showErrorDialog(final String title, final String message) {
        AlertDialog aDialog = new AlertDialog.Builder(getContext()).setMessage(message).setTitle(title)
                .setIcon(R.drawable.ic_info_black_24dp)
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).create();
        aDialog.show();
    }

    private Consumer<ClinicsInfo> fillClinicInfo(
            List<DoctorListDto.Station> stations, Context context) {
        return clinicInfo -> {
            LayoutParams lpView = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lpView.setMargins(0, 10, 0, 5);
            TextView clinicName = new TextView(context);
            clinicName.setText(clinicInfo.getClinicName());
            clinicName.setTextSize(16);
            clinicName.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,
                    R.drawable.ic_icons8_clinic_18), null, null, null);
            clinicName.setCompoundDrawablePadding(5);
            clinicName.setPadding(5, 0, 0, 0);
            clinicName.setLayoutParams(lpView);

            TextView clinicNameAdd = new TextView(context);
            clinicNameAdd.setText(clinicInfo.getStreet() + " " + clinicInfo.getHouse());
            clinicNameAdd.setTextSize(16);
            clinicNameAdd.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,
                    R.drawable.ic_icons8_map_marker_18), null, null, null);
            clinicNameAdd.setLayoutParams(lpView);
            clinicNameAdd.setCompoundDrawablePadding(5);
            clinicNameAdd.setPadding(5, 0, 0, 0);
            clinicsList.addView(clinicName);
            clinicsList.addView(clinicNameAdd);
            clinicInfo.getStationsDistance()
                    .stream()
                    .forEach(std -> {
                        LayoutParams lpViewForStation = new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT);
                        lpViewForStation.setMarginStart(50);
                        stations.stream()
                                .filter(station -> station.getId().equalsIgnoreCase(std.getStation().toString()))
                                .findAny()
                                .ifPresent(stationObj -> {
                                    final TextView stationTextView = StationWithColor
                                            .of(stationObj.getName(), stationObj.getLineColor(), context, lpViewForStation)
                                            .initWithDistanceWalking(std.getDistanceWalking(), std.getTimeWalking());
                                    stationTextView.setTextSize(14);
                                    clinicsList.addView(stationTextView);
                                });
                    });
        };
    }

    @AllArgsConstructor(staticName = "of")
    @Data
    public static class SlotBundle {
        private ClinicsInfo clinic;
        private LocalDateTime startTime;
        private String slotId;
        private Integer doctorId;
        private String doctorName;
        private String doctorImage;
        private String doctorImageFormat;
        private List<DoctorListDto.Station> stations;
        private String doctorPhoneNumber;
    }

    private void onResult(String message) {
        Toast.makeText(getContext(),
                message,
                Toast.LENGTH_LONG)
                .show();
    }
}
