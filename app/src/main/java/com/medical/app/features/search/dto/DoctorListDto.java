package com.medical.app.features.search.dto;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class DoctorListDto {

    public static DiffUtil.ItemCallback<DoctorDto> DIFF_CALLBACK = new DiffUtil.ItemCallback<DoctorDto>() {
        @Override
        public boolean areItemsTheSame(@NonNull DoctorDto oldItem, @NonNull DoctorDto newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull DoctorDto oldItem, @NonNull DoctorDto newItem) {
            return oldItem.getId().equals(newItem.getId()) && oldItem.getName()
                    .equalsIgnoreCase(newItem.getName());
        }
    };

    @SerializedName("Total")
    @Expose
    private Integer total;
    @SerializedName("DoctorList")
    @Expose
    private List<DoctorDto> doctorList = null;
    @SerializedName("Type")
    @Expose
    private String type;

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class DoctorDto {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Rating")
        @Expose
        private String rating;
        @SerializedName("Sex")
        @Expose
        private Integer sex;
        @SerializedName("Img")
        @Expose
        private String img;
        @SerializedName("AddPhoneNumber")
        @Expose
        private String addPhoneNumber;
        @SerializedName("Category")
        @Expose
        private String category;
        @SerializedName("Degree")
        @Expose
        private String degree;
        @SerializedName("Rank")
        @Expose
        private String rank;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("ExperienceYear")
        @Expose
        private Integer experienceYear;
        @SerializedName("Price")
        @Expose
        private Integer price;
        @SerializedName("SpecialPrice")
        @Expose
        private Object specialPrice;
        @SerializedName("Departure")
        @Expose
        private Integer departure;
        @SerializedName("Clinics")
        @Expose
        private List<Integer> clinics = null;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("Specialities")
        @Expose
        private List<Speciality> specialities = null;
        @SerializedName("Stations")
        @Expose
        private List<Station> stations = null;
        @SerializedName("BookingClinics")
        @Expose
        private List<Integer> bookingClinics = null;
        @SerializedName("isActive")
        @Expose
        private Boolean isActive;
        @SerializedName("TextAbout")
        @Expose
        private String textAbout;
        @SerializedName("InternalRating")
        @Expose
        private Double internalRating;
        @SerializedName("OpinionCount")
        @Expose
        private Integer opinionCount;
        @SerializedName("Extra")
        @Expose
        private Object extra;
        @SerializedName("KidsReception")
        @Expose
        private String kidsReception;
        @SerializedName("ClinicsInfo")
        @Expose
        private List<ClinicsInfo> clinicsInfo = null;
        @SerializedName("Telemed")
        @Expose
        private List<Telemed> telemed = null;
        @SerializedName("ImgFormat")
        @Expose
        private String imgFormat;
        @SerializedName("RatingLabel")
        @Expose
        private String ratingLabel;
        @SerializedName("FocusClinic")
        @Expose
        private Integer focusClinic;
        @SerializedName("IsExclusivePrice")
        @Expose
        private Boolean isExclusivePrice;
        @SerializedName("RatingReviewsLabel")
        @Expose
        private String ratingReviewsLabel;

        @SerializedName("Slots")
        @Expose
        private Map<String, List<Slot>> slots = null;

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }

            DoctorDto article = (DoctorDto) obj;
            return article.getId() == this.id;
        }

    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Slots {

        @Expose
        private List<Slot> slots = null;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Slot {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("FinishTime")
        @Expose
        private String finishTime;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Speciality {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("NameGenitive")
        @Expose
        private String nameGenitive;
        @SerializedName("NamePlural")
        @Expose
        private String namePlural;
        @SerializedName("NamePluralGenitive")
        @Expose
        private String namePluralGenitive;
        @SerializedName("IsSimple")
        @Expose
        private Boolean isSimple;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("BranchAlias")
        @Expose
        private String branchAlias;
    }

    @NoArgsConstructor
    @Data
    @Builder
    @AllArgsConstructor
    public static class Station {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("LineName")
        @Expose
        private String lineName;
        @SerializedName("LineColor")
        @Expose
        private String lineColor;
        @SerializedName("CityId")
        @Expose
        private String cityId;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("DistrictIds")
        @Expose
        private List<String> districtIds = null;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Telemed {

        @SerializedName("ClinicId")
        @Expose
        private Integer clinicId;
        @SerializedName("Chat")
        @Expose
        private Boolean chat;
        @SerializedName("Phone")
        @Expose
        private Boolean phone;
    }

    @NoArgsConstructor
    @Data
    @Builder
    @AllArgsConstructor
    public static class ClinicsInfo {

        @SerializedName("ClinicId")
        @Expose
        private Integer clinicId;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Stations")
        @Expose
        private List<String> stations = null;
        @SerializedName("Specialities")
        @Expose
        private List<Speciality_> specialities = null;
        @SerializedName("ClinicName")
        @Expose
        private String clinicName;
        @SerializedName("Street")
        @Expose
        private String street;
        @SerializedName("House")
        @Expose
        private String house;
        @SerializedName("StationsDistance")
        @Expose
        private List<StationsDistance> stationsDistance = null;
        @SerializedName("SlotsWork")
        @Expose
        private Boolean slotsWork;
        @SerializedName("DiscountCondition")
        @Expose
        private Integer discountCondition;
        @SerializedName("RequestFormSurname")
        @Expose
        private Boolean requestFormSurname;
        @SerializedName("RequestFormBirthday")
        @Expose
        private Boolean requestFormBirthday;
        @SerializedName("RequestFormClientAge")
        @Expose
        private Boolean requestFormClientAge;
        @SerializedName("Recommend")
        @Expose
        private Boolean recommend;

        private String phone;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Speciality_ {

        @SerializedName("DeparturePriceFrom")
        @Expose
        private Object departurePriceFrom;
        @SerializedName("DeparturePriceTo")
        @Expose
        private Object departurePriceTo;
        @SerializedName("SpecialityId")
        @Expose
        private Integer specialityId;
        @SerializedName("Price")
        @Expose
        private Integer price;
        @SerializedName("SpecialPrice")
        @Expose
        private Object specialPrice;
        @SerializedName("InternalRating")
        @Expose
        private Double internalRating;
        @SerializedName("HighlightDiscount")
        @Expose
        private Integer highlightDiscount;
        @SerializedName("StandardDocDoc")
        @Expose
        private Boolean standardDocDoc;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    @Builder
    public static class StationsDistance {

        @SerializedName("Station")
        @Expose
        private Integer station;
        @SerializedName("TimeWalking")
        @Expose
        private Integer timeWalking;
        @SerializedName("DistanceWalking")
        @Expose
        private Integer distanceWalking;
    }
}
