package com.medical.app.features.adapter.stream;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.medical.app.R;
import com.medical.app.dao.entity.StreamEntity;
import com.medical.app.dto.StreamDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
public class StreamByDateAdapter extends RecyclerView.Adapter<StreamByDateAdapter.StreamViewHolder> {
    @Builder.Default
    @Getter
    private List<StreamDto> values = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public StreamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_stream_by_date, parent, false);
        return new StreamByDateAdapter.StreamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StreamViewHolder holder, int position) {
        holder.bindData(values.get(position), context);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void setValues(List<StreamDto> values) {
        this.values = values;
        this.notifyDataSetChanged();
    }

    public static class StreamViewHolder extends RecyclerView.ViewHolder {
        private TextView topDescription;
        private RecyclerView streamByTime;

        public StreamViewHolder(@NonNull View itemView) {
            super(itemView);
            topDescription = itemView.findViewById(R.id.top_description);
            streamByTime = itemView.findViewById(R.id.list_of_streams_by_date);

        }

        public void bindData(StreamDto dto, Context context) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            streamByTime.setLayoutManager(linearLayoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                    streamByTime.getContext(),
                    linearLayoutManager.getOrientation());
            streamByTime.addItemDecoration(dividerItemDecoration);
            streamByTime.setAdapter(StreamByTimeAdapter.builder().values(dto.getValues().stream()
            .sorted(Comparator.comparing(StreamEntity::getCreateTime).reversed()).collect(Collectors.toList())).build());
            topDescription.setText(dto.getDate().toString("dd.MM.yyyy"));
        }
    }
}
