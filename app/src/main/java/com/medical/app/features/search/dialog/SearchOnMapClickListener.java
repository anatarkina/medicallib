package com.medical.app.features.search.dialog;

public interface SearchOnMapClickListener {
    void onMapClickListener(double latitude, double longitude);
}
