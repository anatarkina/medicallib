package com.medical.app.features.events;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.dao.entity.Tracker;
import com.medical.app.features.events.adapter.CardAdapter;
import com.medical.app.repository.EventRepository;
import com.medical.app.repository.PomRepository;
import com.medical.app.repository.TrackerRepository;

import org.joda.time.LocalDateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static android.content.Context.MODE_PRIVATE;
import static com.medical.app.dao.entity.Tracker.DataSet.DataSetType.LOWER_PRESSURE;
import static com.medical.app.dao.entity.Tracker.DataSet.DataSetType.UPPER_PRESSURE;
import static com.medical.app.repository.PomRepository.getPillCountPredicate;
import static com.medical.app.repository.PomRepository.getPillDurationPredicate;
import static com.medical.app.utils.NullFunctions.isBlankString;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

public class DashboardFragment extends Fragment {
    private CardAdapter cardAdapter;
    private EventRepository eventRepository;
    private PomRepository pomRepository;
    private TrackerRepository trackerRepository;
    private TextView activeText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dashboard, null);
        RecyclerView recyclerView = view.findViewById(R.id.dashboard_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        activeText = view.findViewById(R.id.active_txt);
        eventRepository = new EventRepository(getContext());
        pomRepository = new PomRepository(getContext());
        trackerRepository = new TrackerRepository(getContext());
        cardAdapter = CardAdapter.of(new ArrayList<>(), getActivity());
        recyclerView.setAdapter(cardAdapter);
        fillValues();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(),
                this.getClass().getSimpleName(), this.getClass().getSimpleName());
        return view;
    }

    private void fillValues() {
        eventRepository.getAll().observe(getViewLifecycleOwner(), events -> {
            List<EventEntity> cards = ofNullable(events)
                    .orElse(emptyList())
                    .stream()
                    .filter(EventEntity::isActive)
                    .filter(event -> {
                        boolean isAfter = LocalDateTime.now().isAfter(event.getFrom());
                        return event.getTo() != null ? isAfter && LocalDateTime.now().isBefore(event.getTo()) : isAfter;
                    })
                    .sorted(Comparator.comparing(event -> event.getType().getOrder()))
                    .collect(Collectors.toList());
            cardAdapter.setValues(cards);
            if (cards.isEmpty() && activeText != null) {
                activeText.setVisibility(View.VISIBLE);
            }
        });
        addPills();
        addTracker();
        if (isFirstRun()) {
            addOnBoarding();
        }
    }

    private void addTracker() {
        trackerRepository.getAllTrackers().observe(getViewLifecycleOwner(), trackers -> {
            AsyncTask.execute(() -> {
                eventRepository.deleteByType(CardType.TRACKER);
                String trackerDescription = trackers.stream()
                        .map(tracker -> {
                            Tracker.TrackerType type = tracker.getType();
                            switch (type) {
                                case IMPULSE_TRACKER:
                                    Tracker.DataSet upperPressure = tracker.getDataSets().stream()
                                            .filter(set -> UPPER_PRESSURE.equals(set.getType()))
                                            .sorted(Comparator.comparing(Tracker.DataSet::getDate))
                                            .limit(1).findFirst().orElse(null);
                                    Tracker.DataSet lowerPressure = tracker.getDataSets().stream()
                                            .filter(set -> LOWER_PRESSURE.equals(set.getType()))
                                            .sorted(Comparator.comparing(Tracker.DataSet::getDate))
                                            .limit(1).findFirst().orElse(null);
                                    BigDecimal upperPressureValue = ofNullable(upperPressure).map(Tracker.DataSet::getValue).orElse(null);
                                    BigDecimal lowerPressureValue = ofNullable(lowerPressure).map(Tracker.DataSet::getValue).orElse(null);
                                    if (upperPressureValue != null && lowerPressureValue != null) {
                                        return type.getName() + ": " +
                                                upperPressureValue + " / " + lowerPressureValue;
                                    }
                                case SUGAR_TRACKER:
                                case HEIGHT_TRACKER:
                                case WEIGHT_TRACKER:
                                    Tracker.DataSet dataSet = tracker.getDataSets().stream().sorted(Comparator.comparing(Tracker.DataSet::getDate))
                                            .limit(1).findFirst().orElse(null);
                                    if (dataSet != null && dataSet.getValue() != null) {
                                        return dataSet.getType().getName() + ": " + dataSet.getValue() + " " + dataSet.getType().getMeasure();
                                    }
                            }
                            return null;

                        })
                        .filter(item -> item != null)
                        .collect(Collectors.joining(","));
                if (!isBlankString(trackerDescription)) {
                    eventRepository.insert(EventEntity.builder()
                            .topDescription(CardType.TRACKER.getDescription())
                            .from(LocalDateTime.now())
                            .to(LocalDateTime.now().plusDays(1))
                            .active(true)
                            .type(CardType.TRACKER)
                            .content(trackerDescription)
                            .build());
                }
            });
        });
    }

    private void addPills() {
        pomRepository.getPomsWithPills().observe(getViewLifecycleOwner(), pomWithPills -> {
            CardType tablets = CardType.TABLETS;
            AsyncTask.execute(() -> {
                eventRepository.deleteByType(tablets);
                if (pomWithPills != null) {
                    List<Pill> collect = pomWithPills.stream().map(PomWithPills::getPills)
                            .flatMap(List::stream)
                            .filter(getPillDurationPredicate().and(getPillCountPredicate()))
                            .collect(Collectors.toList());
                    if (!collect.isEmpty()) {
                        List<String> pillsDescriptions = collect.stream()
                                .limit(2)
                                .sorted(Comparator.comparing(Pill::getOrder))
                                .map(Pill::getName).collect(Collectors.toList());
                        if (collect.size() > 2) {
                            int diff = collect.size() - 2;
                            pillsDescriptions.add("+" + diff + " лекарств" + (diff == 1 ? "о" : diff < 5 ? "а" : ""));
                        }
                        String descriptions = pillsDescriptions
                                .stream()
                                .collect(Collectors.joining(","));
                        eventRepository.insert(EventEntity.builder()
                                .topDescription(tablets.getDescription())
                                .from(LocalDateTime.now())
                                .to(LocalDateTime.now().plusDays(1))
                                .active(true)
                                .type(tablets)
                                .content(descriptions)
                                .build());
                    }
                }


            });
        });
    }

    private void addOnBoarding() {
        AsyncTask.execute(() -> {
            eventRepository.insert(EventEntity.builder()
                    .topDescription(CardType.ONB_INFO.getDescription())
                    .from(LocalDateTime.now())
                    .active(true)
                    .type(CardType.ONB_INFO)
                    .content(getResources().getString(R.string.onb_content))
                    .build());
            eventRepository.insert(EventEntity.builder()
                    .topDescription(CardType.ONB_TRACKER.getDescription())
                    .from(LocalDateTime.now())
                    .active(true)
                    .type(CardType.ONB_TRACKER)
                    .content(getResources().getString(R.string.onb_tracker))
                    .build());
            eventRepository.insert(EventEntity.builder()
                    .topDescription(CardType.ONB_DOCTOR.getDescription())
                    .from(LocalDateTime.now())
                    .active(true)
                    .type(CardType.ONB_DOCTOR)
                    .content(getResources().getString(R.string.onb_doctor))
                    .build());
            eventRepository.insert(EventEntity.builder()
                    .topDescription(CardType.ONB_HISTORY.getDescription())
                    .from(LocalDateTime.now())
                    .active(true)
                    .type(CardType.ONB_HISTORY)
                    .content(getResources().getString(R.string.onb_history))
                    .build());
        });
    }

    private boolean isFirstRun() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(this.getClass().getSimpleName(), MODE_PRIVATE);
        SharedPreferences.Editor editor;
        boolean isFirstRun = false;
        if (sharedPreferences.getBoolean("firstRun", true)) {
            isFirstRun = true;
            editor = sharedPreferences.edit();
            editor.putBoolean("firstRun", false);
            editor.commit();
        }
        return isFirstRun;
    }
}

