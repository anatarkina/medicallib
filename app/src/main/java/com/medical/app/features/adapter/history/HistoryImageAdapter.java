package com.medical.app.features.adapter.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.bumptech.glide.Glide;
import com.medical.app.R;
import java.util.List;
import lombok.Builder;

@Builder
public class HistoryImageAdapter extends RecyclerView.Adapter<HistoryImageAdapter.ImageViewHolder> {

  @Builder.Default
  private List<String> urls;
  private int size;
  private Context context;

  @NonNull
  @Override
  public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    final View layout = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.item_image_history, viewGroup, false);
    layout.setPadding(0, 10, 10, 0);
    return new ImageViewHolder(layout);
  }

  @Override
  public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
    holder.bindData(context, urls.get(position), position, size);
  }

  @Override
  public int getItemCount() {
    return urls.size();
  }


  public static class ImageViewHolder extends ViewHolder {

    private ImageView imageView;
    private TextView label;

    public ImageViewHolder(View view) {
      super(view);
      this.imageView = view.findViewById(R.id.history_image);
      this.label = view.findViewById(R.id.history_title);
    }

    public void bindData(Context context, String url, int position, int size) {
      final int target = position + 1;
      final int itemHidden = size - target;
      if (target == 5 && itemHidden != 0) {
        label.setText("+" + itemHidden);
      } else {
        label.setVisibility(View.INVISIBLE);
      }
      Glide.with(context)
          .load(url)
          .centerCrop()
          .into(imageView);
    }
  }
}
