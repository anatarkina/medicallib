package com.medical.app.features.notes;

import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.leinardi.android.speeddial.SpeedDialView;
import com.medical.app.R;
import com.medical.app.dao.entity.PomWithPills;
import com.medical.app.features.adapter.pom.PomAdapter;
import com.medical.app.repository.PomRepository;

import java.util.List;

import lombok.Builder;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

@Builder
public class PomFragment extends Fragment {

    private PomAdapter adapter;
    private PomRepository pomRepository;
    private TextView activeText;
    private SpeedDialView fabMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_pom, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.pom_list);
        final FragmentActivity activity = getActivity();
        pomRepository = new PomRepository(activity);
        activeText = view.findViewById(R.id.active_txt);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        new Thread(() -> {
            adapter = PomAdapter.builder()
                    .context(activity)
                    .fragmentManager(activity.getSupportFragmentManager())
                    .pomRepository(pomRepository)
                    .build();
            activity.runOnUiThread(() -> recyclerView.setAdapter(adapter));
        }).start();
        observerSetup();
        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());

        return view;
    }

    private void observerSetup() {
        pomRepository.getPomsWithPills().observe(getViewLifecycleOwner(), poms -> {
            List<PomWithPills> pomWithPills = ofNullable(poms)
                    .orElse(emptyList());
            adapter.setPoms(pomWithPills);
            if (activeText != null) {
                if (pomWithPills.isEmpty()) {
                    if (fabMenu != null) {
                        activeText.setVisibility(View.VISIBLE);
                        ViewTooltip
                                .on(this, fabMenu)
                                .autoHide(true, 1000)
                                .corner(50)
                                .color(getActivity().getColor(R.color.lightGrey))
                                .textColor(getActivity().getColor(R.color.back_button))
                                .textSize(TypedValue.COMPLEX_UNIT_SP, 16)
                                .position(ViewTooltip.Position.TOP)
                                .text("Нажми чтобы добавить")
                                .show();
                    }
                } else {
                    activeText.setVisibility(View.GONE);
                }
            }
        });
    }
}
