package com.medical.app.features.search.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class DoctorResponseDto {

    @SerializedName("Doctor")
    @Expose
    private List<Doctor> doctorList = null;

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Doctor {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Rating")
        @Expose
        private String rating;
        @SerializedName("Sex")
        @Expose
        private Integer sex;
        @SerializedName("Img")
        @Expose
        private String img;
        @SerializedName("AddPhoneNumber")
        @Expose
        private String addPhoneNumber;
        @SerializedName("Category")
        @Expose
        private String category;
        @SerializedName("Degree")
        @Expose
        private String degree;
        @SerializedName("Rank")
        @Expose
        private String rank;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("TextEducation")
        @Expose
        private String textEducation;
        @SerializedName("TextAssociation")
        @Expose
        private String textAssociation;
        @SerializedName("TextDegree")
        @Expose
        private String textDegree;
        @SerializedName("TextSpec")
        @Expose
        private String textSpec;
        @SerializedName("TextCourse")
        @Expose
        private String textCourse;
        @SerializedName("TextExperience")
        @Expose
        private String textExperience;
        @SerializedName("ExperienceYear")
        @Expose
        private Integer experienceYear;
        @SerializedName("Price")
        @Expose
        private Integer price;
        @SerializedName("SpecialPrice")
        @Expose
        private Object specialPrice;
        @SerializedName("Departure")
        @Expose
        private Integer departure;
        @SerializedName("Clinics")
        @Expose
        private List<Integer> clinics = null;
        @SerializedName("Alias")
        @Expose
        private String alias;
        @SerializedName("BookingClinics")
        @Expose
        private List<Object> bookingClinics = null;
        @SerializedName("isActive")
        @Expose
        private Boolean isActive;
        @SerializedName("KidsReception")
        @Expose
        private Integer kidsReception;
        @SerializedName("ClinicsInfo")
        @Expose
        private List<ClinicsInfo> clinicsInfo = null;
        @SerializedName("OpinionCount")
        @Expose
        private Integer opinionCount;
        @SerializedName("TextAbout")
        @Expose
        private String textAbout;
        @SerializedName("ImgFormat")
        @Expose
        private String imgFormat;
        @SerializedName("Surname")
        @Expose
        private String surname;
        @SerializedName("RatingLabel")
        @Expose
        private String ratingLabel;
        @SerializedName("Achievements")
        @Expose
        private Achievements achievements;
        @SerializedName("Specialization")
        @Expose
        private List<Object> specialization = null;
        @SerializedName("RatingReviewsLabel")
        @Expose
        private String ratingReviewsLabel;
        @SerializedName("IsExclusivePrice")
        @Expose
        private Boolean isExclusivePrice;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class ClinicsInfo {
        @SerializedName("ClinicId")
        @Expose
        private Integer clinicId;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Stations")
        @Expose
        private List<String> stations = null;
        @SerializedName("ClinicName")
        @Expose
        private String clinicName;
        @SerializedName("Street")
        @Expose
        private String street;
        @SerializedName("House")
        @Expose
        private String house;
        @SerializedName("SlotsWork")
        @Expose
        private Boolean slotsWork;
        @SerializedName("DiscountCondition")
        @Expose
        private Integer discountCondition;
        @SerializedName("RequestFormSurname")
        @Expose
        private Boolean requestFormSurname;
        @SerializedName("RequestFormBirthday")
        @Expose
        private Boolean requestFormBirthday;
        @SerializedName("RequestFormClientAge")
        @Expose
        private Boolean requestFormClientAge;
        @SerializedName("Recommend")
        @Expose
        private Boolean recommend;
    }


    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Course {

        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Organization")
        @Expose
        private String organization;
        @SerializedName("Year")
        @Expose
        private String year;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Achievements {

        @SerializedName("Courses")
        @Expose
        private List<Course> courses = null;
        @SerializedName("Education")
        @Expose
        private List<Education> education = null;
        @SerializedName("Experience")
        @Expose
        private List<Experience> experience = null;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Experience {

        @SerializedName("YearBegin")
        @Expose
        private String yearBegin;
        @SerializedName("YearEnd")
        @Expose
        private String yearEnd;
        @SerializedName("City")
        @Expose
        private String city;
        @SerializedName("Organization")
        @Expose
        private String organization;
        @SerializedName("Position")
        @Expose
        private String position;
    }

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public class Education {
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Type")
        @Expose
        private String type;
        @SerializedName("Specialization")
        @Expose
        private String specialization;
        @SerializedName("Year")
        @Expose
        private String year;
    }
}
