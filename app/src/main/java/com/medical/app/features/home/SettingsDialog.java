package com.medical.app.features.home;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.medical.app.R;
import com.medical.app.dao.entity.User;
import com.medical.app.repository.UserRepository;
import com.medical.app.scheduler.PillSchedulerManager;
import com.medical.app.view.TimeEditTextView;

import org.joda.time.LocalTime;

import static java.util.Optional.ofNullable;

public class SettingsDialog extends DialogFragment {
    private static final String TAG = "SETTINGS_EDITOR";
    private Button save, decline;
    private SwitchMaterial notificationPills, notificationDoctor, doctorSaveToCalendar;
    private TimeEditTextView startTimeNotification, endTimeNotification;
    private UserRepository userRepository;
    private RadioGroup radioGroup;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_FullScreenDialog);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_settings, null);
        save = dialogLayout.findViewById(R.id.save_btn);
        decline = dialogLayout.findViewById(R.id.decline_btn);
        notificationPills = dialogLayout.findViewById(R.id.notification_pills);
        notificationDoctor = dialogLayout.findViewById(R.id.notification_doctor);
        doctorSaveToCalendar = dialogLayout.findViewById(R.id.doctor_save_to_calendar);
        startTimeNotification = dialogLayout.findViewById(R.id.start_time_text);
        endTimeNotification = dialogLayout.findViewById(R.id.end_time_text);
        radioGroup = dialogLayout.findViewById(R.id.set_radio_group);

        FirebaseAnalytics.getInstance(getContext()).setCurrentScreen(getActivity(), TAG, SettingsDialog.class.getSimpleName());
        decline.setOnClickListener(v -> dismiss());
        save.setOnClickListener(getSaveListener());
        userRepository = new UserRepository(getContext());
        dialog.setContentView(dialogLayout);
        fillValues();
        return dialog;
    }

    private View.OnClickListener getSaveListener() {
        return v -> {
            Toast.makeText(getContext(), "Настройки сохранены", Toast.LENGTH_LONG).show();

            userRepository.updateSettingsFields(startTimeNotification.getLocalTime(), endTimeNotification.getLocalTime(), notificationPills.isChecked()
                    , notificationDoctor.isChecked(), doctorSaveToCalendar.isChecked(), getNumberNotifications());
            rescheduleNotifications();
            dismiss();
        };
    }

    private Integer getNumberNotifications() {
        int id = radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.one_set:
                return 1;
            case R.id.two_sets:
                return 2;
            case R.id.three_sets:
                return 3;
            case R.id.four_sets:
                return 4;
            case R.id.five_sets:
                return 5;
            case R.id.six_sets:
                return 6;
        }
        return 3;
    }

    private void fillValues() {
        userRepository.getCurrentUser().observe(getActivity(), user -> {
            notificationDoctor.setChecked(ofNullable(user).map(User::getNotificationDoctor).orElse(true));
            notificationPills.setChecked(ofNullable(user).map(User::getNotificationPills).orElse(true));
            doctorSaveToCalendar.setChecked(ofNullable(user).map(User::getDoctorSaveToCalendar).orElse(true));
            startTimeNotification.setFormattedText(ofNullable(user).map(User::getNotifyStart).orElse(new LocalTime(8, 0)));
            endTimeNotification.setFormattedText(ofNullable(user).map(User::getNotifyEnd).orElse(new LocalTime(22, 0)));
            radioGroup.check(ofNullable(user).map(User::getNumberNotifications).map(this::getGroupSet).orElse(R.id.three_sets));
        });
    }

    private Integer getGroupSet(Integer number) {
        switch (number) {
            case 1:
                return R.id.one_set;
            case 2:
                return R.id.two_sets;
            case 3:
                return R.id.three_sets;
            case 4:
                return R.id.four_sets;
            case 5:
                return R.id.five_sets;
            case 6:
                return R.id.six_sets;
        }
        return R.id.three_sets;
    }


    private void rescheduleNotifications() {
        PillSchedulerManager.builder()
                .context(getActivity().getApplicationContext())
                .reschedule();
    }
}
