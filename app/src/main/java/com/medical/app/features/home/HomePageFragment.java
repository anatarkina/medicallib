package com.medical.app.features.home;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.medical.app.configs.ApiConstants.GoogleConst.SELECTED_TAB;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.medical.app.R;
import com.medical.app.features.SelectableTabFragment;
import com.medical.app.features.adapter.MainFragmentAdapter;
import com.medical.app.features.events.DashboardFragment;
import com.medical.app.repository.UserRepository;

import java.util.Set;

public class HomePageFragment extends Fragment implements SelectableTabFragment {

    private TextView topFrameText;
    private ImageView profileImage;
    private MainFragmentAdapter fapAdapter;
    private TabLayout tabs;
    private ViewPager viewPager;
    private UserRepository userRepository;
    private MaterialToolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_profile, null);
        viewPager = view.findViewById(R.id.viewpager);
        tabs = view.findViewById(R.id.tabs);
        topFrameText = view.findViewById(R.id.top_frame_text);
        profileImage = view.findViewById(R.id.profile_image);
        userRepository = new UserRepository(getContext());
        toolbar = view.findViewById(R.id.my_toolbar);
        toolbar.inflateMenu(R.menu.settings_menu);
        toolbar.setOnMenuItemClickListener(getOnMenuClickListener());
        fapAdapter = new MainFragmentAdapter(getChildFragmentManager(),
                BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        if (savedInstanceState == null) {
            setupProfileViewPager();
        }
        loadData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupProfileViewPager();
    }

    private Toolbar.OnMenuItemClickListener getOnMenuClickListener() {
        return item -> {
            int itemId = item.getItemId();
            switch (itemId) {
                case R.id.action_profile:
                    new ProfileDialog().show(getChildFragmentManager(), ProfileDialog.class.getSimpleName());
                    return true;
                case R.id.action_settings:
                    new SettingsDialog().show(getChildFragmentManager(), SettingsDialog.class.getSimpleName());
                    return true;
            }
            return false;
        };
    }

    private void setupProfileViewPager() {
        fapAdapter.clear();
        fapAdapter.addFragment(new DashboardFragment(), getString(R.string.dashboard_text));
        fapAdapter.addFragment(new TrackerFragment(), getString(R.string.tracker_text));
        fapAdapter.addFragment(new StreamFragment(), getString(R.string.steam_txt));
        viewPager.setAdapter(fapAdapter);
        tabs.setupWithViewPager(viewPager);
        Bundle arguments = getArguments();
        if (arguments != null) {
            tabs.getTabAt(arguments.getInt(SELECTED_TAB)).select();
        }
    }


    private void loadData() {
        userRepository.getCurrentUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                topFrameText.setText(user.getFullName());
                if (getContext() != null) {
                    Glide.with(getContext()).load(user.getAvatarUrl())
                            .error(ContextCompat.getDrawable(getContext(), R.drawable.default_avatar))
                            .into(profileImage);
                }
            }
        });
    }


    @Override
    public void callWithArguments(Bundle bundle) {
        tabs.getTabAt(bundle.getInt(SELECTED_TAB)).select();
    }
}
