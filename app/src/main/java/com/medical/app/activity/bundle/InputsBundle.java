package com.medical.app.activity.bundle;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.medical.app.dao.entity.Tracker.DataSet.DataSetType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class InputsBundle {

  private DataSetType type;
  private TextInputLayout textInputLayout;
  private TextInputEditText value;
}