package com.medical.app.activity.bundle;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class LocationBundle {

  private Double lat, lng;
}
