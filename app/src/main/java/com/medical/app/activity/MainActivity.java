package com.medical.app.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.medical.app.R;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.features.SelectableTabFragment;
import com.medical.app.features.events.dialog.DoctorEventDialog;
import com.medical.app.features.home.HomePageFragment;
import com.medical.app.features.notes.NotesPageFragment;

import com.medical.app.features.search.SearchMainFragment;
import com.medical.app.features.search.dialog.DoctorReviewDialog;
import com.medical.app.repository.EventRepository;

import static com.medical.app.configs.ApiConstants.Flags.ACTION;
import static com.medical.app.configs.ApiConstants.Flags.CALL_DOCTOR;
import static com.medical.app.configs.ApiConstants.Flags.ID;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView navigation;
    private NotesPageFragment notesPageFragment;
    private HomePageFragment profilePageFragment;
    private SearchMainFragment searchMainFragment;
    private Bundle arguments;

    private FragmentManager myFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notesPageFragment = new NotesPageFragment();
        profilePageFragment = new HomePageFragment();
        searchMainFragment = new SearchMainFragment();
        navigation = findViewById(R.id.bottom_main);
        myFragmentManager = getSupportFragmentManager();
        navigation.setOnNavigationItemSelectedListener(this);
        FragmentTransaction fragmentTransaction = myFragmentManager
                .beginTransaction();
        fragmentTransaction
                .add(R.id.main_container, profilePageFragment, HomePageFragment.class.getName());
        fragmentTransaction.commit();
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("MainActivity", "FCM Registration Token: " + token);
        FirebaseMessaging.getInstance().subscribeToTopic("mimi");
    }

    private void doWithAction(String action, Intent intent) {
        if (action != null) {
            switch (action) {
                case CALL_DOCTOR:
                    AsyncTask.execute(() -> {
                        long id = intent.getLongExtra(ID, 0);
                        EventRepository eventRepository = new EventRepository(this);
                        EventEntity event = eventRepository.findById(id);
                        if (event != null) {
                            DoctorEventDialog.of(event)
                                    .show(getSupportFragmentManager(), DoctorReviewDialog.class.getSimpleName());
                        }

                    });
                    break;
            }
        }
    }

    @Override
    public void onNewIntent(@SuppressLint("UnknownNullness") Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getStringExtra(ACTION);
        doWithAction(action, intent);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.bt_user_item) {
            Log.i(MainActivity.class.getName(), "User Item selected");
            setupFragment(profilePageFragment, HomePageFragment.class.getName());
            return true;
        }
        if (itemId == R.id.bt_notes_item) {
            Log.i(MainActivity.class.getName(), "Notes Item selected");
            setupFragment(notesPageFragment, NotesPageFragment.class.getName());
            return true;
        }
        if (itemId == R.id.bt_find_item) {
            Log.i(MainActivity.class.getName(), "Find Item selected");
            setupFragment(searchMainFragment, SearchMainFragment.class.getName());
            return true;
        }
        if (itemId == R.id.bt_forum_item) {
            onError("Функционал находится в разработке! Ожидайте в следующих версиях");
            return false;
        }
        return false;
    }

    private void setupFragment(
            Fragment fragmentNew, String tag) {
        final Fragment fragment = myFragmentManager.findFragmentByTag(tag);
        if (fragment == null) {
            if (arguments != null) {
                fragmentNew.setArguments(arguments);
                arguments = null;
            }
            FragmentTransaction fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.main_container, fragmentNew,
                    tag);
            fragmentTransaction.commit();
        } else {
            if (fragment instanceof SelectableTabFragment && arguments != null) {
                ((SelectableTabFragment) fragment).callWithArguments(arguments);
                arguments = null;
            }
        }
    }

    public void setSelectedFragment(int itemId, Bundle arguments) {
        this.arguments = arguments;
        navigation.setSelectedItemId(itemId);
    }

    private void onError(String message) {
        Toast
                .makeText(this,
                        message,
                        Toast.LENGTH_SHORT)
                .show();
    }
}

