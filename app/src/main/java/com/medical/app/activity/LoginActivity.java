package com.medical.app.activity;

import static java.util.Optional.ofNullable;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.AuthUI.IdpConfig;
import com.firebase.ui.auth.FirebaseUiException;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.medical.app.R;
import com.medical.app.dao.entity.User;
import com.medical.app.repository.UserRepository;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

  private static final String TAG = "LoginActivity";
  private static final int RC_SIGN_IN = 1001;
  private UserRepository userRepository;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    // Choose authentication providers
    List<IdpConfig> providers = Arrays.asList(
        new AuthUI.IdpConfig.PhoneBuilder().build(),
        new AuthUI.IdpConfig.EmailBuilder().build(),
        new AuthUI.IdpConfig.GoogleBuilder().build(),
        new AuthUI.IdpConfig.FacebookBuilder().build());
    ActivityOptions options = ActivityOptions
        .makeCustomAnimation(getApplicationContext(), R.anim.sd_fade_and_translate_in,
            R.anim.sd_scale_fade_and_translate_out);
    final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    userRepository = new UserRepository(this);
    if (currentUser == null) {
      startActivityForResult(
          AuthUI.getInstance()
              .createSignInIntentBuilder()
              .setAvailableProviders(providers)
              .setTheme(R.style.LoginTheme)
              .setLogo(R.mipmap.icon_main)
              .build()
          , RC_SIGN_IN
          , options.toBundle()
      );
    } else {
      saveUserInfo(currentUser);
      goMainScreen();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == RC_SIGN_IN) {
      IdpResponse response = IdpResponse.fromResultIntent(data);

      if (resultCode == RESULT_OK) {
        // Successfully signed in
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        saveUserInfo(user);
        goMainScreen();
      } else {
        Toast.makeText(this, "" + ofNullable(response).map(IdpResponse::getError).map(
            FirebaseUiException::getMessage).orElse("Empty response"), Toast.LENGTH_LONG).show();
      }
    }
  }


  private void goMainScreen() {
    Intent intent = new Intent(this, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
        | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }

  private void saveUserInfo(FirebaseUser account) {
    userRepository.insertIfNotExists(User.builder()
        .id(account.getUid())
        .avatarUrl(ofNullable(account.getPhotoUrl()).map(Object::toString).orElse(null))
        .email(account.getEmail())
        .phone(account.getPhoneNumber())
        .fullName(account.getDisplayName())
        .build());
  }
}
