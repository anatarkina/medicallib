package com.medical.app.configs;

public class ApiConstants {

    public static class GoogleConst {
        public static final int LOCATION_REQUEST = 1000;
        public static final int GPS_REQUEST = 1001;
        public static final String SELECTED_TAB = "SELECTED_TAB";
    }

    public static class CommonParams {
        public static final String DB_NAME = "mimi-database-new-10";
        public static final int PICK_CAMERA_IMAGE = 2;
    }

    public static class Flags {
        public static final String ID = "id";
        public static final String ACTION = "ACTION";
        public static final String CALL_DOCTOR = "CALL_DOCTOR";
    }
}
