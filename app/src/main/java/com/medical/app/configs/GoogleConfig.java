package com.medical.app.configs;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public class GoogleConfig {

  public static final GoogleSignInOptions GSO = new GoogleSignInOptions.Builder(
      GoogleSignInOptions.DEFAULT_SIGN_IN)
      .requestIdToken(
          "865525881997-bp5h25rh40d8h5o1qnk2fv3t9f8sijqo.apps.googleusercontent.com")
      .requestEmail()
      .build();

}
