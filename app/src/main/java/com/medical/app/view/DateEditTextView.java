package com.medical.app.view;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;
import com.google.common.base.Strings;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateEditTextView extends TextInputEditText {

    public static final String DATE_MASK = "[00]{.}[00]{.}[9900]";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat
            .forPattern("dd.MM.yyyy");

    public DateEditTextView(Context context) {
        super(context);
        init();
    }

    public DateEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DateEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        MaskedTextChangedListener.Companion.installOn(this, DATE_MASK,
                null);
    }

    public LocalDate getDate() {
        final String date = getDateString();
        if (!Strings.isNullOrEmpty(date)) {
            try {
                return DATE_FORMATTER.parseLocalDate(date);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
        return null;
    }

    @NotNull
    public String getDateString() {
        return getText().toString();
    }

    public void setFormattedText(LocalDate date) {
        this.setText(date.toString(DATE_FORMATTER));
    }

    public void setFormattedText(LocalDateTime date) {
        this.setText(date.toString(DATE_FORMATTER));
    }

    public LocalDateTime getLocalDateTime() {
        final LocalDateTime currentTime = new LocalDateTime();
        final String date = getDateString();
        try {
            if (!Strings.isNullOrEmpty(date)) {
                return LocalDateTime
                        .parse(date, DATE_FORMATTER)
                        .withHourOfDay(currentTime.getHourOfDay())
                        .withMinuteOfHour(currentTime.getMinuteOfHour());
            }
        } catch (Exception e) {
            return currentTime;
        }
        return currentTime;
    }

}
