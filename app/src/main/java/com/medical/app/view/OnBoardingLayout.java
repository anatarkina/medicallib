package com.medical.app.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.activity.MainActivity;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.features.home.ProfileDialog;
import com.medical.app.repository.EventRepository;
import org.joda.time.LocalDateTime;

import static com.medical.app.configs.ApiConstants.GoogleConst.SELECTED_TAB;

public class OnBoardingLayout extends RelativeLayout {
    private EventEntity dataModel;
    private MaterialCardView root;
    private FragmentActivity activity;

    public OnBoardingLayout(MaterialCardView root, FragmentActivity activity, EventEntity dataModel) {
        super(activity);
        this.activity = activity;
        this.dataModel = dataModel;
        this.root = root;
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.card_onboarding, this);
        TextView description = view.findViewById(R.id.description);
        Button skip = view.findViewById(R.id.later_btn);
        Button add = view.findViewById(R.id.add_btn);
        description.setText(dataModel.getContent());
        root.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGrey));
        skip.setOnClickListener(v -> moveToTomorrow());

        add.setOnClickListener(v -> {
            makeInactive();
            switch (dataModel.getType()) {
                case ONB_INFO:
                    new ProfileDialog().show(activity.getSupportFragmentManager(), ProfileDialog.class.getSimpleName());
                    break;
                case ONB_TRACKER:
                    if (activity instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) this.activity;
                        Bundle bundle = new Bundle();
                        bundle.putInt(SELECTED_TAB, 1);
                        mainActivity.setSelectedFragment(R.id.bt_user_item, bundle);
                        Toast.makeText(activity, "Здесь вы могли бы добавить показатели", Toast.LENGTH_LONG).show();
                    }
                    break;
                case ONB_DOCTOR:
                    if (activity instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) this.activity;
                        mainActivity.setSelectedFragment(R.id.bt_find_item, null);
                        Toast.makeText(activity, "Здесь вы могли бы найти нужного вам врача", Toast.LENGTH_LONG).show();
                    }
                    break;
                case ONB_HISTORY:
                    if (activity instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) this.activity;
                        Bundle bundle = new Bundle();
                        bundle.putInt(SELECTED_TAB, 1);
                        mainActivity.setSelectedFragment(R.id.bt_notes_item, bundle);
                        Toast.makeText(activity, "Создавай историю болезни и не забудь приложить нужные фотогграфии", Toast.LENGTH_LONG).show();
                    }
                    break;

            }
        });
    }

    private void moveToTomorrow() {
        dataModel.setFrom(LocalDateTime.now().plusDays(1).withHourOfDay(0));
        EventRepository eventRepository = new EventRepository(activity);
        eventRepository.insert(dataModel);
    }

    private void makeInactive() {
        dataModel.setActive(false);
        EventRepository eventRepository = new EventRepository(activity);
        eventRepository.insert(dataModel);
    }
}
