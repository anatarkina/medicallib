package com.medical.app.view;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import com.medical.app.view.callback.OnExpandCollapseCallback;
import lombok.Setter;

public class CollapsingLinearLayout extends LinearLayout {

  private boolean expanded = true;
  @Setter
  private OnExpandCollapseCallback onCollapseCallback;

  public CollapsingLinearLayout(Context context) {
    super(context);
    init();
  }

  public CollapsingLinearLayout(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CollapsingLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  public CollapsingLinearLayout(Context context, AttributeSet attrs, int defStyleAttr,
      int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init();
  }

  public void applyController(TextView view) {
    String text = expanded ? "<img src='ic_expand_less_black_24dp' />"
        : "<img src='ic_expand_more_black_24dp' />";
    view.setText(
        Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY, ImageGetter.of(view.getContext()), null));
    view.setOnClickListener(getOnClickListener(view));
  }

  private OnClickListener getOnClickListener(TextView view) {
    return v -> {
      if (!expanded) {
        expand(this, view);
      } else {
        collapse(this, view);
      }
    };
  }

  private void makeImageExpand(TextView view) {
    String text = expanded ? "<img src='ic_expand_less_black_24dp' />"
        : "<img src='ic_expand_more_black_24dp' />";
    view.setText(
        Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY, ImageGetter.of(view.getContext()),
            null));
  }


  public void expand(View _headerLayout, TextView view) {
    Animation _showAnimation = new Animation() {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) _headerLayout
            .getLayoutParams();
        params.topMargin = (int) (getHeaderHeight(_headerLayout) * (interpolatedTime - 1));
        _headerLayout.setLayoutParams(params);
        _headerLayout.setVisibility(VISIBLE);

      }
    };
    _showAnimation.setDuration(
        (int) (getHeaderHeight(_headerLayout) / _headerLayout.getContext().getResources()
            .getDisplayMetrics().density));
    _headerLayout.clearAnimation();
    _headerLayout.startAnimation(_showAnimation);
    expanded = !expanded;
    makeImageExpand(view);
    if (onCollapseCallback != null) {
      onCollapseCallback.onCollapse(expanded);
    }
  }


  private int getHeaderHeight(View _headerLayout) {
    _headerLayout.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    return _headerLayout.getMeasuredHeight();
  }

  public void collapse(View _headerLayout, TextView view) {
    Animation _hideAnimation = new Animation() {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        LayoutParams params = (LayoutParams) _headerLayout.getLayoutParams();
        params.topMargin = -(int) (getHeaderHeight(_headerLayout) * interpolatedTime);
        _headerLayout.setLayoutParams(params);
        _headerLayout.setVisibility(GONE);
      }
    };

    _hideAnimation.setDuration((long) Math.abs(
        (_headerLayout.getLayoutParams().height) / _headerLayout.getContext().getResources()
            .getDisplayMetrics().density));
    _headerLayout.clearAnimation();
    _headerLayout.startAnimation(_hideAnimation);
    expanded = !expanded;
    makeImageExpand(view);
    if (onCollapseCallback != null) {
      onCollapseCallback.onCollapse(expanded);
    }
  }

  private void init() {
    LinearLayout.LayoutParams lpView = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT);
    //lpView.topMargin = -(int) (getHeaderHeight(this));
    this.setLayoutParams(lpView);
    this.setVisibility(VISIBLE);
  }
}
