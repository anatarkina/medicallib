package com.medical.app.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.medical.app.R;

import org.joda.time.Duration;

import lombok.AllArgsConstructor;

import static java.util.Optional.ofNullable;

@AllArgsConstructor(staticName = "of")
public class StationWithColor {

    private String stationName;
    private String lineColor;
    private Context context;
    private LayoutParams layoutParams;

    public TextView init() {
        TextView textView = new TextView(context);
        return init(textView);
    }

    private GradientDrawable getCircle(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setCornerRadii(new float[]{0, 0, 0, 0, 0, 0, 0, 0});
        shape.setColor(color);
        shape.setSize(30, 30);
        return shape;
    }


    public TextView init(TextView textView) {
        textView.setText("  " + stationName + "  ");
        textView.setTextSize(12);
        textView.setLayoutParams(layoutParams);
        textView.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_light));
        textView.setCompoundDrawablesWithIntrinsicBounds(
                getCircle(Color.parseColor("#" + lineColor)), null, null, null);
        return textView;
    }

    public TextView initWithDistanceWalking(Integer distanceWalking, Integer timeWalking) {
        TextView textView = new TextView(context);

        if (distanceWalking != null && timeWalking != null) {
            String text = " &nbsp;" + stationName + " " + ofNullable(distanceWalking).map(dw -> " " + dw + " м").orElse("")
                    + ofNullable(timeWalking).map(wk -> " <img src='ic_directions_run' />" + Duration.standardSeconds(timeWalking).getStandardMinutes() + " мин  ")
                    .orElse("");
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY, ImageGetter.of(context), null));
            textView.setTextSize(12);
            textView.setLayoutParams(layoutParams);
            textView.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_light));
            textView.setCompoundDrawablesWithIntrinsicBounds(
                    getCircle(Color.parseColor("#" + lineColor)), null, null, null);
        } else {
            return init();
        }
        return textView;
    }
}
