package com.medical.app.view;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;
import com.google.common.base.Strings;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeEditTextView extends TextInputEditText {
    public static final String TIME_MASK = "[00]{:}[00]";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat
            .forPattern("HH:mm");

    public TimeEditTextView(Context context) {
        super(context);
        init();
    }

    public TimeEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TimeEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        MaskedTextChangedListener.Companion.installOn(this, TIME_MASK,
                null);
    }

    public LocalTime getDate() {
        final String date = getDateString();
        if (!Strings.isNullOrEmpty(date)) {
            try {
                return DATE_FORMATTER.parseLocalTime(date);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
        return null;
    }

    @NotNull
    public String getDateString() {
        return getText().toString();
    }

    public void setFormattedText(LocalTime date) {
        this.setText(date.toString(DATE_FORMATTER));
    }

    public LocalTime getLocalTime() {
        final LocalTime currentTime = new LocalTime();
        final String date = getDateString();
        try {
            if (!Strings.isNullOrEmpty(date)) {
                return LocalTime
                        .parse(date, DATE_FORMATTER);
            }
        }
        catch (Exception e){
            return currentTime;
        }
        return currentTime;
    }
}
