package com.medical.app.view.callback;

public interface OnExpandCollapseCallback {

  void onCollapse(boolean expanded);
}
