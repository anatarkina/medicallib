package com.medical.app.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.medical.app.R;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class TextViewWithLine {
    private Context context;
    private String text;
    private String color;

    public TextView init() {
        TextView textView = new TextView(context);
        return init(textView, color);
    }

    private GradientDrawable getLine(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(color);
        shape.setSize(15, 35);
        return shape;
    }

    public TextView init(TextView textView, String color) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setText("  " + text + "  ");
        textView.setTextSize(14);
        textView.setLayoutParams(layoutParams);
        textView.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_light));
        textView.setCompoundDrawablesWithIntrinsicBounds(
                getLine(Color.parseColor("#" + color)), null, null, null);
        return textView;
    }

}
