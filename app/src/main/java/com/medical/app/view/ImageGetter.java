package com.medical.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;

import androidx.core.content.ContextCompat;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class ImageGetter implements Html.ImageGetter {

    private Context context;

    public Drawable getDrawable(String source) {
        int id;
        if (context != null) {
            id = context.getResources().getIdentifier(source, "drawable", context.getPackageName());
            if (id == 0) {
                // the drawable resource wasn't found in our package, maybe it is a stock android drawable?
                id = context.getResources().getIdentifier(source, "drawable", "android");
            }
            if (id == 0) {
                // prevent a crash if the resource still can't be found
                return null;
            } else {
                Drawable d = ContextCompat.getDrawable(context, id);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        }
        return null;
    }

}