package com.medical.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.google.android.material.textfield.TextInputEditText;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

public class ActionEditText extends TextInputEditText {

  public ActionEditText(Context context) {
    super(context);
  }

  public ActionEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ActionEditText(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }


  @Override
  public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
    InputConnection conn = super.onCreateInputConnection(outAttrs);
    outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
    return conn;
  }
}
