package com.medical.app.view;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.material.textfield.TextInputEditText;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy;
import java.util.Arrays;

public class PhoneEditTextView extends TextInputEditText {

  public PhoneEditTextView(Context context) {
    super(context);
    init();
  }

  public PhoneEditTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public PhoneEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }


  private void init() {
    MaskedTextChangedListener.Companion.installOn(
        this,
        "+7 ([000]) [000]-[00]-[00]",
        Arrays.asList("+7 ([000]) [000]-[00]-[00]#[000]"),
        AffinityCalculationStrategy.WHOLE_STRING,
        null
    );
  }
}
