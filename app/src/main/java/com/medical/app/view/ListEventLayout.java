package com.medical.app.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.dao.entity.CardType;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.features.events.dialog.DoctorEventDialog;
import com.medical.app.features.events.dialog.PillsEventDialog;
import com.medical.app.features.events.dto.DoctorEventInfo;
import com.medical.app.features.search.dialog.DoctorReviewDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListEventLayout extends RelativeLayout {
    private EventEntity dataModel;
    private MaterialCardView root;
    private FragmentActivity activity;

    public ListEventLayout(MaterialCardView root, FragmentActivity activity, EventEntity dataModel) {
        super(activity);
        this.activity = activity;
        this.dataModel = dataModel;
        this.root = root;
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.card_tablets_event, this);
        LinearLayout listOfPills = view.findViewById(R.id.list_of_pills);
        List<String> pills = Arrays.asList(dataModel.getContent().split(","));
        root.setBackgroundColor(ContextCompat.getColor(activity, R.color.white));
        CardType type = dataModel.getType();
        String color = type.equals(CardType.TRACKER) ? "00963C" : "E53935";
        pills.forEach(pill -> listOfPills.addView(TextViewWithLine.of(getContext(), pill, color).init()));
        if (type.equals(CardType.TABLETS)) {
            root.setOnClickListener(v -> {
                new PillsEventDialog().show(activity.getSupportFragmentManager(), PillsEventDialog.class.getSimpleName());
            });
        }
    }
}
