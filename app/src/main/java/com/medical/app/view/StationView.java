package com.medical.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.medical.app.R;
import lombok.Getter;

public class StationView extends LinearLayout {

  @Getter
  private TextView stationColor, stationName;

  public StationView(Context context) {
    super(context);
    init();
  }

  public StationView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public StationView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }
  private void init() {
    View view = inflate(getContext(), R.layout.metro_layout, this);
    stationColor = view.findViewById(R.id.station_color);
    stationName = view.findViewById(R.id.station_name);
  }
}
