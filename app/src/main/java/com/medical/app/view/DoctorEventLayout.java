package com.medical.app.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.medical.app.R;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.features.events.dialog.DoctorEventDialog;
import com.medical.app.features.events.dto.DoctorEventInfo;
import com.medical.app.features.search.dialog.DoctorReviewDialog;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorEventLayout extends RelativeLayout {
    private EventEntity dataModel;
    private MaterialCardView root;
    private FragmentActivity activity;

    public DoctorEventLayout(MaterialCardView root, FragmentActivity activity, EventEntity dataModel) {
        super(activity);
        this.activity = activity;
        this.dataModel = dataModel;
        this.root = root;
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.card_doctor_event, this);
        CircleImageView image = view.findViewById(R.id.doctor_image);
        TextView clinic = view.findViewById(R.id.clinic_name);
        TextView street = view.findViewById(R.id.street);
        LinearLayout stations = view.findViewById(R.id.stations);
        TextView date_time = view.findViewById(R.id.date_time);
        root.setBackgroundColor(ContextCompat.getColor(activity, R.color.white));
        DoctorEventInfo info = DoctorEventInfo.toObject(dataModel.getContent());
        Glide.with(this).load(info.getImg()).into(image);
        clinic.setText(info.getClinic());
        street.setText(info.getStreet());
        date_time.setText(info.getStartTime().toString("d.MM.YYYY HH:mm"));
        info.getStations().stream().limit(2).forEach(station -> {
            stations.addView(StationWithColor
                    .of(station.getName(), station.getColor(), getContext(),
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT))
                    .init());
        });
        root.setOnClickListener(v -> DoctorEventDialog.of(dataModel)
                .show(activity.getSupportFragmentManager(), DoctorReviewDialog.class.getSimpleName()));
    }
}
