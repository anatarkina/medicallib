package com.medical.app.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.medical.app.R;

public class ImageOverlayView extends RelativeLayout {

    private TextView bottomDescription, topDescription, topBottomDescription;
    private ImageView menu;
    private PopupMenu popupMenu;

    private String sharingText;

    public ImageOverlayView(Context context) {
        super(context);
        init();
    }

    public ImageOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setBottomDescription(String description) {
        bottomDescription.setText(description);
    }

    public void setTopDescription(String description) {
        topDescription.setText(description);
    }

    public void setTopBottomDescription(String description) {
        topBottomDescription.setText(description);
    }

    public void setShareText(String text) {
        this.sharingText = text;
    }

    private void sendShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sharingText);
        sendIntent.setType("text/plain");
        getContext().startActivity(sendIntent);
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_poster_overlay, this);
        bottomDescription = view.findViewById(R.id.bottom_description);
        topDescription = view.findViewById(R.id.top_description);
        topBottomDescription = view.findViewById(R.id.top_bottom_description);
        menu = view.findViewById(R.id.more_menu);
        popupMenu = showMenu(menu);
        menu.setOnClickListener(v -> {
            popupMenu.show();
        });
    }

    public void setPopUpClickListener(OnMenuItemClickListener menuItemClickListener) {
        popupMenu.setOnMenuItemClickListener(menuItemClickListener);
    }

    private PopupMenu showMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this.getContext(),
                view);
        popupMenu.inflate(R.menu.snapshots_menu);
        return popupMenu;
    }
}