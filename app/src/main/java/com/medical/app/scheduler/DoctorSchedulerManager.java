package com.medical.app.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.medical.app.dao.entity.EventEntity;
import com.medical.app.receiver.DoctorReceiver;
import com.medical.app.repository.EventRepository;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.lang.ref.WeakReference;
import java.util.Locale;

import lombok.AllArgsConstructor;

import static android.content.Context.ALARM_SERVICE;
import static com.medical.app.configs.ApiConstants.Flags.ID;
import static com.medical.app.dao.entity.CardType.DOCTOR;
import static com.medical.app.utils.DateUtils.DATE_TIME_FORMATTER;

@AllArgsConstructor
public class DoctorSchedulerManager {
    private WeakReference<Context> context;
    private AlarmManager alarmManager;

    public static SchedulerManagerBuilder builder() {
        return new SchedulerManagerBuilder();
    }

    public void reschedule() {
        AsyncTask.execute(() -> {
            try {
                EventRepository eventRepository = new EventRepository(context.get());
                eventRepository.getAllSync()
                        .stream()
                        .filter(event -> DOCTOR.equals(event.getType()))
                        .filter(event -> LocalDateTime.now().isBefore(event.getTo()))
                        .filter(event -> event.isActive())
                        .forEach(event -> {
                            scheduleEvent(event);
                        });
            } catch (Exception e) {
                Log.e(DoctorSchedulerManager.class.getSimpleName(), "Can't schedule doctor notifications", e);
            }
        });
    }

    public void cancel(EventEntity event) {
        Intent intent = createIntent(DoctorSchedulerManager.class.getSimpleName(), event.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.get(), 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    private void scheduleEvent(EventEntity event) {
        Log.i(this.getClass().getSimpleName(), "Schedule doctor event by id: " + event.getId());
        Intent intent = createIntent(DoctorSchedulerManager.class.getSimpleName(), event.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.get(), 0, intent, 0);
        alarmManager.cancel(pendingIntent);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                event.getTo().minusHours(1)
                        .toDateTime(DateTimeZone.getDefault())
                        .toCalendar(Locale.getDefault())
                        .getTimeInMillis(),
                pendingIntent);
    }

    private Intent createIntent(String action, Long id) {
        Intent intent = new Intent(context.get(), DoctorReceiver.class);
        intent.setAction(action);
        intent.putExtra(ID, id);
        return intent;
    }

    public static class SchedulerManagerBuilder {
        private Context context;

        SchedulerManagerBuilder() {
        }

        public SchedulerManagerBuilder context(Context context) {
            this.context = context;
            return this;
        }

        public void reschedule() {
            new DoctorSchedulerManager(new WeakReference<>(context), (AlarmManager) context
                    .getSystemService(ALARM_SERVICE)).reschedule();
        }

        public void reschedule(EventEntity event) {
            new DoctorSchedulerManager(new WeakReference<>(context), (AlarmManager) context
                    .getSystemService(ALARM_SERVICE)).scheduleEvent(event);
        }


        public void cancel(EventEntity event) {
            new DoctorSchedulerManager(new WeakReference<>(context), (AlarmManager) context
                    .getSystemService(ALARM_SERVICE)).cancel(event);
        }
    }
}
