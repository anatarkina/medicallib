package com.medical.app.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.User;
import com.medical.app.receiver.PillReceiver;
import com.medical.app.repository.UserRepository;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.function.Predicate;

import lombok.AllArgsConstructor;

import static android.content.Context.ALARM_SERVICE;
import static com.medical.app.utils.NullFunctions.isBlankString;

@AllArgsConstructor
public class PillSchedulerManager {
    private WeakReference<Context> context;
    private AlarmManager alarmManager;
    private UserRepository userRepository;

    public static SchedulerManagerBuilder builder() {
        return new SchedulerManagerBuilder();
    }

    public void reschedule() {
        AsyncTask.execute(() -> {
            try {
                Intent intent = createIntent(PillSchedulerManager.class.getSimpleName());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context.get(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                User user = userRepository.getCurrent();
                LocalTime startTime = user.getNotifyStart();
                int initialFrequency = user.getNumberNotifications() - 1;
                Integer frequency = initialFrequency > 0 ? initialFrequency : 1;
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                        startTime.toDateTimeToday().toDateTime(DateTimeZone.getDefault())
                                .toCalendar(Locale.getDefault()).getTimeInMillis(),
                        getInterval(startTime, user.getNotifyEnd(), frequency),
                        pendingIntent);

            } catch (Exception e) {
                Log.e(PillSchedulerManager.class.getSimpleName(), "Can't schedule pill notifications", e);
            }
        });
    }

    private long getInterval(LocalTime startTime, LocalTime endTime, Integer frequency) {
        return (endTime.toDateTimeToday().toDateTime(DateTimeZone.getDefault()).toCalendar(Locale.getDefault()).getTimeInMillis() -
                startTime.toDateTimeToday().toDateTime(DateTimeZone.getDefault()).toCalendar(Locale.getDefault()).getTimeInMillis()) / frequency;
    }

    private Intent createIntent(String action) {
        Intent intent = new Intent(context.get(), PillReceiver.class);
        intent.setAction(action);
        return intent;
    }

    public static class SchedulerManagerBuilder {
        private Context context;

        SchedulerManagerBuilder() {
        }

        public SchedulerManagerBuilder context(Context context) {
            this.context = context;
            return this;
        }

        public void reschedule() {
            new PillSchedulerManager(new WeakReference<>(context), (AlarmManager) context
                    .getSystemService(ALARM_SERVICE), new UserRepository(context)).reschedule();
        }
    }
}
