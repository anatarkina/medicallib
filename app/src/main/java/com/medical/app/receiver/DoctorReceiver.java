package com.medical.app.receiver;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.google.firebase.FirebaseApp;
import com.medical.app.R;
import com.medical.app.activity.MainActivity;
import com.medical.app.dao.entity.EventEntity;
import com.medical.app.dao.entity.User;
import com.medical.app.features.events.dto.DoctorEventInfo;
import com.medical.app.repository.EventRepository;
import com.medical.app.repository.UserRepository;
import com.medical.app.scheduler.DoctorSchedulerManager;

import java.util.concurrent.ExecutionException;

import static com.medical.app.configs.ApiConstants.Flags.ACTION;
import static com.medical.app.configs.ApiConstants.Flags.CALL_DOCTOR;
import static com.medical.app.configs.ApiConstants.Flags.ID;

public class DoctorReceiver extends BroadcastReceiver {
    private static final String TAG = "DoctorReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Receive intent");
        FirebaseApp.initializeApp(context);
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            DoctorSchedulerManager.builder()
                    .context(context)
                    .reschedule();
        } else {
            AsyncTask.execute(() -> {
                User user = new UserRepository(context).getCurrent();
                long id = intent.getLongExtra(ID, 0);
                Log.d(TAG, "Execute event by id: " + id);
                EventRepository eventRepository = new EventRepository(context);
                EventEntity event = eventRepository.findById(id);
                if (event != null) {
                    DoctorEventInfo info = DoctorEventInfo.toObject(event.getContent());
                    if (user.getNotificationDoctor() && event.isActive()) {
                        sendNotification(context, id, info);
                    }
                }
            });
        }
    }

    public static void sendNotification(Context context, long id, DoctorEventInfo info) {
        Log.d(TAG, "Try to send notify: " + info.toString());

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(ACTION, CALL_DOCTOR);
        intent.putExtra(ID, id);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "M_CH_ID")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Запись к врачу")
                .setContentText(info.getFioDoctor())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // === Removed some obsoletes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "M_CH_ID";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }
        FutureTarget<Bitmap> futureTarget = Glide.with(context).asBitmap()
                .load(info.getImgFormat())
                .circleCrop().submit();
        LoadImageTask task = new LoadImageTask(icon -> {
            notificationBuilder.setLargeIcon(icon);
            Glide.with(context).clear(futureTarget);
            notificationManager.notify(0,
                    notificationBuilder.build());
        });
        task.execute(futureTarget);
    }

    private static class LoadImageTask extends AsyncTask<FutureTarget<Bitmap>, Void, Bitmap> {
        private OnSuccess onSuccess;

        interface OnSuccess {
            void onSuccess(Bitmap bitmap);
        }

        LoadImageTask(OnSuccess onSuccess) {
            this.onSuccess = onSuccess;
        }

        @SafeVarargs
        @Override
        protected final Bitmap doInBackground(FutureTarget<Bitmap>... futureTargets) {
            try {
                return futureTargets[0].get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null)
                onSuccess.onSuccess(bitmap);
        }
    }
}
