package com.medical.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.medical.app.dao.entity.Pill;
import com.medical.app.dao.entity.User;
import com.medical.app.repository.PomRepository;
import com.medical.app.repository.UserRepository;
import com.medical.app.scheduler.DoctorSchedulerManager;
import com.medical.app.scheduler.PillSchedulerManager;

import org.joda.time.LocalTime;

import java.util.List;

import static com.medical.app.repository.PomRepository.getPillCountPredicate;
import static com.medical.app.repository.PomRepository.getPillDurationPredicate;
import static com.medical.app.utils.NotificationUtil.sendNotification;

public class PillReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        FirebaseApp.initializeApp(context);
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            reschedule(context);
        } else {
            AsyncTask.execute(() -> {
                PomRepository pomRepository = new PomRepository(context);
                User user = new UserRepository(context).getCurrent();
                List<Pill> pills = pomRepository.getPills();
                boolean existPills = pills.stream()
                        .filter(getPillDurationPredicate().and(getPillCountPredicate()))
                        .findAny()
                        .isPresent();
                LocalTime now = LocalTime.now();
                if (existPills && now.isAfter(user.getNotifyStart()) && now.isBefore(user.getNotifyEnd().plusSeconds(10)) && user.getNotificationPills()) {
                    sendNotification(context, "План лекарства",
                            "Пора принимать лекарства");
                }
            });
        }
    }

    private void reschedule(Context context) {
        PillSchedulerManager.builder()
                .context(context)
                .reschedule();
    }

}
