package com.medical.app.dto;

import com.medical.app.dao.entity.Record;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.joda.time.LocalDate;

@AllArgsConstructor(staticName = "of")
@Data
public class RecordByDate {

  private String date;
  private List<Record> records;

}
