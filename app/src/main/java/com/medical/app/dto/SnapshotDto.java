package com.medical.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor(staticName = "of")
@Data
@Builder
public class SnapshotDto {

  private Long recordId;
  private String imageUrl;
  private String description;
  private String chipSelected;
  private String date;
  private boolean selected;
  private boolean updated;
}
