package com.medical.app.dto;

import com.medical.app.dao.entity.StreamEntity;

import org.joda.time.LocalDate;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StreamDto {
    private LocalDate date;
    private List<StreamEntity> values;
}
