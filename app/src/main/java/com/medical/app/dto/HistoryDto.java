package com.medical.app.dto;

import com.medical.app.dao.entity.Record;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HistoryDto {

  private String history;
  private String value;
  private String valueDescription;
  private String owner;
  private String ownerDescription;
  private String date;
  @Builder.Default
  private List<String> urls;
  private List<Record> values;
}
